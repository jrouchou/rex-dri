/* This file is use to start a webpack development server
 * That supports hot module replacement.
 * The compiled files are made available on 0.0.0.0:3000
 *
 * The link with Django is made using webpack-bundle-tracker on the js side.
 *   It generates the `webpack-stat.json` file with the url of each compiled file.
 *
 * On Django side, the "dynamic" link is made using django-webpack-loader package.
 *
 * [OUT OF THE SCOPE OF THIS FILE]
 * Note that if you run `yarn build`, then the file are generated and stored in the `dist` folder.
 *  `webpack-stat.json` will be updated accordingly; so there is nothing to worry with Django.
 *
 */

var webpack = require("webpack");
var WebpackDevServer = require("webpack-dev-server");
var config = require("./webpack.config.dev");

new WebpackDevServer(webpack(config), {
  publicPath: config.output.publicPath,
  hot: true,
  inline: true,
  //progress: true,
  historyApiFallback: true,
  headers: { "Access-Control-Allow-Origin": "*" },
}).listen(3000, "0.0.0.0", (err) => {
  if (err) {
    // eslint-disable-next-line no-console
    console.log(err);
  }

  // eslint-disable-next-line no-console
  console.log("Listening at 0.0.0.0:3000");
});
