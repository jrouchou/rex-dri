import arrayOfInstancesToMap from "../../utils/arrayOfInstancesToMap";
import { getLatestApiReadData } from "../../hooks/useGlobalState";

class CurrencyService {
  /**
   * Stores the currencies by currency id for quick join
   * @type {Map<string, object>}
   * @private
   */
  _currenciesByCurrencyId = new Map();

  /**
   * @private
   * @type {array.<object>}
   */
  _options = [];

  /**
   * Must be called once before accessing other methods.
   */
  initialize() {
    const currencies = this.getCurrencies();

    if (currencies.length === 0) {
      // To use this function, you need to have currencies loaded in the store.
      // **Make sure the currencies are loaded in the backend.**
      // Also, make sure not use this function before we have the currencies loaded :)
      throw new Error(
        "Currencies in the store are empty see code for more info."
      );
    }

    this._currenciesByCurrencyId = arrayOfInstancesToMap(currencies);

    this._options = currencies.map((c) => ({
      label: c.name ? c.name : c.code,
      value: c.code,
      disabled: false,
    }));
  }

  getCurrencies() {
    return getLatestApiReadData("currencies-all");
  }

  getCurrenciesOptions() {
    return this._options;
  }

  /**
   * Get the currency object for its id
   * @param {number} currencyId
   * @returns {Object}
   */
  getCurrencyForCurrencyId(currencyId) {
    return this._currenciesByCurrencyId.get(currencyId);
  }

  getCurrencySymbol(currencyCode) {
    if (currencyCode === "EUR") {
      return "€";
    }

    const currency = this._currenciesByCurrencyId.get(currencyCode);
    if (currency.symbol) return currency.symbol;

    return ` ${currencyCode}`;
  }

  /**
   * Function for converting money amounts to euros.
   *
   * @param {number|null} amount
   * @param {string} currencyCode
   * @returns {string|null} if no matching currency could be found
   */
  convertAmountToEur(amount, currencyCode) {
    if (amount === null) return null;

    if (currencyCode === "EUR") {
      return amount.toFixed(2);
    }

    if (this._currenciesByCurrencyId.has(currencyCode)) {
      return (
        amount /
        this._currenciesByCurrencyId.get(currencyCode).one_EUR_in_this_currency
      ).toFixed(2);
    }
    return null;
  }
}

export default new CurrencyService();
