import arrayOfInstancesToMap from "../../utils/arrayOfInstancesToMap";
import { getLatestApiReadData } from "../../hooks/useGlobalState";

class UniversityService {
  /**
   * Stores the universities by its id
   * @type {Map<number, object>}
   * @private
   */
  _universitiesById = new Map();

  /**
   * @private
   * @type {array.<object>}
   */
  _options = [];

  /**
   * Must be called once before accessing other methods.
   */
  initialize() {
    const universities = this.getUniversities();
    this._universitiesById = arrayOfInstancesToMap(universities);

    this._options = universities.map((el) => ({
      value: el.id,
      label: el.name,
      disabled: false,
    }));
  }

  /**
   * Get the full list of universities in the app
   * @returns {Array<Object>}
   */
  getUniversities() {
    return getLatestApiReadData("universities-all");
  }

  getUniversitiesOptions() {
    return this._options;
  }

  /**
   * @param {number} univId
   * @returns {Object}
   */
  getUniversityById(univId) {
    return this._universitiesById.get(univId);
  }

  /**
   * Tells whether or not a univId is known or not
   * @param {number} univId
   * @returns {boolean}
   */
  hasUniversity(univId) {
    return this._universitiesById.has(univId);
  }

  /**
   *
   * @param {number} univId
   * @returns {string}
   */
  getUnivName(univId) {
    const univ = this.getUniversityById(univId);

    if (typeof univ === "undefined")
      return "(Université pas encore répertoriée sur REX-DRI)";
    return univ.name;
  }
}

export default new UniversityService();
