import arrayOfInstancesToMap from "../../utils/arrayOfInstancesToMap";
import { getLatestApiReadData } from "../../hooks/useGlobalState";

class CountryService {
  /**
   * Stores the countries by its id
   * @type {Map<number, object>}
   * @private
   */
  _countriesById = new Map();

  /**
   * @private
   * @type {array.<object>}
   */
  _options = [];

  /**
   * Must be called once before accessing other methods.
   */
  initialize() {
    const countries = this.getCountries();
    this._countriesById = arrayOfInstancesToMap(countries);

    this._options = countries.map((country) => ({
      label: country.name,
      value: country.id,
      disabled: false,
    }));
  }

  /**
   * Get the country object for a country id
   *
   * @param {number} countryId
   * @returns {Object}
   */
  getCountryForCountryId(countryId) {
    return this._countriesById.get(countryId);
  }

  /**
   * @returns {Array.<Object>}
   */
  getCountries() {
    return getLatestApiReadData("countries-all");
  }

  getCountryOptions() {
    return this._options;
  }
}

export default new CountryService();
