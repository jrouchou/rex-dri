import { getMostNRecentSemesters } from "../utils/compareSemesters";
import UniversityService from "./data/UniversityService";
import CountryService from "./data/CountryService";
import { getUpdatedMajor, getUpdatedMinor } from "../utils/majorMinorMappings";

/**
 * Class that handle all the filter manipulation with caching
 */
class FilterService {
  _universitiesCountriesCache = new Map();

  _semesterOptions = [];

  _defaultSemesters = [];

  _majorMinorOptions = undefined;

  _destinationOpenCount = 0;

  initialize() {
    const universities = UniversityService.getUniversities();

    universities.forEach((univ) => {
      this._universitiesCountriesCache.set(
        univ.id,
        CountryService.getCountryForCountryId(univ.country)
      );
    });

    const possibleSemesters = new Set(
      universities.flatMap((u) => this.getSemestersInUniv(u))
    );

    this._semesterOptions = [...possibleSemesters].map((semester) => ({
      value: semester,
      label: semester,
    }));

    this._defaultSemesters = getMostNRecentSemesters(
      this._semesterOptions.map((el) => el.value),
      4
    );

    const possibleMajorMinors = new Set(
      universities.flatMap((u) => this.getMajorMinorsInUniv(u))
    );

    this._majorMinorOptions = [...possibleMajorMinors].map((el) => ({
      value: el,
      label: el,
    }));

    this._destinationOpenCount = universities.filter(
      (u) => u.denormalized_infos.is_destination_open === true
    ).length;
  }

  get universityIdsCountries() {
    return this._universitiesCountriesCache;
  }

  get countriesOptions() {
    return CountryService.getCountryOptions();
  }

  /**
   * @return {array.<object>}
   */
  get majorMinorOptions() {
    return this._majorMinorOptions;
  }

  /**
   * @return {array.<object>}
   */
  get semesterOptions() {
    return this._semesterOptions;
  }

  get defaultSemesters() {
    return this._defaultSemesters;
  }

  /**
   * @return {number}
   */
  get destinationOpenCount() {
    return this._destinationOpenCount;
  }

  getMajorInUniv(univObj) {
    return [
      ...new Set(
        Object.values(
          univObj.denormalized_infos.semesters_majors_minors
        ).flatMap((forSemester) => Object.keys(forSemester))
      ),
    ];
  }

  /**
   * @param univObj
   * @param allowedSemesters
   * @return {array.<string>}
   */
  getMajorMinorsInUniv(univObj, allowedSemesters = null) {
    const realMinors = Object.entries(
      univObj.denormalized_infos.semesters_majors_minors
    )
      .filter(([sem]) =>
        allowedSemesters === null ? true : allowedSemesters.includes(sem)
      )
      .map(([, forSemester]) => forSemester)
      .flatMap((forSemester) => Object.entries(forSemester))
      .flatMap(([major, minors]) =>
        minors.map((minor) => {
          const maj = getUpdatedMajor(major);
          const min = getUpdatedMinor(major, minor);
          return `${maj} — ${min}`;
        })
      );

    const extraMinors = this.getMajorInUniv(univObj).map((major) => {
      const maj = getUpdatedMajor(major);
      return `${maj} — Toutes filières confondues`;
    });
    return [...new Set(realMinors), ...extraMinors];
  }

  getSemestersInUniv(univObj) {
    return Object.keys(univObj.denormalized_infos.semesters_majors_minors);
  }

  /**
   *
   * @param {array.<string>} countryCodes
   * @return {array.<object>}
   */
  getUniversitiesInCountries(countryCodes) {
    const possiblesCountries = new Set(countryCodes);
    const out = [];
    this.universityIdsCountries.forEach((country, univId) => {
      if (possiblesCountries.has(country.id)) out.push(univId);
    });
    return out;
  }

  /**
   *
   * @param {array.<string>} selectedCountriesCode
   * @param {array.<string>} selectedSemesters
   * @param {array.<string>} selectedMajorMinors
   * @return {array.<number>}
   */
  getSelection(
    selectedCountriesCode = [],
    selectedSemesters = [],
    selectedMajorMinors = [],
    selectOnlyOpenDestinations = false
  ) {
    let possible =
      selectedCountriesCode.length === 0
        ? UniversityService.getUniversities()
        : this.getUniversitiesInCountries(selectedCountriesCode).map((id) =>
            UniversityService.getUniversityById(id)
          );

    if (selectedSemesters.length > 0) {
      const possibleSemesters = new Set(selectedSemesters);
      possible = possible.filter((univ) => {
        const semestersInUniv = this.getSemestersInUniv(univ);
        return semestersInUniv.some((semester) =>
          possibleSemesters.has(semester)
        );
      });
    }

    if (selectedMajorMinors.length > 0) {
      const possibleMajorMinors = new Set(selectedMajorMinors);
      possible = possible.filter((univ) => {
        const majorMinorsInUniv = new Set(this.getMajorMinorsInUniv(univ));
        return [...majorMinorsInUniv].some((el) => possibleMajorMinors.has(el));
      });
    }

    if (selectedSemesters.length > 0 && selectedMajorMinors.length > 0) {
      const possibleMajorMinors = new Set(selectedMajorMinors);
      const possibleSemesters = new Set(selectedSemesters);
      possible = possible.filter((univ) => {
        const semestersInUniv = this.getSemestersInUniv(univ);
        const possibleSemesterInUniv = semestersInUniv.filter((sem) =>
          possibleSemesters.has(sem)
        );
        return this.getMajorMinorsInUniv(
          univ,
          possibleSemesterInUniv
        ).some((el) => possibleMajorMinors.has(el));
      });
    }

    if (selectOnlyOpenDestinations) {
      possible = possible.filter(
        (univ) => univ.denormalized_infos.is_destination_open === true
      );
    }

    return possible.map((univ) => univ.id);
  }
}

export default new FilterService();
