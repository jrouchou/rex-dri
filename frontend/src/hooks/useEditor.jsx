import React, { useCallback } from "react";
import FullScreenDialogService from "../services/FullScreenDialogService";
import Editor from "../components/edition/Editor";
import useEditorDispatch from "./wrappers/useEditorDispatch";
import useEditorState from "./wrappers/useEditorState";

/**
 * Hook that provides an easy access to an editor.
 *
 * @param {FormInfo} formInfo
 * @param {function(bool*)} [onClose] - Method called when the editor is clsoed
 * @returns {function(object)}
 */
function useEditor(formInfo, onClose = () => {}) {
  // Returns a callback that opens an editor correctly configured
  return useCallback((modelData, saveOnMount = false) => {
    const { route, license, Form, formLevelErrors } = formInfo;

    const InternalEditor = React.memo(() => {
      const { saveData, clearSaveError } = useEditorDispatch(route);

      const {
        savingHasError,
        lastUpdateTimeInModel,
        hasPendingModeration,
      } = useEditorState(route);

      return (
        <Editor
          license={license}
          Form={Form}
          formLevelErrors={formLevelErrors}
          rawModelData={modelData}
          // state api related
          saveData={saveData}
          saveOnMount={saveOnMount}
          clearSaveError={clearSaveError}
          savingHasError={savingHasError}
          lastUpdateTimeInModel={lastUpdateTimeInModel}
          hasPendingModeration={hasPendingModeration}
          onClose={onClose}
        />
      );
    });

    FullScreenDialogService.openDialog(<InternalEditor />);
  }, []);
}

export default useEditor;
