import { useEffect } from "react";

/**
 * Hook to run a method on component un-mount.
 *
 * @param {Function} execOnUnMount
 */
function useOnComponentUnMount(execOnUnMount) {
  useEffect(() => {
    return execOnUnMount;
  }, []);
}

export default useOnComponentUnMount;
