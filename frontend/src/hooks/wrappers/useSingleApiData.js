import { useEffect, useMemo, useState } from "react";
import { apiDataIsUsable, getLatestRead } from "../../utils/api/utils";
import RequestParams from "../../utils/api/RequestParams";
import { useApiRead } from "./api";
import useOnBeforeComponentMount from "../useOnBeforeComponentMount";

/**
 * Hook to provide access to the data from the backend stored in the global state.
 *
 * @param {string} routeName - Route from where to get the data
 * @param {"all"|"one"} variant - Is it a "all" or a "one" (GET all or GET one object)
 * @param {RequestParams|function():RequestParams} [initialParams] - RequestParams for the first request.

 * @returns {{isLoading: boolean, data: ({}|Function), latestData: *, hasError: boolean, setParams: React.Dispatch<React.SetStateAction<RequestParams>>}}
 */
function useSingleApiData(
  routeName,
  variant,
  initialParams = RequestParams.Builder.build()
) {
  const [params, setParams] = useState(initialParams);

  const [data, performRead] = useApiRead(routeName, variant);

  /**
   *
   *
   *
   *
   */

  // Initial load
  useOnBeforeComponentMount(() => {
    if (!apiDataIsUsable(data)) performRead(params);
  });

  // reload data if needed
  useEffect(() => {
    if (data.readFailed.failed === true) return;

    if (!data.isReading) {
      if (data.isInvalidated === true) performRead(params);
      if (!data.readSucceeded.requestParams.equals(params)) {
        performRead(params);
      }
    }
  }, [data, params]);

  return useMemo(() => {
    const hasError = data.readFailed.failed;
    const isLoading = !apiDataIsUsable(data);
    const latestData = getLatestRead(data).data;

    if (!data.isReading) {
      // make sure performRead is triggered (it seems not te be triggered sometimes)
      if (data.isInvalidated === true) performRead(params);
    }

    return { hasError, isLoading, latestData, data, setParams };
  }, [data, params, setParams]);
}

export default useSingleApiData;
