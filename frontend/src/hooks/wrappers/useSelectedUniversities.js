import { useMemo } from "react";
import useGlobalState from "../useGlobalState";

export function useSelectedUniversities() {
  return useGlobalState("app-selected-universities", null);
}

export function useSetSelectedUniversities() {
  const [, setter] = useSelectedUniversities();
  return useMemo(() => setter, [setter]);
}
