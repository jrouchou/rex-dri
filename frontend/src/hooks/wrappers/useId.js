import { useState } from "react";
import { v4 as uuidv4 } from "uuid";

/**
 * Hook that provides a constant id
 * @returns {string}
 */
function useId() {
  const [id] = useState(uuidv4());
  return id;
}

export default useId;
