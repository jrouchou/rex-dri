import { useCallback, useMemo, useState } from "react";
import { getLatestRead } from "../utils/api/utils";
import useOnBeforeComponentMount from "./useOnBeforeComponentMount";
import useOnComponentUnMount from "./useOnComponentUnMount";

/**
 * @type {Map<string, any>}
 */
export const globalState = new Map();

/**
 * Direct access to a persisted value in the global state
 *
 * @param {string} key
 * @returns {any}
 */
export function getPersistedValue(key) {
  return globalState.get(key);
}

if (process.env.NODE_ENV !== "production") {
  window.globalRexDriState = globalState;
}

/**
 * @type {Map<string, Set<Function>>}
 */
const globalStateListeners = new Map();

/**
 * Helper to update all the listeners on the global state key
 *
 * @param {string} key
 * @param {any} newValue
 * @private
 */
function updateGlobalStateListeners(key, newValue) {
  if (globalStateListeners.has(key)) {
    globalStateListeners.get(key).forEach((f) => {
      f(newValue);
    });
  }
}

/**
 * Helper to update the values in the global state and tel the associated listeners
 *
 * @param {string} key
 * @param {any} newValue
 */
export function updateGlobalState(key, newValue) {
  const valueInStore = getPersistedValue(key);
  if (valueInStore !== newValue) {
    globalState.set(key, newValue);
    updateGlobalStateListeners(key, newValue);
  }
}

/**
 * Helper to reduce the value in the globalState (identified by a key)
 * And trigger an update to all subscribers.
 *
 * @param {string} key
 * @param {function(any, object): any} reducer
 * @param {{type: string, ...}} action
 */
export function reduceGlobalState(key, reducer, action) {
  if (process.env.NODE_ENV !== "production") {
    const { type } = action;

    let backgroundColor;
    if (type.includes("STARTED")) backgroundColor = "cyan";
    else if (type.includes("SUCCEEDED")) backgroundColor = "green";
    else if (type.includes("INVALIDATED")) backgroundColor = "orange";
    else if (type.includes("FAILED")) backgroundColor = "red";
    else backgroundColor = "black";

    // eslint-disable-next-line no-console
    console.log(
      `%c ACTION %c ${action.type}`,
      `background: ${backgroundColor}; color: white; font-weight: bold;`,
      ""
    );
  }

  const previousValue = getPersistedValue(key);
  const newValue = reducer(previousValue, action);
  updateGlobalState(key, newValue);
}

/**
 * Gets the latest data retrieved from the api for the key
 * @param {string} key - route / sing or plur
 */
export function getLatestApiReadData(key) {
  return getLatestRead(globalState.get(`api-${key}`)).data;
}

/**
 * Hook that stores the state in a persistent state
 *
 * @param {string} key
 * @param {*} initialValue
 * @returns {[*, Function]}
 */
function useGlobalState(key, initialValue) {
  // initializing the global state if needed
  useOnBeforeComponentMount(() => {
    if (!globalState.has(key)) globalState.set(key, initialValue);
  });

  const [state, setState] = useState(() => getPersistedValue(key));

  useOnBeforeComponentMount(() => {
    if (!globalStateListeners.has(key))
      globalStateListeners.set(key, new Set());
    globalStateListeners.get(key).add(setState);
  });

  useOnComponentUnMount(() => {
    if (globalStateListeners.has(key)) {
      globalStateListeners.get(key).delete(setState);
    }
  });

  const setPersistentState = useCallback((value) => {
    const currentGlobalValue = getPersistedValue(key);
    const valueToStore =
      value instanceof Function ? value(currentGlobalValue) : value;

    updateGlobalState(key, valueToStore);
  }, []);

  // Make sure to return the latest data
  return useMemo(() => [state, setPersistentState], [state]);
}

export default useGlobalState;
