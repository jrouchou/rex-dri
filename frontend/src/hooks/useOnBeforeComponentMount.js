import { useRef } from "react";

/**
 * Hook to make sure something is directly run and only ran during mount.
 * (useMemo might be triggered again, useEffect with no dependencies seems to have some delay).
 *
 * @param {Function} execOnMount
 * @returns {boolean} Has the method been executed?
 */
function useOnBeforeComponentMount(execOnMount) {
  const ref = useRef(false);
  if (ref.current === false) {
    execOnMount();
    ref.current = true;
  }

  // Has been ran?
  return ref.current;
}

export default useOnBeforeComponentMount;
