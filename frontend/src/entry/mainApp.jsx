import "core-js/stable";
import "regenerator-runtime/runtime";

import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";
import { SnackbarProvider } from "notistack"; // provider to easily handle notifications across the app
import CssBaseline from "@material-ui/core/CssBaseline";
import App from "../components/app/App";
import ThemeProvider from "../components/common/theme/ThemeProvider";
import SnackbarCloseButton from "../components/app/SnackbarCloseButton";
import NavigationService from "../services/NavigationService";
import OfflineThemeProvider from "../components/common/theme/OfflineThemeProvider";
import AlertServiceComponent from "../components/services/AlertServiceComponent";
import FullScreenDialogServiceComponent from "../components/services/FullScreenDialogServiceComponent";
import NotificationServiceComponent from "../components/services/NotificationServiceComponent";
import NotifierImportantInformation from "../components/app/NotifierImportantInformation";
import { useStylesNotiStack } from "./shared";

function SubEntry() {
  // to ba able to use the theme from here
  const classesNotistack = useStylesNotiStack();

  return (
    <SnackbarProvider
      maxSnack={3}
      anchorOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      action={(key) => <SnackbarCloseButton notiKey={key} />}
      classes={{ ...classesNotistack }}
    >
      <>
        <CssBaseline />
        <AlertServiceComponent />
        <FullScreenDialogServiceComponent />
        <NotificationServiceComponent />
        <NotifierImportantInformation />
        <App />
      </>
    </SnackbarProvider>
  );
}

function MainReactEntry() {
  return (
    <OfflineThemeProvider>
      {/* We make sure to have at least one theme active, that's why there is an offline theme too */}
      <ThemeProvider>
        <Router ref={NavigationService.setComponent}>
          <SubEntry />
        </Router>
      </ThemeProvider>
    </OfflineThemeProvider>
  );
}

const wrapper = document.getElementById("app");
ReactDOM.render(<MainReactEntry />, wrapper);

// eslint-disable-next-line no-undef
if (module.hot) {
  // eslint-disable-next-line no-undef
  module.hot.accept();
}
