import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import CreateIcon from "@material-ui/icons/Create";
import sortBy from "lodash/sortBy";
import PictureEditor from "./PictureForm";
import DeleteHandler from "../common/DeleteHandler";

const styles = (theme) => ({
  spacer: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(1),
  },
  picture: {
    maxHeight: "30vh",
    minHeight: "100px",
    maxWidth: "100%",
    minWidth: "40vw",
  },
  pictureContainer: {
    width: "fit-content",
    // marginLeft: "auto",
    // marginRight: "auto",
  },
  rightIcon: {
    marginLeft: theme.spacing(1),
  },
});

/**
 * WARNING BETA
 *
 * HOOKIFIED WHEN WILL BE USED
 */
class Pictures extends React.Component {
  state = {
    editorOpen: false,
    info: {},
    deleteId: undefined,
  };

  openEditorPanel(info = {}) {
    this.setState({
      editorOpen: true,
      info,
    });
  }

  closeEditorPanel(somethingWasSaved) {
    this.setState({
      editorOpen: false,
      info: {},
    });
    if (somethingWasSaved) {
      this.props.onSomethingWasSaved();
    }
  }

  render() {
    const { classes, theme, pictures } = this.props;
    const { deleteId } = this.state;
    return (
      <>
        <Button
          variant="contained"
          color="primary"
          onClick={() => this.openEditorPanel()}
        >
          Ajouter
          <CreateIcon className={classes.rightIcon} />
        </Button>
        {sortBy(pictures, "id").map((info) => (
          <React.Fragment key={info.id}>
            <Typography variant="h4">{info.title}</Typography>
            {info.description}
            {info.license}
            <div className={classes.spacer} />
            <Paper style={theme.myPaper} className={classes.pictureContainer}>
              <img
                src={info.file}
                className={classes.picture}
                alt={info.title}
              />
            </Paper>
            <div>
              <Button
                variant="contained"
                color="primary"
                onClick={() => this.openEditorPanel(info)}
              >
                Éditer
                <CreateIcon className={classes.rightIcon} />
              </Button>
              <Button
                variant="contained"
                color="secondary"
                onClick={() => this.setState({ deleteId: info.id })}
              >
                Supprimer
                <CreateIcon className={classes.rightIcon} />
              </Button>
            </div>
          </React.Fragment>
        ))}
        <PictureEditor
          open={this.state.editorOpen}
          closeEditorPanel={(bool) => this.closeEditorPanel(bool)}
          rawModelData={this.state.info}
        />
        {typeof deleteId !== "undefined" ? (
          <DeleteHandler
            performClose={() => {
              this.setState({ deleteId: undefined });
              this.props.onSomethingWasSaved();
            }}
            route="pictures"
            id={deleteId}
          />
        ) : (
          <></>
        )}
      </>
    );
  }
}

Pictures.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  pictures: PropTypes.array.isRequired,
  onSomethingWasSaved: PropTypes.func.isRequired,
};

export default withStyles(styles, { withTheme: true })(Pictures);
