import React from "react";

import DateField from "../../edition/fields/DateField";

import FormLevelError from "../../../utils/editionRelated/FormLevelError";
import { CommentField } from "../../edition/fields/wrappedFields";
import UsefulLinksField from "../../edition/fields/UsefulLinksField";
import HiddenField from "../../edition/fields/HiddenField";
import FormInfo from "../../../utils/editionRelated/FormInfo";

function onlyOneIsNull(a, b) {
  return (a == null && b != null) || (b == null && a != null);
}

const formLevelErrors = [
  new FormLevelError(
    ["autumn_begin", "autumn_end"],
    (begin, end) => onlyOneIsNull(begin, end),
    "Si une date est saisie pour le semestre d'automne, l'autre doit l'être aussi."
  ),
  new FormLevelError(
    ["spring_begin", "spring_end"],
    (begin, end) => onlyOneIsNull(begin, end),
    "Si une date est saisie pour le semestre de printemps, l'autre doit l'être aussi."
  ),
  new FormLevelError(
    ["autumn_begin", "autumn_end"],
    (begin, end) => begin && end && begin > end,
    "Le début du semestre d'automne doit être antérieur à sa fin..."
  ),
  new FormLevelError(
    ["spring_begin", "spring_end"],
    (begin, end) => begin && end && begin > end,
    "Le début du semestre de printemps doit être antérieur à sa fin..."
  ),
];

function UniversitySemestersDatesForm() {
  return (
    <>
      <HiddenField fieldMapping="university" />
      <DateField
        label="Date de début du semestre de printemps"
        fieldMapping="spring_begin"
      />
      <DateField
        label="Date de fin du semestre de printemps"
        fieldMapping="spring_end"
      />
      <DateField
        label={"Date de début du semestre d'automne"}
        fieldMapping="autumn_begin"
      />
      <DateField
        label={"Date de fin du semestre d'automne"}
        fieldMapping="autumn_end"
      />
      <CommentField />
      <UsefulLinksField />
    </>
  );
}

export default new FormInfo(
  "universitiesSemestersDates",
  UniversitySemestersDatesForm,
  formLevelErrors
);
