import React from "react";
import {
  CommentField,
  CountriesField,
  ImportanceLevelField,
  TitleField,
} from "../../edition/fields/wrappedFields";
import UsefulLinksField from "../../edition/fields/UsefulLinksField";
import FormInfo from "../../../utils/editionRelated/FormInfo";

function CountryDriForm() {
  return (
    <>
      <TitleField />
      <ImportanceLevelField />
      <CountriesField />
      <CommentField />
      <UsefulLinksField />
    </>
  );
}

export default new FormInfo("countryDri", CountryDriForm);
