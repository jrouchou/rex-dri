/* eslint-disable indent */
import React, { useState } from "react";
import PropTypes from "prop-types";
import compose from "recompose/compose";
import Close from "@material-ui/icons/Close";
import DoneAll from "@material-ui/icons/DoneAll";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/styles";
import Typography from "@material-ui/core/Typography";
import Chip from "@material-ui/core/Chip";
import withUnivInfo from "../../../hoc/withUnivInfo";
import RequestParams from "../../../utils/api/RequestParams";
import PaginatedData from "../../common/PaginatedData";
import withNetworkWrapper, {
  NetWrapParam,
} from "../../../hoc/withNetworkWrapper";

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(1),
  },
  chip: {
    margin: theme.spacing(0.5),
  },
  inlineIcon: {
    fontSize: "1em",
    position: "relative",
    top: ".125em",
  },
  itemRoot: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

function Item(props) {
  const { majors, semester, seats, comment, master, doubleDegree } = props;
  const classes = useStyles();

  return (
    <div className={classes.itemRoot}>
      <div>
        <Chip label={semester} color="primary" className={classes.chip} />
        {majors.map((spe) => (
          <Chip
            key={spe}
            label={<b>{spe}</b>}
            className={classes.chip}
            color="secondary"
          />
        ))}
      </div>
      <div>
        {seats ? (
          <>
            <Typography display="inline">Au total,&nbsp;</Typography>
            <Typography color="primary" display="inline">
              {seats}
              &nbsp; place
              {seats > 1 ? "s" : ""}
            </Typography>
            <Typography display="inline">
              &nbsp;
              {seats > 1 ? "étaient/sont disponibles" : "est/disponible"}.
            </Typography>
          </>
        ) : (
          <Typography variant="caption">
            Aucune information n'est disponible concernant le nombre de places
            disponibles.
          </Typography>
        )}
        <div>
          <Typography>
            Master&nbsp;
            {master ? (
              <DoneAll className={classes.inlineIcon} color="primary" />
            ) : (
              <Close className={classes.inlineIcon} color="error" />
            )}
          </Typography>
          <Typography>
            Double diplome&nbsp;
            {doubleDegree ? (
              <DoneAll className={classes.inlineIcon} color="primary" />
            ) : (
              <Close className={classes.inlineIcon} color="error" />
            )}
          </Typography>
        </div>
        <div>
          {comment && (
            <>
              <Typography variant="caption">Commentaire de la DRI :</Typography>
              <Typography>
                <em>{comment}</em>
              </Typography>
            </>
          )}
        </div>
      </div>
    </div>
  );
}

Item.propTypes = {
  doubleDegree: PropTypes.bool.isRequired,
  master: PropTypes.bool.isRequired,
  semester: PropTypes.string.isRequired,
  majors: PropTypes.arrayOf(PropTypes.string).isRequired,
  comment: PropTypes.string,
  seats: PropTypes.number,
};

Item.defaultProps = {
  comment: "",
  seats: null,
};

function renderEl(dataEl) {
  const {
    comment,
    double_degree: doubleDegree,
    is_master_offered: master,
    semester,
    year,
    majors,
    id,
    nb_seats_offered: seats,
  } = dataEl;

  const p = {
    seats,
    id,
    doubleDegree,
    comment,
    master,
    majors: typeof majors === "string" ? majors.split(",") : [],
    semester: `${semester}${year}`,
  };

  return <Item key={p.id} {...p} />;
}

function UniversityOffers({ offers, goToPage }) {
  const classes = useStyles();

  return (
    <Paper className={classes.paper}>
      <Typography variant="h4">Possibilité(s) d'échanges</Typography>
      <Typography variant="caption">
        REX-DRI s'efforce d'être à jour avec l'ENT. Toutefois, seul l'ENT fait
        foi à 100% concernant les possibilités passées et actuelles d'échanges.
        <br />
        Ci-dessous, retrouvez les possibilités d'échanges répertoriées depuis
        juillet 2019.
      </Typography>

      <PaginatedData
        data={offers}
        goToPage={goToPage}
        render={renderEl}
        stepperOnBottom
        stepperOnTop={false}
        EmptyMessageComponent={
          <Typography>
            <em>
              Aucune possibilité (passée ou présente) n'a été enregistrée à ce
              jour.
            </em>
          </Typography>
        }
      />
    </Paper>
  );
}

UniversityOffers.propTypes = {
  offers: PropTypes.object.isRequired,
  goToPage: PropTypes.func.isRequired,
};

const buildParams = (univId, page) =>
  RequestParams.Builder.withQueryParam("university", univId)
    .withQueryParam("page", page)
    .withQueryParam("page_size", 3)
    .build();

const ConnectedComponent = compose(
  withUnivInfo(),
  withNetworkWrapper([
    new NetWrapParam("offers", "all", {
      addDataToProp: "offers",
      params: (props) => buildParams(props.univId, props.page),
      propTypes: {
        univId: PropTypes.number.isRequired,
        page: PropTypes.number.isRequired,
      },
    }),
  ])
)(UniversityOffers);

export default () => {
  // Lifting state up to work around NetworkWrapper limitations
  const [page, goToPage] = useState(1);

  return <ConnectedComponent page={page} goToPage={goToPage} />;
};
