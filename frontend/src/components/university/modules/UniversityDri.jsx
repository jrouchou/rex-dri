import React, { useMemo } from "react";
import PropTypes from "prop-types";
import compose from "recompose/compose";

import Markdown from "../../common/markdown/Markdown";

import ModuleWrapper from "./common/ModuleWrapper";
import ModuleGroupWrapper from "./common/ModuleGroupWrapper";

import UniversityDriForm from "../forms/UniversityDriForm";
import RequestParams from "../../../utils/api/RequestParams";
import withNetworkWrapper, {
  NetWrapParam,
} from "../../../hoc/withNetworkWrapper";
import withUnivInfo from "../../../hoc/withUnivInfo";

function CoreComponent({ rawModelData }) {
  const { comment } = rawModelData;

  return <Markdown source={comment} />;
}

CoreComponent.propTypes = {
  rawModelData: PropTypes.shape({
    comment: PropTypes.string,
  }).isRequired,
};

function UniversityDri({ univId, univDriItems }) {
  const defaultModelData = useMemo(
    () => ({
      universities: [univId],
      importance_level: "-",
    }),
    [univId]
  );

  return (
    <ModuleGroupWrapper
      groupTitle={"Informations émanant de la DRI liées à l'université"}
      formInfo={UniversityDriForm}
      defaultModelData={defaultModelData}
    >
      {univDriItems.map((rawModelData) => (
        <ModuleWrapper
          key={rawModelData.id} // use the id of the model to prevent useless unmount
          buildTitle={(modelData) => modelData.title}
          rawModelData={rawModelData}
          formInfo={UniversityDriForm}
          CoreComponent={CoreComponent}
        />
      ))}
    </ModuleGroupWrapper>
  );
}

UniversityDri.propTypes = {
  univId: PropTypes.string.isRequired,
  univDriItems: PropTypes.array.isRequired,
};

const buildParams = (univId) =>
  RequestParams.Builder.withQueryParam("universities", univId).build();

export default compose(
  withUnivInfo(["countryId"]),
  withNetworkWrapper([
    new NetWrapParam("universityDri", "all", {
      addDataToProp: "univDriItems",
      params: (props) => buildParams(props.univId),
      propTypes: {
        univId: PropTypes.string.isRequired,
      },
    }),
  ])
)(UniversityDri);
