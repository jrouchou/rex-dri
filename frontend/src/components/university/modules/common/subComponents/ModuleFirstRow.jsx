import React from "react";
import IconButton from "@material-ui/core/IconButton";

import VerifiedUserIcon from "@material-ui/icons/VerifiedUser";
import CreateIcon from "@material-ui/icons/Create";
import MoreIcon from "@material-ui/icons/MoreHoriz";

import SettingsBackRestoreIcon from "@material-ui/icons/SettingsBackupRestore";
import Tooltip from "@material-ui/core/Tooltip";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/styles";
import green from "@material-ui/core/colors/green";
import orange from "@material-ui/core/colors/orange";
import red from "@material-ui/core/colors/red";
import MyBadge from "../../../../common/MyBadge";
import getModerationTooltipAndClass from "../moduleWrapperFunctions/getModerationTooltipAndClass";
import getVersionTooltipAndClass from "../moduleWrapperFunctions/getVersionTooltipAndClass";
import getEditTooltipAndClass from "../moduleWrapperFunctions/getEditTooltipAndClass";
import ModuleTitle from "./ModuleTitle";
import UpdateInfo from "./UpdateInfo";
import SimplePopupMenu from "../../../../common/SimplePopupMenu";

const useStyles = makeStyles((theme) => {
  const onDesktops = "@media (min-width:450px)";
  const onMobiles = "@media (max-width:450px)";

  return {
    button: {
      width: theme.spacing(5),
      height: theme.spacing(5),
      // margin: theme.spacing(0.5),
    },
    green: {
      color: green.A700,
    },
    orange: {
      color: orange.A700,
    },
    red: {
      color: red.A700,
    },
    container: {
      display: "flex",
      justifyContent: "space-between",
    },
    itemLeft: {
      paddingBottom: theme.spacing(1),
    },
    itemRight: {
      textAlign: "right",
      [onDesktops]: {
        minWidth: 130,
      },
      [onMobiles]: {
        minWidth: 45,
      },
    },
    desktopsOnly: {
      [onMobiles]: {
        display: "none",
      },
    },
    mobilesOnly: {
      [onDesktops]: {
        display: "none",
      },
    },
  };
});

function ModuleFirstRow(props) {
  const classes = useStyles();

  const { rawModelData, Icon, buildTitle } = props;
  const nbVersions = Math.max(0, rawModelData.nb_versions);
  const hasPendingModeration = rawModelData.has_pending_moderation;

  const {
    user_can_edit: userCanEdit,
    user_can_moderate: userCanModerate,
    versioned,
  } = rawModelData.obj_info;
  const { versionTooltip, versionClass } = getVersionTooltipAndClass(
    nbVersions
  );
  const { moderTooltip, moderClass } = getModerationTooltipAndClass(
    hasPendingModeration,
    userCanEdit
  );
  const { editTooltip, editClass } = getEditTooltipAndClass(
    userCanEdit,
    userCanModerate
  );

  const moderDisabled = moderClass === "disabled" || moderClass === "green";
  const editDisabled = editClass === "disabled";
  const versionDisabled = !versioned || versionClass === "disabled";

  const menuItems = [
    {
      disabled: moderDisabled,
      label: "En attente de modération",
      onClick: () => props.openPendingModerationPanel(),
    },
    {
      disabled: editDisabled,
      label: "Éditer l'élément",
      onClick: () => props.editCurrent(),
    },
    {
      disabled: versionDisabled,
      label: "Précédente(s) version(s)",
      onClick: () => props.openHistoryPanel(),
    },
  ];

  return (
    <div className={classes.container}>
      <div className={classes.itemLeft}>
        <ModuleTitle
          rawModelData={rawModelData}
          buildTitle={buildTitle}
          Icon={Icon}
        />
        <UpdateInfo rawModelData={rawModelData} />
      </div>

      <div className={classes.itemRight}>
        <div className={classes.desktopsOnly}>
          <Tooltip title={moderTooltip} placement="top">
            <div style={{ display: "inline-block" }}>
              &nbsp;
              {/* Needed to fire events for the tooltip when below is disabled! when below is disabled!! */}
              <MyBadge
                badgeContent={hasPendingModeration ? 1 : 0}
                color="secondary"
                minNumber={1}
              >
                <IconButton
                  aria-label="Modération"
                  disabled={moderDisabled}
                  onClick={() => props.openPendingModerationPanel()}
                  className={classes.button}
                >
                  <VerifiedUserIcon className={classes[moderClass]} />
                </IconButton>
              </MyBadge>
            </div>
          </Tooltip>

          <Tooltip title={editTooltip} placement="top">
            <div style={{ display: "inline-block" }}>
              &nbsp;
              {/* Needed to fire events for the tooltip when below is disabled!! */}
              <IconButton
                aria-label="Éditer"
                className={classes.button}
                disabled={editDisabled}
                onClick={() => props.editCurrent()}
              >
                <CreateIcon className={classes[editClass]} />
              </IconButton>
            </div>
          </Tooltip>

          {versioned && (
            <Tooltip title={versionTooltip} placement="top">
              <div style={{ display: "inline-block" }}>
                &nbsp;
                {/* Needed to fire events for the tooltip when below is disabled!! */}
                <MyBadge
                  badgeContent={nbVersions}
                  color="secondary"
                  minNumber={2}
                >
                  <IconButton
                    aria-label="Restorer"
                    disabled={versionDisabled}
                    className={classes.button}
                    onClick={() => props.openHistoryPanel()}
                  >
                    <SettingsBackRestoreIcon
                      className={classes[versionClass]}
                    />
                  </IconButton>
                </MyBadge>
              </div>
            </Tooltip>
          )}
        </div>
        <div className={classes.mobilesOnly}>
          <SimplePopupMenu
            items={menuItems}
            renderHolder={({ onClick }) => (
              <IconButton
                color="secondary"
                onClick={onClick}
                disabled={moderDisabled && editDisabled && versionDisabled}
              >
                <MoreIcon />
              </IconButton>
            )}
          />
        </div>
      </div>
    </div>
  );
}

ModuleFirstRow.propTypes = {
  openHistoryPanel: PropTypes.func.isRequired,
  editCurrent: PropTypes.func.isRequired,
  openPendingModerationPanel: PropTypes.func.isRequired,
  rawModelData: PropTypes.object.isRequired,
  Icon: PropTypes.object,
  buildTitle: PropTypes.func.isRequired,
};

ModuleFirstRow.defaultProps = {
  Icon: undefined,
};

export default React.memo(ModuleFirstRow);
