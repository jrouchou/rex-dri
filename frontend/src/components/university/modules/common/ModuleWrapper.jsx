/* eslint-disable react/jsx-curly-newline */
import React, { useCallback } from "react";
import PropTypes from "prop-types";

import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/styles";
import ModuleTitle from "./subComponents/ModuleTitle";
import ModuleFirstRow from "./subComponents/ModuleFirstRow";
import UsefulLinks from "./subComponents/UsefulLinks";
import AlertService from "../../../../services/AlertService";
import useEditor from "../../../../hooks/useEditor";
import FormInfo from "../../../../utils/editionRelated/FormInfo";
import FullScreenDialogService from "../../../../services/FullScreenDialogService";
import History from "../../../edition/History";
import PendingModeration from "../../../edition/PendingModeration";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(1),
    // marginBottom: theme.spacing(2)
  },
}));

function alertThereIsSomethingPendingModeration(
  openPendingModeration,
  openEditor
) {
  AlertService.open({
    info: false,
    title: "Une version est en attente de modération",
    description:
      "Vous vous apprêter à éditer le module tel qu'il est présenté sur la page principale. Toutefois, il conseillé d'éditer le module à partir de la version en attente de modération car cette dernière sera écrasée par votre contribution.",
    agreeText: "Ok, je vais voir la version en attente de modération",
    disagreeText:
      "Je ne veux pas me baser sur le travail en attente de modération",
    handleResponse: (agree) => {
      if (agree) {
        openPendingModeration();
      } else {
        openEditor(true);
      }
    },
    multilineButtons: true,
  });
}

// TODO clean render methods

/**
 * Wrapper around modules to handle in a standard way the edition/moderation/history version;
 * displaying of certain attributes, etc.
 */
function ModuleWrapper({
  formInfo,
  rawModelData,
  onSave,
  buildTitle,
  CoreComponent,
  Icon,
}) {
  const classes = useStyles();

  const openEditorTmp = useEditor(formInfo, (somethingWasSaved = false) => {
    if (somethingWasSaved) {
      onSave();
    }
  });

  function renderCore(modelData) {
    return (
      <>
        <CoreComponent rawModelData={modelData} />
        <UsefulLinks usefulLinks={modelData.useful_links} />
      </>
    );
  }

  function renderTitle(modelData) {
    return <ModuleTitle rawModelData={modelData} buildTitle={buildTitle} />;
  }

  const openHistory = useCallback(() => {
    FullScreenDialogService.openDialog(
      <History
        modelInfo={{
          contentTypeId: rawModelData.content_type_id,
          id: rawModelData.id,
        }}
        editFromVersion={openEditorTmp}
        rawModelDataEx={rawModelData}
        renderCore={renderCore}
        renderTitle={renderTitle}
      />
    );
  }, [rawModelData, openEditorTmp]);

  const openPendingModeration = useCallback(() => {
    FullScreenDialogService.openDialog(
      <PendingModeration
        renderTitle={renderTitle}
        renderCore={renderCore}
        modelInfo={{
          contentTypeId: rawModelData.content_type_id,
          id: rawModelData.id,
        }}
        editFromPendingModeration={openEditorTmp}
        userCanModerate={rawModelData.obj_info.user_can_moderate}
      />
    );
  }, [rawModelData, openEditorTmp]);

  /**
   * Function to open the editor panels.
   * It also checks that there is no model pending moderation
   *
   * @param {boolean} [ignorePendingModeration=false]
   */
  const openEditor = useCallback(
    (ignorePendingModeration = false) => {
      if (ignorePendingModeration || !rawModelData.has_pending_moderation) {
        openEditorTmp(rawModelData);
      } else {
        alertThereIsSomethingPendingModeration(
          openPendingModeration,
          openEditor
        );
      }
    },
    [rawModelData]
  );

  return (
    <>
      <Paper className={classes.root} square>
        <ModuleFirstRow
          editCurrent={openEditor}
          openHistoryPanel={openHistory}
          openPendingModerationPanel={openPendingModeration}
          rawModelData={rawModelData}
          buildTitle={buildTitle}
          Icon={Icon}
        />
        {renderCore(rawModelData)}
      </Paper>
    </>
  );
}

ModuleWrapper.propTypes = {
  formInfo: PropTypes.instanceOf(FormInfo).isRequired,
  rawModelData: PropTypes.object.isRequired,
  buildTitle: PropTypes.func,
  CoreComponent: PropTypes.func.isRequired,
  Icon: PropTypes.object,
  onSave: PropTypes.func,
};

ModuleWrapper.defaultProps = {
  buildTitle: () => null,
  onSave: () => {},
  Icon: undefined,
};

export default ModuleWrapper;
