import React from "react";
import PropTypes from "prop-types";

import Typography from "@material-ui/core/Typography";
import ModuleWrapper from "./common/ModuleWrapper";

import {
  CountryTaggedItemForm,
  UniversityTaggedItemForm,
} from "../forms/TaggedItemEditorForm";
import TruncatedMarkdown from "../../common/markdown/TruncatedMarkdown";

function CoreComponent({ rawModelData }) {
  const { comment } = rawModelData;
  return (
    <>
      {comment.length === 0 ? (
        <Typography variant="caption">
          <em>Aucune information n'est disponible... contribuez ! 😉</em>
        </Typography>
      ) : (
        <TruncatedMarkdown source={comment} />
      )}
    </>
  );
}

CoreComponent.propTypes = {
  rawModelData: PropTypes.shape({
    comment: PropTypes.string,
  }).isRequired,
};

function TaggedItem(props) {
  return (
    <ModuleWrapper
      buildTitle={() => props.title}
      rawModelData={props.data}
      formInfo={
        props.variant === "country"
          ? CountryTaggedItemForm
          : UniversityTaggedItemForm
      }
      CoreComponent={CoreComponent}
      onSave={props.invalidateGroup}
      Icon={props.Icon}
    />
  );
}

TaggedItem.propTypes = {
  data: PropTypes.shape({
    comment: PropTypes.string,
    tag: PropTypes.string.isRequired,
    useful_links: PropTypes.array.isRequired,
  }).isRequired,
  title: PropTypes.string.isRequired,
  variant: PropTypes.oneOf(["country", "university"]).isRequired,
  invalidateGroup: PropTypes.func.isRequired,
  Icon: PropTypes.object.isRequired,
};

export default TaggedItem;
