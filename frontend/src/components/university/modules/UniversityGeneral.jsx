import React from "react";
import PropTypes from "prop-types";
import compose from "recompose/compose";

import Typography from "@material-ui/core/Typography";

import Grid from "@material-ui/core/Grid";
import Divider from "@material-ui/core/Divider";
import PhotoSizeSelectActualIcon from "@material-ui/icons/PhotoSizeSelectActual";
import TextLink from "../../common/TextLink";
import ModuleWrapper from "./common/ModuleWrapper";

import UniversityGeneralForm from "../forms/UniversityGeneralForm";
import withUnivInfo from "../../../hoc/withUnivInfo";
import RequestParams from "../../../utils/api/RequestParams";
import withNetworkWrapper, {
  NetWrapParam,
} from "../../../hoc/withNetworkWrapper";

function CoreComponent({ rawModelData, cityName, countryName }) {
  const { name, acronym, logo, website } = rawModelData;

  return (
    <Grid container spacing={2} direction="row">
      <Grid
        item
        xs={4}
        style={{
          display: "inline-flex",
          alignItems: "center",
        }}
      >
        {logo ? (
          <img
            style={{ width: "100%" }}
            src={logo}
            alt="logo of the university"
          />
        ) : (
          <PhotoSizeSelectActualIcon
            style={{
              marginLeft: "auto",
              marginRight: "auto",
              width: "60%",
              height: "60%",
            }}
          />
        )}
      </Grid>
      <Grid item xs>
        <Typography variant="h5">{name}</Typography>
        <Typography variant="h6">{acronym}</Typography>
        <Divider />
        <Typography variant="subtitle1">
          {cityName},&nbsp;{countryName}
        </Typography>
        <Typography variant="body2">
          Site internet&nbsp;:&nbsp;
          {website ? (
            <TextLink href={website}>{website}</TextLink>
          ) : (
            <em>Non connu.</em>
          )}
        </Typography>
      </Grid>
    </Grid>
  );
}

CoreComponent.propTypes = {
  cityName: PropTypes.string.isRequired,
  countryName: PropTypes.string.isRequired,
  rawModelData: PropTypes.shape({
    comment: PropTypes.string,
    name: PropTypes.string.isRequired,
    acronym: PropTypes.string,
    logo: PropTypes.string,
    website: PropTypes.string,
  }).isRequired,
};

function UniversityGeneral({ university, country, city }) {
  return (
    <ModuleWrapper
      buildTitle={() => "Présentation"}
      rawModelData={university}
      formInfo={UniversityGeneralForm}
      CoreComponent={(props) => (
        <CoreComponent {...props} cityName={city} countryName={country.name} />
      )}
    />
  );
}

UniversityGeneral.propTypes = {
  city: PropTypes.string.isRequired,
  country: PropTypes.object.isRequired,
  university: PropTypes.object.isRequired,
};

const buildParams = (univId) => RequestParams.Builder.withId(univId).build();

export default compose(
  withUnivInfo(["city", "country"]),
  withNetworkWrapper([
    new NetWrapParam("universities", "one", {
      addDataToProp: "university",
      params: (props) => buildParams(props.univId),
      propTypes: {
        univId: PropTypes.number.isRequired,
      },
    }),
  ])
)(UniversityGeneral);
