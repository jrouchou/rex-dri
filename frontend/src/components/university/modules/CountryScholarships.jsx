import React, { useMemo } from "react";
import PropTypes from "prop-types";
import compose from "recompose/compose";

import { makeStyles } from "@material-ui/styles";

import ModuleWrapper from "./common/ModuleWrapper";
import ModuleGroupWrapper from "./common/ModuleGroupWrapper";

import CountryScholarshipForm from "../forms/CountryScholarshipForm";
import withUnivInfo from "../../../hoc/withUnivInfo";
import RequestParams from "../../../utils/api/RequestParams";
import withNetworkWrapper, {
  NetWrapParam,
} from "../../../hoc/withNetworkWrapper";
import ScholarshipCore, {
  getScholarshipDefaultModelData,
} from "./common/ScholarshipCore";

const useStyles = makeStyles((theme) => ({
  item: {
    marginBottom: theme.spacing(2),
    marginTop: theme.spacing(2),
  },
}));

function CountryScholarships({ countryScholarshipsItems, countryId, country }) {
  const classes = useStyles();

  const defaultModelData = useMemo(
    () => ({
      ...getScholarshipDefaultModelData(),
      countries: [countryId],
    }),
    [countryId]
  );

  return (
    <ModuleGroupWrapper
      formInfo={CountryScholarshipForm}
      groupTitle={`Bourses liées au pays (${country.name})`}
      defaultModelData={defaultModelData}
    >
      {countryScholarshipsItems.map((rawModelData) => (
        <div key={rawModelData.id} className={classes.item}>
          <ModuleWrapper
            formInfo={CountryScholarshipForm}
            buildTitle={(modelData) => modelData.title}
            rawModelData={rawModelData}
            CoreComponent={ScholarshipCore}
          />
        </div>
      ))}
    </ModuleGroupWrapper>
  );
}

CountryScholarships.propTypes = {
  countryId: PropTypes.string.isRequired,
  country: PropTypes.object.isRequired,
  countryScholarshipsItems: PropTypes.array.isRequired,
};

const buildParams = (countryId) =>
  RequestParams.Builder.withQueryParam("countries", countryId).build();

export default compose(
  withUnivInfo(["countryId", "country"]),
  withNetworkWrapper([
    new NetWrapParam("countryScholarships", "all", {
      addDataToProp: "countryScholarshipsItems",
      params: (props) => buildParams(props.countryId),
      propTypes: {
        countryId: PropTypes.string.isRequired,
      },
    }),
  ])
)(CountryScholarships);
