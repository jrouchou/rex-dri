import React from "react";
import PropTypes from "prop-types";
import compose from "recompose/compose";
import ModuleWrapper from "./common/ModuleWrapper";
import SharedUnivFeedbackForm from "../forms/SharedUnivFeedbackForm";
import withUnivInfo from "../../../hoc/withUnivInfo";
import RequestParams from "../../../utils/api/RequestParams";
import TruncatedMarkdown from "../../common/markdown/TruncatedMarkdown";
import withNetworkWrapper, {
  NetWrapParam,
} from "../../../hoc/withNetworkWrapper";

function CoreComponent({ rawModelData }) {
  const { comment } = rawModelData;
  return <TruncatedMarkdown truncateFromLength={1000} source={comment} />;
}

CoreComponent.propTypes = {
  rawModelData: PropTypes.shape({
    comment: PropTypes.string,
  }).isRequired,
};

function SharedUnivFeedback({ feedback }) {
  return (
    <ModuleWrapper
      buildTitle={() =>
        "Commentaire partagé par celles et ceux qui sont partis"
      }
      rawModelData={feedback[0]}
      formInfo={SharedUnivFeedbackForm}
      CoreComponent={CoreComponent}
    />
  );
}

SharedUnivFeedback.propTypes = {
  feedback: PropTypes.array.isRequired,
};

const buildParams = (univId) =>
  RequestParams.Builder.withQueryParam("university", univId).build();

export default compose(
  withUnivInfo(),
  withNetworkWrapper([
    new NetWrapParam("sharedUnivFeedbacks", "all", {
      addDataToProp: "feedback",
      params: (props) => buildParams(props.univId),
      propTypes: {
        univId: PropTypes.number.isRequired,
      },
    }),
  ])
)(SharedUnivFeedback);
