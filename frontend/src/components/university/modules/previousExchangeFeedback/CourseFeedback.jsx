import React from "react";

import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/styles";
import Typography from "@material-ui/core/Typography";
import Chip from "@material-ui/core/Chip";
import Divider from "@material-ui/core/Divider";
import ChillIcon from "@material-ui/icons/LocalBar";
import HardCoreIcon from "@material-ui/icons/FitnessCenter";
import TruncatedMarkdown from "../../../common/markdown/TruncatedMarkdown";
import MetricFeedback from "../../../common/MetricFeedback";
import TextLink from "../../../common/TextLink";
import { GridColumn, GridLine } from "./common";
import LanguageService from "../../../../services/data/LanguageService";

const useStyles = makeStyles((theme) => ({
  feedbackHeaderText: {
    textAlign: "center",
  },
  smallCard: {
    marginBottom: theme.spacing(2),
  },
  divider: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(3),
  },
  chip: {
    marginRight: theme.spacing(1),
  },
}));

const metricProps = {
  min: -5,
  max: 5,
  height: 12,
  width: 100,
};

export function CourseFeedbackCore(props) {
  const classes = useStyles();
  const {
    renderUtcInfo,
    courseCode,
    courseTitle,
    courseCategory,
    courseProfile,
    courseLink,
    courseCredits,
    languageCode,
    adequation,
    workingDose,
    followingEase,
    comment,
    wouldRecommend,
    untouched,
  } = props;
  const languageName = LanguageService.getLanguageName(languageCode);
  const secondaryChipLabel = `${courseCategory ? ` ${courseCategory}` : ""}${
    courseProfile ? ` — ${courseProfile}` : ""
  }`;

  return (
    <div className={classes.smallCard}>
      {courseCode && (
        <Chip label={courseCode} color="default" className={classes.chip} />
      )}
      {renderUtcInfo && (
        <>
          <Chip
            label={secondaryChipLabel}
            color="primary"
            variant="outlined"
            className={classes.chip}
          />
          {courseCredits && (
            <Chip
              label={`${courseCredits} ECTS`}
              color="secondary"
              variant="outlined"
            />
          )}
          <div />
          {courseTitle && (
            <Typography variant="h6" display="inline">
              {courseTitle}
            </Typography>
          )}
          {courseLink && (
            <>
              &nbsp;
              <TextLink href={courseLink}>
                <Typography variant="caption">(lien externe)</Typography>
              </TextLink>
            </>
          )}
        </>
      )}

      {languageName && (
        <Typography>
          Cours enseigné en&nbsp;
          {LanguageService.getLanguageName(languageCode)}.
        </Typography>
      )}
      {!untouched && (
        <>
          <GridLine>
            <GridColumn>
              <Typography variant="h6" className={classes.feedbackHeaderText}>
                Adéquation
              </Typography>
              <MetricFeedback {...metricProps} value={adequation} />
            </GridColumn>
            <GridColumn>
              <Typography variant="h6" className={classes.feedbackHeaderText}>
                Recommandé ?
              </Typography>
              <MetricFeedback {...metricProps} value={wouldRecommend} />
            </GridColumn>
            <GridColumn>
              <Typography variant="h6" className={classes.feedbackHeaderText}>
                Facilité pour suivre
              </Typography>
              <MetricFeedback {...metricProps} value={followingEase} />
            </GridColumn>
            <GridColumn>
              <Typography variant="h6" className={classes.feedbackHeaderText}>
                Charge de travail
              </Typography>
              <MetricFeedback
                {...metricProps}
                type="two-negatives"
                value={workingDose}
                showBarIcons
                LeftBarIcon={ChillIcon}
                RightBarIcon={HardCoreIcon}
              />
            </GridColumn>
          </GridLine>

          <Divider />
          <Typography>Commentaire</Typography>
          <TruncatedMarkdown source={comment === null ? "" : comment} />
        </>
      )}
    </div>
  );
}

CourseFeedbackCore.propTypes = {
  courseCode: PropTypes.string,
  courseCategory: PropTypes.string,
  courseProfile: PropTypes.string,
  courseTitle: PropTypes.string,
  // Not really interesting based on the data from the DB, so we are not showing it.
  courseTshProfile: PropTypes.string,
  courseCredits: PropTypes.number,
  courseLink: PropTypes.string,
  languageCode: PropTypes.string,
  adequation: PropTypes.number,
  wouldRecommend: PropTypes.number,
  workingDose: PropTypes.number,
  followingEase: PropTypes.number,
  comment: PropTypes.string,
  untouched: PropTypes.bool.isRequired,
  renderUtcInfo: PropTypes.bool,
};

CourseFeedbackCore.defaultProps = {
  renderUtcInfo: true,
  courseCode: "",
  courseCategory: "",
  courseProfile: "",
  courseTitle: "",
  courseTshProfile: "",
  courseCredits: null,
  courseLink: "",
  languageCode: "",
  adequation: null,
  wouldRecommend: null,
  workingDose: null,
  followingEase: null,
  comment: "",
};

function CourseFeedback(props) {
  const classes = useStyles();
  const { exchangeCourses } = props;
  const coursesData = exchangeCourses.map((course) => ({
    courseCode: course.code,
    courseCredits: parseFloat(course.ects),
    courseCategory: course.category,
    courseProfile: course.profile,
    courseLink: course.link,
    courseTitle: course.title,
    courseTshProfile: course.tsh_profile,
    untouched: course.course_feedback.untouched,
    languageCode: course.course_feedback.language,
    adequation: course.course_feedback.adequation,
    wouldRecommend: course.course_feedback.would_recommend,
    workingDose: course.course_feedback.working_dose,
    followingEase: course.course_feedback.following_ease,
    comment: course.course_feedback.comment,
    id: course.course_feedback.id,
  }));
  coursesData.sort((a, b) => {
    if (typeof a.courseCode === "string" && typeof b.courseCode === "string")
      return a.courseCode.localeCompare(b.courseCode);
    // if no valid course code, we try the course title
    if (typeof a.courseTitle === "string" && typeof b.courseTitle === "string")
      return a.courseTitle.localeCompare(b.courseTitle);
    return 0;
  });

  return (
    <>
      <Divider className={classes.divider} />
      {coursesData.map((el) => (
        <React.Fragment key={el.id}>
          <CourseFeedbackCore {...el} />
          <div className={classes.divider} />
        </React.Fragment>
      ))}
    </>
  );
}

CourseFeedback.propTypes = {
  exchangeCourses: PropTypes.array.isRequired,
};

export default CourseFeedback;
