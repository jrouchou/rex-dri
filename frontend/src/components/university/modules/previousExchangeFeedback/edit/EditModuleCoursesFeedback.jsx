import React from "react";
import PropTypes from "prop-types";
import compose from "recompose/compose";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/styles";
import RequestParams from "../../../../../utils/api/RequestParams";
import CourseFeedbackForm from "./CourseFeedbackForm";
import ModuleWrapper from "../../common/ModuleWrapper";
import { CourseFeedbackCore } from "../CourseFeedback";
import CustomLink from "../../../../common/CustomLink";
import APP_ROUTES from "../../../../../config/appRoutes";
import withNetworkWrapper, {
  NetWrapParam,
} from "../../../../../hoc/withNetworkWrapper";

import { useApiInvalidateAll } from "../../../../../hooks/wrappers/api";

function CoreComponent({ rawModelData }) {
  const p = rawModelData;

  return (
    <CourseFeedbackCore
      courseCode={p.course_code}
      followingEase={p.following_ease}
      workingDose={p.working_dose}
      adequation={p.adequation}
      comment={p.comment}
      languageCode={p.language}
      renderUtcInfo={false}
      untouched={p.untouched}
      wouldRecommend={p.would_recommend}
    />
  );
}

CoreComponent.propTypes = {
  rawModelData: PropTypes.shape({
    course_code: PropTypes.string,
    following_ease: PropTypes.number,
    working_dose: PropTypes.number,
    adequation: PropTypes.number,
    comment: PropTypes.string,
    language: PropTypes.string,
    untouched: PropTypes.bool.isRequired,
    would_recommend: PropTypes.number,
  }).isRequired,
};

const useStyles = makeStyles((theme) => ({
  wrapper: {
    marginTop: theme.spacing(2),
    magrinBottom: theme.spacing(2),
  },
}));

function EditModuleCoursesFeedback({ courseFeedbacks }) {
  const classes = useStyles();

  // also invalidate all to make sure all the site pages are consistent
  const invalidateExchangeFeedbacks = useApiInvalidateAll("exchangeFeedbacks");

  courseFeedbacks.sort((a, b) => {
    if (
      typeof a.course_code === "string" &&
      typeof b.course_code === "string"
    ) {
      return a.course_code.localeCompare(b.course_code);
    }
    // if no valid course code, we try the course title
    if (
      typeof a.course_title === "string" &&
      typeof b.course_title === "string"
    ) {
      return a.course_title.localeCompare(b.course_title);
    }

    return 0;
  });

  return (
    <>
      <Typography>
        S'il manque des cours dans la liste ci-dessous, vous pouvez demander à
        recharger les données de l'ENT depuis &nbsp;
      </Typography>
      <Typography>
        <CustomLink
          to={APP_ROUTES.myExchanges}
          Component={Typography}
          componenentProps={{ color: "primary" }}
        >
          la page de sélection de l'un de vos échanges.&nbsp;
        </CustomLink>
      </Typography>
      <Typography>
        <em>
          Si certains cours sont en trop par rapport à l'ENT, ils devraient être
          invalidés du côté de REX-DRI sous 24h.
        </em>
      </Typography>
      {courseFeedbacks.map((el) => (
        <div key={el.id} className={classes.wrapper}>
          <ModuleWrapper
            buildTitle={() =>
              `Avis sur ${el.course_title} ${
                el.course_code ? `(${el.course_code})` : ""
              }`
            }
            rawModelData={el}
            formInfo={CourseFeedbackForm}
            CoreComponent={CoreComponent}
            onSave={invalidateExchangeFeedbacks}
          />
        </div>
      ))}
    </>
  );
}

EditModuleCoursesFeedback.propTypes = {
  courseFeedbacks: PropTypes.array.isRequired,
};

const buildParams = (exchangeId) =>
  RequestParams.Builder.withQueryParam("course__exchange", exchangeId).build();

export default compose(
  withNetworkWrapper([
    new NetWrapParam("courseFeedbacks", "all", {
      addDataToProp: "courseFeedbacks",
      params: (props) => buildParams(props.exchangeId),
      propTypes: {
        exchangeId: PropTypes.number.isRequired,
      },
    }),
  ])
)(EditModuleCoursesFeedback);
