import React, { useCallback, useEffect, useState } from "react";
import PropTypes from "prop-types";
import withWidth, { isWidthUp } from "@material-ui/core/withWidth";
import compose from "recompose/compose";

import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import StarsIcon from "@material-ui/icons/Stars";
import AttachMoneyIcon from "@material-ui/icons/AttachMoney";
import HistoryIcon from "@material-ui/icons/History";
import OfflineBoltIcon from "@material-ui/icons/OfflineBolt";
import { makeStyles } from "@material-ui/styles";
import CoverGallery from "../common/CoverGallery/CoverGallery";

import GeneralInfoTab from "./tabs/GeneralInfoTab";
import PreviousExchangesTab from "./tabs/PreviousExchangesTab";
import ScholarshipsTab from "./tabs/ScholarshipsTab";
import { PaddedPageDiv } from "../pages/shared";
import { appBarHeight, siteMaxWidth } from "../../config/sharedStyles";
import TipsAndTricksTab from "./tabs/TipsAndTricksTab";
import NavigationService from "../../services/NavigationService";

const useStyles = makeStyles((theme) => ({
  tabBar: {
    minHeight: theme.spacing(9),
    [`@media (min-width:${siteMaxWidth()}px)`]: {
      top: appBarHeight(theme),
    },
  },
}));

/**
 * Component that handles all the elements of the university page
 */
function UniversityTemplate({ tabName, width, univId }) {
  const classes = useStyles();

  const [selectedTab, setSelectedTab] = useState(tabName);

  const handleChange = useCallback(
    (event, newTabName) => {
      setSelectedTab(newTabName);
      NavigationService.goToUniversityTab(univId, newTabName);
    },
    [univId]
  );

  const tabIsNotValid = tabName === "" || typeof tabName === "undefined";

  useEffect(() => {
    if (tabIsNotValid) NavigationService.goToUniversityTab(univId, "general");
  }, [univId, tabName]);

  if (tabIsNotValid) return <></>;

  const scroll = !isWidthUp("sm", width);

  return (
    <>
      <CoverGallery />
      <AppBar position="sticky" color="default" className={classes.tabBar}>
        <Tabs
          value={selectedTab}
          onChange={handleChange}
          variant={scroll ? "scrollable" : "standard"}
          centered={!scroll}
          scrollButtons="on"
          indicatorColor="secondary"
          textColor="secondary"
        >
          <Tab label="Généralités" value="general" icon={<StarsIcon />} />
          <Tab
            label="Précédents départs"
            value="previous-exchanges"
            icon={<HistoryIcon />}
          />
          <Tab
            label="Bourses"
            value="scholarships"
            icon={<AttachMoneyIcon />}
          />
          <Tab
            label="Tips & Tricks"
            value="tips-tricks"
            icon={<OfflineBoltIcon />}
          />
        </Tabs>
      </AppBar>
      <PaddedPageDiv>
        {tabName === "general" && <GeneralInfoTab />}
        {tabName === "previous-exchanges" && <PreviousExchangesTab />}
        {tabName === "scholarships" && <ScholarshipsTab />}
        {tabName === "tips-tricks" && <TipsAndTricksTab />}
      </PaddedPageDiv>
    </>
  );
}

UniversityTemplate.propTypes = {
  width: PropTypes.string.isRequired,
  univId: PropTypes.number.isRequired,
  tabName: PropTypes.string,
};

UniversityTemplate.defaultProps = {
  tabName: "",
};

export default compose(withWidth({ noSSR: true }))(UniversityTemplate);
