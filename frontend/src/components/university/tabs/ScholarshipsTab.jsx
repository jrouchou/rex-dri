import React from "react";

import Grid from "@material-ui/core/Grid";

import UniversityScholarships from "../modules/UniversityScholarships";
import CountryScholarships from "../modules/CountryScholarships";

/**
 * Tab on the university page containing information related to scholarship
 */
function ScholarshipsTab() {
  return (
    <>
      <Grid container spacing={3}>
        <Grid item md={6} xs={12}>
          <UniversityScholarships />
        </Grid>
        <Grid item md={6} xs={12}>
          <CountryScholarships />
        </Grid>
      </Grid>
    </>
  );
}

ScholarshipsTab.propTypes = {};

export default ScholarshipsTab;
