import React from "react";

import { makeStyles } from "@material-ui/styles";
import UniversityGeneral from "../modules/UniversityGeneral";
import UniversitySemestersDates from "../modules/UniversitySemestersDates";
import UniversityOffers from "../modules/UniversityOffers";
// import UniversityDri from "../modules/UniversityDri";
// import CountryDri from "../modules/CountryDri";
import SharedUnivFeedback from "../modules/SharedUnivFeedback";

const useStyles = makeStyles((theme) => ({
  wideRoot: {
    display: "flex",
    width: "100%",
    marginLeft: "auto",
    marginRight: "auto",
    [theme.breakpoints.down("sm")]: {
      display: "none",
    },
  },
  wideLeftCol: {
    flexGrow: 8,
    flexShrink: 4,
    marginRight: theme.spacing(2),
  },
  wideRightCol: {
    flexGrow: 6,
    flexShrink: 6,
  },
  wideColItem: {
    marginBottom: theme.spacing(2),
  },
  smallRoot: {
    width: "100%",
    [theme.breakpoints.up("md")]: {
      display: "none",
    },
  },
  smallColItem: {
    marginBottom: theme.spacing(2),
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  spacer: {
    marginBottom: theme.spacing(2),
  },
}));

/**
 * Main tab of the university page
 */
function GeneralInfoTab() {
  const classes = useStyles();
  // We render both full screen and small screen layout to prevent components from unmounting.
  // And causing trouble elsewhere :)
  // the hiding is done with display none.
  return (
    <>
      <div className={classes.wideRoot}>
        <div className={classes.wideLeftCol}>
          {[UniversityGeneral, SharedUnivFeedback].map((Comp, idx) => (
            // eslint-disable-next-line react/no-array-index-key
            <div key={idx} className={classes.wideColItem}>
              <Comp />
            </div>
          ))}
        </div>
        <div className={classes.wideRightCol}>
          {[UniversityOffers, UniversitySemestersDates].map((Comp, idx) => (
            // eslint-disable-next-line react/no-array-index-key
            <div key={idx} className={classes.wideColItem}>
              <Comp />
            </div>
          ))}
        </div>
      </div>
      <div className={classes.smallRoot}>
        <div className={classes.spacer} />
        {[
          UniversityGeneral,
          UniversityOffers,
          SharedUnivFeedback,
          UniversitySemestersDates,
        ].map((Comp, idx) => (
          // eslint-disable-next-line react/no-array-index-key
          <div key={idx} className={classes.smallColItem}>
            <Comp />
          </div>
        ))}
      </div>
    </>
  );
}

GeneralInfoTab.propTypes = {};

export default GeneralInfoTab;
