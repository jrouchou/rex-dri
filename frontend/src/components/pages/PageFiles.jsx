import React from "react";
import PropTypes from "prop-types";
import Paper from "@material-ui/core/Paper";
import { compose } from "recompose";
import { Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import RequestParams from "../../utils/api/RequestParams";
import Pictures from "../user/Pictures";
import withNetworkWrapper, { NetWrapParam } from "../../hoc/withNetworkWrapper";

const useStyles = makeStyles((theme) => ({
  paper: theme.myPaper,
}));

function getUserIdFromUrl(match) {
  return match.params.userId;
}

/**
 * WARNING BETA files & padding
 * Page that lists the files available
 */
// eslint-disable-next-line no-unused-vars
function PageFiles({ pictures, files }) {
  const classes = useStyles();

  return (
    <>
      <Paper className={classes.paper}>
        <Typography variant="h3">Photos</Typography>
        <Pictures pictures={pictures} onSomethingWasSaved={() => {}} />
      </Paper>
      {/* <Paper style={theme.myPaper}> */}
      {/*  <Typography variant={"h3"}>Fichiers</Typography> */}
      {/* </Paper> */}
    </>
  );
}

PageFiles.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      userId: PropTypes.string.isRequired,
    }),
  }).isRequired,
  pictures: PropTypes.array.isRequired,
  files: PropTypes.array.isRequired,
};

const buildPictureParams = (match) =>
  RequestParams.Builder.withQueryParam(
    "owner",
    getUserIdFromUrl(match)
  ).build();

const buildFilesParams = (match) =>
  RequestParams.Builder.withQueryParam(
    "owner",
    getUserIdFromUrl(match)
  ).build();

export default compose(
  withNetworkWrapper([
    new NetWrapParam("pictures", "all", {
      addDataToProp: "pictures",
      params: (props) => buildPictureParams(props.match),
      propTypes: {
        match: PropTypes.shape({
          params: PropTypes.shape({
            userId: PropTypes.string.isRequired,
          }),
        }).isRequired,
      },
    }),
    new NetWrapParam("files", "all", {
      addDataToProp: "files",
      params: (props) => buildFilesParams(props.match),
      propTypes: {
        match: PropTypes.shape({
          params: PropTypes.shape({
            userId: PropTypes.string.isRequired,
          }),
        }).isRequired,
      },
    }),
  ])
)(PageFiles);
