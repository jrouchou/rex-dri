import React from "react";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import CancelIcon from "@material-ui/icons/Cancel";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import Fab from "@material-ui/core/Fab";
import Grid from "@material-ui/core/Grid";
import Divider from "@material-ui/core/Divider";
import { compose } from "recompose";
import { makeStyles } from "@material-ui/styles";
import UserInfo from "../user/UserInfo";
import { withErrorBoundary } from "../common/ErrorBoundary";
import { withPaddedPaper } from "./shared";
import APP_ROUTES from "../../config/appRoutes";
import { CURRENT_USER_ID } from "../../config/user";

const useStyle = makeStyles((theme) => ({
  header: {
    textAlign: "right",
  },
  spacer: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(1),
  },
  inlineIcon: {
    fontSize: "1em",
    position: "relative",
    top: ".125em",
  },
}));

/**
 * Component corresponding to the page with user information
 */
function PageUser(props) {
  function getUserIdFromUrl() {
    return props.match.params.userId;
  }

  const classes = useStyle();
  const requestedUserId = getUserIdFromUrl();

  if (requestedUserId === "me") {
    props.history.push(APP_ROUTES.forUser(CURRENT_USER_ID));
    return <></>;
  }

  return (
    <>
      <Grid
        container
        direction="row"
        justify="flex-end"
        alignItems="flex-start"
      >
        <Fab
          variant="round"
          size="large"
          color="primary"
          className={classes.header}
        >
          #{requestedUserId}
        </Fab>
      </Grid>

      <Typography variant="body2">
        Les éléments marqués d'un « tick » (
        <CheckCircleIcon color="primary" className={classes.inlineIcon} />
        ) sont visibles par les autres utilisateurs.
        <br />
        Les éléments marqués d'une croix (
        <CancelIcon color="disabled" className={classes.inlineIcon} />
        ) ne sont jamais visibles par les autres utilisateurs, sauf
        administrateurs.
        <br />
        Ces réglages peuvent être changés lors de l'édition: &nbsp;
        <b>
          toutefois, le pseudo et l'identifiant unique seront toujours visible
        </b>
        .
      </Typography>

      <div className={classes.spacer} />

      <Divider />

      <div className={classes.spacer} />

      <UserInfo userId={requestedUserId} />
    </>
  );
}

PageUser.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      userId: PropTypes.string.isRequired,
    }),
  }).isRequired,
  history: PropTypes.object.isRequired,
};

export default compose(withPaddedPaper(), withErrorBoundary())(PageUser);
