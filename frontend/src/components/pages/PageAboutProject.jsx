import React from "react";
import Typography from "@material-ui/core/Typography";
import { compose } from "recompose";
import Markdown from "../common/markdown/Markdown";
import { withErrorBoundary } from "../common/ErrorBoundary";
import { withPaddedPaper } from "./shared";

const source = `
# Genèse

La plateforme REX-DRI est née de la volonté d'un ex-représentant étudiant 
au CÉVU et d'une large concertation. Elle a ouvert ses portes à l'été 2019 🎉.

À ce jour, c'est le fruit d'un paquet d'heures de travail bénévole, d'une PR, et du soutien
technique de la DSI.

L'objectif est, et a toujours été, de faciliter les mobilités sortantes en proposant
une plateforme d'information faisant la part belle à la contribution et la mise à disposition 
de données issues de l'ENT. Le tout dans le respect du RGPD.


Aujourd'hui, la plateforme est sous la responsabilité du SIMDE ❤


# Contribuer

REX-DRI n'a pas vocation à être immobilisée pour de longues années.
Au contraire même ! Si vous avez envie de mettre la main à la pâte pour l'améliorer et proposer une
expérience toujours plus stable, performante ou complète, il y a moult possibilités autour de cette pépite Open Source.

* [Ouvrir une issue](https://gitlab.utc.fr/rex-dri/rex-dri/issues) pour remonter un problème 
ou faire une demande de fonctionnalité.
* Résoudre des issues, bénévolement ou au travers d'une PR.

**Toute aide est la bienvenue, c'est un projet très riche !**

Dans tous les cas, tout se passe sur [le GitLab de l'UTC](https://gitlab.utc.fr/rex-dri/rex-dri) 😄
`;

/**
 * Component corresponding to page about the project.
 */
function PageAboutProject() {
  return (
    <>
      <Typography variant="h3">
        Le projet &nbsp;
        <em>
          <b>REX-DRI</b>
        </em>
      </Typography>
      <Markdown source={source} />
    </>
  );
}

export default compose(
  withPaddedPaper(),
  withErrorBoundary()
)(PageAboutProject);
