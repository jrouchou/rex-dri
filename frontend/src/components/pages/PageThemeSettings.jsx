import React from "react";
import { compose } from "recompose";
import { withErrorBoundary } from "../common/ErrorBoundary";
import ColorTool from "../settings/theme/ColorTools";
import { withPaddedPaper } from "./shared";

/**
 * Component corresponding to the site settings page
 */
function PageThemeSettings() {
  return (
    <>
      <ColorTool />
    </>
  );
}

PageThemeSettings.propTypes = {};

export default compose(
  withPaddedPaper(),
  withErrorBoundary()
)(PageThemeSettings);
