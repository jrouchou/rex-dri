import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import BaseMarkdown from "../common/markdown/BaseMarkdown";
import { PaddedPageDiv } from "./shared";
import { CGU_MARKDOWN_SOURCE, RGPD_MARKDOWN_SOURCE } from "../../config/other";

const useStyles = makeStyles((theme) => ({
  detailsRoot: {
    display: "block",
  },
  paperSwitch: {
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing(2),
  },

  centered: {
    margin: "0 auto",
    display: "block",
  },
  submitButton: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    marginLeft: "auto",
    marginRight: "auto",
  },
}));

// eslint-disable-next-line no-undef
const validatedOnServer = !!__hasValidatedCguRgpd;

function submit(formIsValid) {
  if (formIsValid && !validatedOnServer) {
    document.getElementById("formSubmitButton").click();
  }
}

function FormRgpdCgu() {
  const [expanded, setExpanded] = React.useState("");
  const [cguValidated, setCguValidated] = React.useState(validatedOnServer);
  const [rgpdValidated, setRgpdValidated] = React.useState(validatedOnServer);
  const classes = useStyles();

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : "");
  };

  const formIsValid = cguValidated && rgpdValidated;

  // updated the real _form generated by django silently
  const checkBox = document.getElementById("id_has_validated_cgu_rgpd");
  if (checkBox !== null) {
    checkBox.checked = formIsValid;
  }

  const data = [
    {
      panel: "cgu",
      title: "Conditions d'utilisation",
      expanded: expanded === "cgu",
      source: CGU_MARKDOWN_SOURCE,
      validated: cguValidated,
      setValidated: setCguValidated,
    },
    {
      panel: "rgpd",
      title: "Mention d'information RGPD",
      expanded: expanded === "rgpd",
      source: RGPD_MARKDOWN_SOURCE,
      validated: rgpdValidated,
      setValidated: setRgpdValidated,
    },
  ];

  return (
    <PaddedPageDiv>
      {data.map((el) => (
        <ExpansionPanel
          expanded={el.expanded}
          onChange={handleChange(el.panel)}
          key={el.panel}
        >
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography>
              {el.title}
              &nbsp;
            </Typography>
            <Typography variant="caption" color="secondary">
              ({el.validated ? "acceptée" : "non acceptée"})
            </Typography>
          </ExpansionPanelSummary>

          <ExpansionPanelDetails classes={{ root: classes.detailsRoot }}>
            <BaseMarkdown source={el.source} headingOffset={2} />
            {!validatedOnServer && (
              <>
                <br />
                <Paper className={classes.paperSwitch}>
                  <div
                    style={{
                      width: "100%",
                      display: "flex",
                    }}
                  >
                    <FormControlLabel
                      className={classes.centered}
                      control={
                        <Switch
                          disabled={validatedOnServer}
                          checked={el.validated}
                          onChange={(e) => el.setValidated(e.target.checked)}
                          color="primary"
                        />
                      }
                      label={`Acceptée ? (${el.validated ? "oui" : "non"})`}
                    />
                  </div>
                </Paper>
              </>
            )}
          </ExpansionPanelDetails>
        </ExpansionPanel>
      ))}
      {!validatedOnServer && (
        <div
          style={{
            display: "flex",
            width: "100%",
          }}
        >
          <Button
            disabled={validatedOnServer || !formIsValid}
            onClick={() => submit(formIsValid)}
            className={classes.submitButton}
            variant="contained"
            size="large"
            color="primary"
          >
            Enregistrer
          </Button>
        </div>
      )}
    </PaddedPageDiv>
  );
}

export default FormRgpdCgu;
