import React from "react";
import { Line } from "react-chartjs-2";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";

const colors = [
  "#1f77b4",
  "#ff7f0e",
  "#2ca02c",
  "#d62728",
  "#9467bd",
  "#8c564b",
  "#e377c2",
  "#7f7f7f",
  "#bcbd22",
  "#17becf",
];

const plotOptions = {
  tooltips: {
    intersect: false,
    mode: "index",
  },
  scales: {
    xAxes: [
      {
        type: "time",
      },
    ],
  },
};

function PlotResults({ results, title }) {
  if (results.length === 0) {
    return <></>;
  }

  const firstEl = results[0];
  const cols = new Set(Object.keys(firstEl));
  const canPlot = cols.has("x") && cols.has("y");

  if (!canPlot) {
    return (
      <Typography variant="body1">
        <i>
          Pour afficher les résultats sous forme de graphique, vous devez
          préciser les alias «&nbsp;x&nbsp;» (qui devra être une date, un mois
          ou une année) et «&nbsp;y&nbsp;» dans le SELECT. Vous pouvez également
          préciser des catégories avec l'alias «&nbsp;cat&nbsp;».
        </i>
      </Typography>
    );
  }

  if (!cols.has("cat")) {
    const mapping = new Map();
    results.forEach(({ x, y }) => {
      mapping.set(x, y);
    });

    const X = [...mapping.keys()];
    X.sort();

    return (
      <Line
        redraw
        height="110vh"
        data={{
          labels: X,
          datasets: [
            {
              label: title,
              backgroundColor: "rgb(255, 99, 132)",
              borderColor: "rgb(255, 99, 132)",
              data: X.map((x) => mapping.get(x)),
            },
          ],
        }}
        options={plotOptions}
      />
    );
  }

  // Otherwise, we have categories
  // We need to reconstruct all the curves
  const categories = [...new Set(results.map((el) => el.cat))];

  const mapping = new Map();
  results.forEach(({ x, y, cat }) => {
    if (!mapping.has(x)) {
      mapping.set(x, new Map());
    }
    mapping.get(x).set(cat, y);
  });

  const X = [...mapping.keys()];
  X.sort();

  const datasets = categories.map((cat, index) => ({
    label: cat,
    fill: "disabled",
    borderColor: colors[index % colors.length],
    data: X.map((x) => {
      const val = mapping.get(x).get(cat);
      return typeof val !== "undefined" ? val : 0;
    }),
  }));

  return (
    <Line
      redraw
      height="110vh"
      data={{
        labels: X,
        datasets,
      }}
      options={plotOptions}
    />
  );
}

PlotResults.propTypes = {
  results: PropTypes.array.isRequired,
  title: PropTypes.string,
};

PlotResults.defaultProps = {
  title: "y = f(x)",
};

export default PlotResults;
