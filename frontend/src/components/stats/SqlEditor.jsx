import React from "react";
import PropTypes from "prop-types";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

/**
 * Component to display information about the dataset
 */
function SqlEditor({ request, setRequest, performRequest }) {
  return (
    <>
      <TextField
        id="standard-multiline-static"
        label="Requête d'exploration"
        multiline
        fullWidth
        margin="normal"
        placeholder="Entrez la requête SQL"
        value={request}
        onChange={(event) => setRequest(event.target.value)}
      />

      <Button
        variant="contained"
        color="primary"
        onClick={() => performRequest(request)}
      >
        Exécuter
      </Button>
    </>
  );
}

SqlEditor.propTypes = {
  request: PropTypes.string.isRequired,
  setRequest: PropTypes.func.isRequired,
  performRequest: PropTypes.func.isRequired,
};

export default React.memo(SqlEditor);
