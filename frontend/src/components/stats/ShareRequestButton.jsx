import React from "react";
import Button from "@material-ui/core/Button";
import ShareIcon from "@material-ui/icons/Share";
import NotificationService from "../../services/NotificationService";

function copyUrlToClipboard() {
  const url = window.location.href;
  const dummy = document.createElement("input");

  document.body.appendChild(dummy);
  dummy.value = url;
  dummy.select();
  document.execCommand("copy");
  document.body.removeChild(dummy);
}

/**
 * Component to have a clickable button to export to CSV
 */
function ShareRequestButton() {
  return (
    <Button
      variant="contained"
      color="secondary"
      size="large"
      fullWidth
      onClick={() => {
        copyUrlToClipboard();
        NotificationService.info("L'URL de la page a été copiée 🎉");
      }}
    >
      Partager cette page&nbsp;
      <ShareIcon />
    </Button>
  );
}

ShareRequestButton.propTypes = {};

export default ShareRequestButton;
