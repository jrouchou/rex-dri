import React, { useState } from "react";
import PropTypes from "prop-types";

import { DatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";

import frLocale from "date-fns/locale/fr";
import format from "date-fns/format";
import KeyboardArrowLeftIcon from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRightIcon from "@material-ui/icons/KeyboardArrowRight";
import { makeStyles } from "@material-ui/styles";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { changeDates, getDatesFromUrl } from "./utils";

/**
 * Class to customize the header of the date selection box
 */
class LocalizedUtils extends DateFnsUtils {
  getDatePickerHeaderText(date) {
    return format(date, "d MMM yyyy", { locale: this.locale });
  }
}

const realMaxDate = new Date();

function DatePickerInternal({ value, onChange }) {
  return (
    <MuiPickersUtilsProvider utils={LocalizedUtils} locale={frLocale}>
      <DatePicker
        clearable
        format="d MMM yyyy"
        value={value}
        onChange={onChange}
        clearLabel="vider"
        cancelLabel="annuler"
        leftArrowIcon={<KeyboardArrowLeftIcon />}
        rightArrowIcon={<KeyboardArrowRightIcon />}
        maxDate={realMaxDate}
      />
    </MuiPickersUtilsProvider>
  );
}

DatePickerInternal.propTypes = {
  value: PropTypes.instanceOf(Date).isRequired,
  onChange: PropTypes.func.isRequired,
};

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  item: {
    margin: theme.spacing(1),
  },
}));

const { dateMin: dateMinUrl, dateMax: dateMaxUrl } = getDatesFromUrl();

/**
 * Component to display the result of a request
 */
function PickDataDateRange() {
  const classes = useStyles();

  const [dateMin, setDateMin] = useState(dateMinUrl);
  const [dateMax, setDateMax] = useState(dateMaxUrl);

  const apply = () => {
    changeDates(dateMin, dateMax);
  };

  return (
    <>
      <Typography variant="h6">
        Choix de la plage temporelle des données
      </Typography>
      <div className={classes.container}>
        <div className={classes.item}>
          <DatePickerInternal value={dateMin} onChange={setDateMin} />
        </div>
        <div className={classes.item}>
          <DatePickerInternal value={dateMax} onChange={setDateMax} />
        </div>

        <div className={classes.item}>
          <Button variant="contained" color="secondary" onClick={apply}>
            Appliquer
          </Button>
        </div>
      </div>
    </>
  );
}

export default PickDataDateRange;
