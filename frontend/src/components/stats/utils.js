import alasql from "alasql";
import { getGetParamInUrl, setGetParamInUrl } from "../../utils/url";
import {
  getUpdatedMajor,
  getUpdatedMinor,
} from "../../utils/majorMinorMappings";
import dateStrToDate from "../../utils/dateStrToDate";
import dateToDateStr from "../../utils/dateToDateStr";

export const currentDatasetName = getGetParamInUrl("dataset");

export const availableDatasets = [
  {
    name: "daily_connections",
    label: "Nombre de connexions",
    info:
      "Attention, les données antérieures au 15 juin 2020 ne sont qu'une estimation basse de la réalité.",
    columns: [
      { name: "date", description: "Date de la statistique" },
      {
        name: "nb_connections",
        description: "Nombre de connexions journalières",
      },
    ],
    exampleRequests: [
      {
        label: "Sélectionner toutes les données",
        request: `SELECT
    *
FROM ?
ORDER BY date ASC;`,
      },
      {
        label: "Afficher le nombre de connexions journalières",
        graphTitle: "Nombre de connexions journalières",
        request: `SELECT
    date AS x,
    nb_connections AS y
FROM ?
ORDER BY date ASC;`,
        default: true,
      },
      {
        label: "Afficher le nombre de connexions mensuelles",
        graphTitle: "Nombre de connexions mensuelles",
        request: `WITH
    data AS (
        SELECT
            SUBSTRING(date, 1, 7) AS month,
            nb_connections
        FROM ?
    )
SELECT
    month AS x,
    sum(nb_connections) AS y
FROM data
GROUP BY month
ORDER BY x ASC;`,
      },
    ],
  },
  {
    name: "daily_exchange_contributions",
    label: "Nombre de contributions",
    columns: [
      { name: "date", description: "Date de la statistique" },
      { name: "university", description: "Université concernée" },
      {
        name: "major",
        description:
          "Branche (ou TC / HUTECH) de l'étudiant⋅e lors de l'échange",
      },
      {
        name: "minor",
        description: "Filière de l'étudiant⋅e lors de l'échange",
      },
      {
        name: "exchange_semester",
        description: "Semestre au cours duquel l'échange a été effectué",
      },
      {
        name: "type",
        description:
          "Type de contribution (avis sur l'échange ou avis sur les cours)",
      },
      {
        name: "nb_contributions",
        description:
          "Nombre de contributions journalières correspondant aux autres champs",
      },
    ],
    exampleRequests: [
      {
        label: "Sélectionner toutes les données",
        request: `SELECT
    *
FROM ?
ORDER BY date ASC;`,
      },
      {
        label:
          "Afficher le nombre de contributions mensuelles (concernant l'échange et non les cours) par branche",
        request: `WITH
    data AS (
        SELECT
            SUBSTRING(date, 1, 7) AS month,
            major,
            nb_contributions
        FROM ?
        WHERE
            type = 'exchange_feedback'
    )
SELECT
     month AS x,
     major AS cat,
     sum(nb_contributions) AS y
 FROM data
 GROUP BY
    month, major
 ORDER BY
    x ASC, cat ASC;`,
        default: true,
      },
      {
        label: "Afficher le nombre de contributions par type",
        request: `SELECT
    type,
    sum(nb_contributions) AS nb_contributions
FROM ?
GROUP BY type
ORDER BY type ASC;`,
      },
      {
        label:
          "Afficher le nombre de contributions (concernant l'échange et non les cours) par branche",
        request: `SELECT
    major,
    sum(nb_contributions) AS nb_contributions
FROM ?
WHERE
    type = 'exchange_feedback'
GROUP BY major
ORDER BY major ASC;`,
      },
      {
        label:
          "Afficher le nombre de contributions (concernant l'échange et non les cours) par université pour la branche IM",
        request: `SELECT
    university,
    sum(nb_contributions) AS nb_contributions
FROM ?
WHERE
    type = 'exchange_feedback'
    AND major = 'IM'
GROUP BY university
ORDER BY nb_contributions DESC;`,
      },
      {
        label:
          "Afficher le nombre de contributions (concernant l'échange et non les cours) par filière pour la branche IM",
        request: `SELECT
    minor,
    sum(nb_contributions) AS nb_contributions
FROM ?
WHERE
    type = 'exchange_feedback'
    AND major = 'IM'
GROUP BY minor
ORDER BY nb_contributions DESC;`,
      },
    ],
  },
];

/**
 * Get the columns of a dataset
 * @param datasetName {string}
 * @returns {[{name: string, description: string}]}
 */
export function getDatasetColumns(datasetName = currentDatasetName) {
  return availableDatasets.find((dataset) => dataset.name === datasetName)
    .columns;
}

/**
 * Get the example requests of a dataset
 *
 * @param datasetName
 * @returns {[{request: string, label: string, default: boolean}]}
 */
export function getDatasetExampleRequests(datasetName = currentDatasetName) {
  return availableDatasets.find((dataset) => dataset.name === datasetName)
    .exampleRequests;
}

/**
 * Get the default request for a dataset
 * @param datasetName {string}
 * @returns {string}
 */
export function getDefaultRequest(datasetName = currentDatasetName) {
  const exampleRequests = getDatasetExampleRequests(datasetName);
  return exampleRequests.filter((el) => el.default === true)[0].request;
}

/**
 * Get the dataset data from the HTML.
 *
 * @returns {[{}]}
 */
function getDatasetData() {
  // Reconstruct the dataset from the backend
  // eslint-disable-next-line no-undef
  const datasetDataFromBackend = __StatsData;
  const cols = Object.keys(datasetDataFromBackend);
  let data = [];
  // eslint-disable-next-line no-plusplus
  for (let i = 0; i < datasetDataFromBackend[cols[0]].length; i++) {
    const elem = {};
    cols.forEach((col) => {
      elem[col] = datasetDataFromBackend[col][i];
    });
    data.push(elem);
  }

  // need to remap major and minors
  if (currentDatasetName === "daily_exchange_contributions") {
    data = data.map((el) => {
      const newMajor = getUpdatedMajor(el.major);
      const newMinor = getUpdatedMinor(el.major, el.minor);
      // eslint-disable-next-line no-param-reassign
      el.major = newMajor;
      // eslint-disable-next-line no-param-reassign
      el.minor = newMinor;
      return el;
    });
    getUpdatedMajor();
  }

  return data;
}

export const currentDatasetData = getDatasetData();

/**
 * Get the request form the url. If no request is found, returns the default request.
 * @returns {string}
 */
export function getRequestFromUrl() {
  let request;

  try {
    const requestInfoString = getGetParamInUrl("request_info");
    request = JSON.parse(atob(requestInfoString)).request;
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log(err);
  }

  if (typeof request !== "string" || request === "") {
    return getDefaultRequest(currentDatasetName);
  }
  return request;
}

// eslint-disable-next-line no-shadow
export function executeSqlRequest(sqlRequest) {
  return alasql.promise(sqlRequest, [currentDatasetData]);
}

/**
 *
 */
export function setRequestInUrl(request) {
  const requestInfo = {
    request,
    version: 1.0,
  };

  const requestInfoAsString = btoa(JSON.stringify(requestInfo));
  setGetParamInUrl("request_info", requestInfoAsString, "push");
}

/**
 * Update the selected dataset
 * @param newDatasetName {string}
 */
export function changeDataset(newDatasetName) {
  if (newDatasetName !== currentDatasetName) {
    const defaultRequest = getDefaultRequest(newDatasetName);
    setRequestInUrl(defaultRequest);
    setGetParamInUrl("dataset", newDatasetName, "reload");
  }
}

/**
 * Get the dates from the url
 * @returns {{dateMax: Date, dateMin: Date}}
 */
export function getDatesFromUrl() {
  const dateMin = dateStrToDate(getGetParamInUrl("date_min"));
  const dateMax = dateStrToDate(getGetParamInUrl("date_max"));

  return { dateMin, dateMax };
}

/**
 * Change the dates in the url and reload the page
 *
 * @param newDateMin {Date}
 * @param newDateMax {Date}
 */
export function changeDates(newDateMin, newDateMax) {
  setGetParamInUrl("date_min", dateToDateStr(newDateMin), "push");
  setGetParamInUrl("date_max", dateToDateStr(newDateMax), "reload");
}
