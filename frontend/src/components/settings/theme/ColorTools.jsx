/* eslint-disable react/sort-comp */
// Inspired by from https://github.com/mui-org/material-ui/blob/master/docs/src/pages/style/color/ColorTool.js

import React, { useCallback } from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { rgbToHex } from "@material-ui/core/styles/colorManipulator";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";
import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";
import { compose } from "recompose";
import Input from "@material-ui/core/Input";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import isEqual from "lodash/isEqual";
import Divider from "@material-ui/core/Divider";
import defaultSiteTheme from "../../../config/defaultTheme.json";
import SaveButton from "../../common/SaveButton";
import ColorDemo from "./ColorDemo";
import TextLink from "../../common/TextLink";
import deepCopy from "../../../utils/deepCopy";
import RequestParams from "../../../utils/api/RequestParams";
import { getTheme } from "../../common/theme/utils";
import { CURRENT_USER_ID } from "../../../config/user";
import LicenseNotice from "../../common/LicenseNotice";
import withNetworkWrapper, {
  NetWrapParam,
} from "../../../hoc/withNetworkWrapper";

import { useApiUpdate } from "../../../hooks/wrappers/api";

const isRgb = (string) => /#?([0-9a-f]{6})/i.test(string);

/**
 * Component to handle website color customization
 */
class ColorTool extends React.Component {
  constructor(props) {
    super(props);

    const theme = this.getInitialTheme();
    this.state = this.getStateForTheme(theme);
  }

  /**
   * Get the theme from the server
   * @returns {object}
   */
  getInitialTheme() {
    return deepCopy(this.props.userData.theme);
  }

  /**
   * Map a theme (from the server) to the state like object
   *
   * @param theme
   * @returns {object}
   */
  getStateForTheme(theme) {
    return {
      theme,
      "light-primary-input": theme.light.primary,
      "light-secondary-input": theme.light.secondary,
      "dark-primary-input": theme.dark.primary,
      "dark-secondary-input": theme.dark.secondary,
    };
  }

  /**
   * Handler to change the night mode
   */
  handleChangeNightMode() {
    const oldTheme = deepCopy(this.state.theme);
    const newTheme = Object.assign(oldTheme, {
      mode: oldTheme.mode === "light" ? "dark" : "light",
    });
    this.updateStateTheme(newTheme);
  }

  /**
   * Handler to take into account changes from color inputs
   * @param intent
   * @returns {Function}
   */
  handleChangeColor = (intent) => (event) => {
    // Lazy to provide a cleaner fix
    // eslint-disable-next-line react/no-access-state-in-setstate
    const oldTheme = this.state.theme;
    const {
      target: { value: color },
    } = event;

    this.setState({
      [`${oldTheme.mode}-${intent}-input`]: color,
    });

    if (isRgb(color)) {
      const newTheme = { ...oldTheme };
      // eslint-disable-next-line no-unused-expressions
      oldTheme.mode === "light"
        ? (newTheme.light[intent] = color)
        : (newTheme.dark[intent] = color);
      this.setState({ theme: newTheme });
    }
  };

  /**
   * Restores the default theme of the app.
   * Doesn't change the night mode status.
   * Restores only the colors for the current night mode.
   */
  handleRestoreDefault() {
    const newTheme = deepCopy(defaultSiteTheme);
    const requestedMode = this.state.theme.mode;
    const otherMode = requestedMode === "light" ? "dark" : "light";
    newTheme.mode = requestedMode;
    newTheme[otherMode] = this.state.theme[otherMode];
    this.setState(this.getStateForTheme(newTheme));
  }

  /**
   * Save the new theme on the server
   */
  handleSendToServer() {
    const userData = deepCopy(this.props.userData);
    const newUserData = Object.assign(userData, { theme: this.state.theme });
    this.props.saveUserDataToServer(newUserData);
  }

  /**
   * Shortcu to the set the theme
   * @param newTheme
   */
  updateStateTheme(newTheme) {
    this.setState({ theme: newTheme });
  }

  /**
   *
   *
   * RENDERERS
   *
   *
   */

  /**
   * Renders A grid of the three color variants
   * @param {string} color
   */
  renderColorBar(color) {
    const { classes } = this.props;
    const { augmentColor, getContrastText } = this.props.theme.palette;
    const background = augmentColor({ main: color });

    return (
      <Grid container className={classes.colorBar}>
        {["dark", "main", "light"].map((key) => (
          <div
            className={classes.colorSquare}
            style={{ backgroundColor: background[key] }}
            key={key}
          >
            <Typography
              variant="caption"
              style={{ color: getContrastText(background[key]) }}
            >
              {rgbToHex(background[key])}
            </Typography>
          </div>
        ))}
      </Grid>
    );
  }

  /**
   * Renders a color picker.
   *
   * @param {"primary"|"secondary"} intent
   */
  renderColorPicker(intent) {
    const requestedMode = this.state.theme.mode;
    const inputValue = this.state[`${requestedMode}-${intent}-input`];
    const color = this.state.theme[requestedMode][intent];
    const caption =
      intent === "primary" ? "Couleur primaire" : "Couleur secondaire";

    return (
      <>
        <Typography gutterBottom variant="h6">
          {caption}
        </Typography>
        <Input
          id={intent}
          value={inputValue}
          onChange={(e) => this.handleChangeColor(intent)(e)}
          type="color"
          fullWidth
        />
        {this.renderColorBar(color)}
      </>
    );
  }

  /**
   * Renders the advanced configuration parts.
   */
  renderAdvancedSettings() {
    const { classes } = this.props;
    return (
      <ExpansionPanel>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Typography>Réglages avancés des couleurs</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails style={{ display: "block" }}>
          <Typography variant="caption">
            Ci-dessous, vous pouvez choisir les couleurs dominantes du site.
            Celles-ci dépendent de l'état d'activation du « mode nuit ». Nous
            vous invitons à utiliser&nbsp;
            <TextLink href="https://material.io/tools/color/">ce site</TextLink>
            &nbsp; pour guider votre choix. À tout moment, vous pouvez rétablir
            le thème par défaut (pour le «mode nuit » en cours) du site en
            cliquant ci-dessous.
          </Typography>

          <Grid container spacing={2}>
            {["primary", "secondary"].map((intent, idx) => (
              // eslint-disable-next-line react/no-array-index-key
              <Grid item xs={6} key={idx}>
                {this.renderColorPicker(intent)}
              </Grid>
            ))}
          </Grid>

          <Divider variant="fullWidth" className={classes.divider} />

          <div style={{ width: "100%" }}>
            <Button
              variant="contained"
              color="primary"
              onClick={() => this.handleRestoreDefault()}
              className={classes.centered}
              style={{ display: "block" }}
            >
              Rétablir le thème par défaut
            </Button>
          </div>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  }

  render() {
    const { classes } = this.props;
    const themeForDemo = getTheme(this.state.theme);
    const hasChanges = !isEqual(this.state.theme, this.getInitialTheme());
    const darkModeActivated = this.state.theme.mode === "dark";

    return (
      <>
        <Typography variant="h6">Réglages</Typography>
        <LicenseNotice variant="REX-DRI—PRIVATE" />
        <FormControlLabel
          labelPlacement="start"
          label={
            !darkModeActivated
              ? "Activer le monde nuit : "
              : "Désactiver le mode nuit :"
          }
          control={
            <Switch
              checked={darkModeActivated}
              onChange={() => this.handleChangeNightMode()}
              value="osef"
            />
          }
        />
        {this.renderAdvancedSettings()}

        <Divider variant="fullWidth" className={classes.divider} />

        <Typography variant="h6">Prévisualisation</Typography>
        <MuiThemeProvider theme={themeForDemo}>
          <ColorDemo />
        </MuiThemeProvider>

        <Divider variant="fullWidth" className={classes.divider} />

        <SaveButton
          label="Enregistrer et appliquer au site"
          successLabel="Enregistré"
          disabled={false}
          handleSaveRequested={() => this.handleSendToServer()}
          success={!hasChanges}
          extraRootClass={classes.centered}
        />
      </>
    );
  }
}

ColorTool.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  saveUserDataToServer: PropTypes.func.isRequired,
  userData: PropTypes.object.isRequired,
};

const styles = (theme) => ({
  colorBar: {
    marginTop: theme.spacing(2),
  },
  colorSquare: {
    width: 64,
    height: 64,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  centered: {
    margin: "0 auto",
  },
  divider: {
    margin: theme.spacing(3),
  },
});

const ConnectedComp = compose(
  withNetworkWrapper([
    new NetWrapParam("userData", "one", {
      addDataToProp: "userData",
      params: RequestParams.Builder.withId(CURRENT_USER_ID).build(),
    }),
  ]),
  withStyles(styles, { withTheme: true })
)(ColorTool);

export default () => {
  const saveUserDataToServerInt = useApiUpdate("userData");
  const saveUserDataToServer = useCallback(
    (data) => saveUserDataToServerInt(CURRENT_USER_ID, data),
    []
  );

  return <ConnectedComp saveUserDataToServer={saveUserDataToServer} />;
};
