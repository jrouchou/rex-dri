import React, { useCallback, useMemo, useState } from "react";

import TextField from "@material-ui/core/TextField";
import fuzzysort from "fuzzysort";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/styles";
import UnivList from "./UnivList";
import UniversityService from "../../services/data/UniversityService";
import { useSelectedUniversities } from "../../hooks/wrappers/useSelectedUniversities";

const useStyles = makeStyles({
  inputCentered: {
    textAlign: "center",
  },
});

/**
 * Component to search through the universities
 */
function Search() {
  const classes = useStyles();

  const [inputValue, setInputValue] = useState("");
  const setInputValueFromEvent = useCallback((e) => {
    setInputValue(e.target.value);
  }, []);

  const [selectedUniversities] = useSelectedUniversities();

  const suggestions = useMemo(() => {
    const universities = UniversityService.getUniversities();

    const filteredUniversities = selectedUniversities;
    const possibleUniversities =
      filteredUniversities === null
        ? universities
        : universities.filter((univ) => filteredUniversities.includes(univ.id));
    const filter = fuzzysort.go(inputValue, possibleUniversities, {
      keys: ["name", "acronym"],
    });

    let suggestionsOut = filter.map((item) => item.obj);
    if (suggestionsOut.length === 0) {
      suggestionsOut = possibleUniversities;
    }
    return suggestionsOut;
  }, [inputValue, selectedUniversities]);

  return (
    <>
      <TextField
        id="full-width"
        label=""
        InputLabelProps={{
          shrink: true,
        }}
        placeholder="Nom ou acronyme..."
        fullWidth
        margin="normal"
        InputProps={{ classes: { input: classes.inputCentered } }}
        onChange={setInputValueFromEvent}
      />
      {suggestions.length === 0 && (
        <Typography color="secondary">
          <em>Aucune université ne correspond à la recherche.</em>
        </Typography>
      )}
      <UnivList universitiesToList={suggestions} />
    </>
  );
}

Search.propTypes = {};

export default Search;
