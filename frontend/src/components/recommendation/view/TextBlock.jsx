import React from "react";
import TextField from "@material-ui/core/TextField";
import PropTypes from "prop-types";
import keycode from "keycode";
import Markdown from "../../common/markdown/Markdown";
import useBlock from "../../../hooks/useBlock";
import truncateString from "../../../utils/truncateString";

/**
 * Comment block that will hold markdown text
 */
function TextBlock(props) {
  const [
    text,
    updateText,
    renderAsInput,
    handleSwitchToInput,
    handleSwitchToMarkdown,
  ] = useBlock(
    typeof props.text === "string" ? props.text : "",
    (v) => props.onChange(v),
    (v) => v.trim(),
    (v) => v === "",
    props.readOnly
  );

  return (
    // eslint-disable-next-line jsx-a11y/no-static-element-interactions
    <div
      role="presentation"
      onClick={handleSwitchToInput}
      onKeyDown={(e) => {
        if (e.ctrlKey && keycode(e) === "enter") handleSwitchToMarkdown();
      }}
    >
      {renderAsInput ? (
        <TextField
          value={text}
          onChange={(e) =>
            updateText(truncateString(e.target.value, props.maxLength))
          }
          placeholder={props.placeHolder}
          fullWidth
          multiline={props.multiline}
          autoFocus={text !== ""}
          variant="standard"
          onBlur={handleSwitchToMarkdown}
        />
      ) : (
        <div
          onClick={handleSwitchToInput}
          onFocus={handleSwitchToInput}
          role="presentation"
        >
          {props.render(text)}
        </div>
      )}
    </div>
  );
}

TextBlock.propTypes = {
  text: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  multiline: PropTypes.bool,
  render: PropTypes.func,
  maxLength: PropTypes.number,
  placeHolder: PropTypes.string,
  readOnly: PropTypes.bool.isRequired,
};

TextBlock.defaultProps = {
  multiline: true,
  maxLength: 0,
  placeHolder: "Vous pouvez écrire du markdown dans ce block",
  // eslint-disable-next-line react/display-name
  render: (text) => <Markdown source={text} />,
};

export default React.memo(TextBlock);
