import React, { useCallback, useState } from "react";
import PropTypes from "prop-types";
import { compose } from "recompose";
import TableHead from "@material-ui/core/TableHead";
import Table from "@material-ui/core/Table";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import { makeStyles } from "@material-ui/styles";
import Button from "@material-ui/core/Button";
import AddIcon from "@material-ui/icons/Add";
import DeleteIcon from "@material-ui/icons/DeleteForever";
import PublicIcon from "@material-ui/icons/LockOpen";
import PrivateIcon from "@material-ui/icons/Lock";
import IconButton from "@material-ui/core/IconButton";
import APP_ROUTES from "../../config/appRoutes";
import SimplePopupMenu from "../common/SimplePopupMenu";
import LinkToUser from "../common/LinkToUser";
import { CURRENT_USER_ID } from "../../config/user";
import NavigationService from "../../services/NavigationService";
import withNetworkWrapper, { NetWrapParam } from "../../hoc/withNetworkWrapper";

import { useApiCreate, useApiDelete } from "../../hooks/wrappers/api";

const emptyList = {
  title: "Une nouvelle liste",
  is_public: false,
  description: "Description de la liste",
  content: [],
  owner: CURRENT_USER_ID,
};

const useStyles = makeStyles((theme) => ({
  row: {
    "&:hover": {
      cursor: "pointer",
    },
  },
  button: {
    margin: theme.spacing(1),
  },
  leftIcon: {
    marginRight: theme.spacing(1),
  },
}));

/**
 * Component to render the list of all available recommendation lists
 */
function SelectListSubPage({ lists }) {
  const [display, setDisplay] = useState("owned");
  const classes = useStyles();

  const createList = useApiCreate("recommendationLists");
  const deleteList = useApiDelete("recommendationLists");

  const goToList = useCallback((listId) => {
    NavigationService.goToRoute(APP_ROUTES.forList(listId));
  }, []);

  const ownedLists = lists.filter((list) => list.is_user_owner);
  const followedLists = lists.filter((list) => !list.is_user_owner);
  const displayedLists = display === "owned" ? ownedLists : followedLists;

  return (
    <>
      <Button
        variant="contained"
        color={display === "owned" ? "primary" : "default"}
        className={classes.button}
        onClick={() => setDisplay("owned")}
      >
        Mes listes ({ownedLists.length})
      </Button>
      <Button
        variant="contained"
        color={display === "followed" ? "primary" : "default"}
        className={classes.button}
        onClick={() => setDisplay("followed")}
      >
        Les listes que je suis ({followedLists.length})
      </Button>
      <Button
        variant="contained"
        color="secondary"
        onClick={() =>
          createList(emptyList, (data) => {
            goToList(data.id);
          })
        }
        className={classes.button}
      >
        <AddIcon className={classes.leftIcon} />
        &nbsp; Créer une liste
      </Button>
      {displayedLists.length !== 0 && (
        <div
          style={{
            overflowX: "scroll",
            height: "fit-content",
            display: "flex",
          }}
        >
          <Table>
            <TableHead>
              <TableRow>
                <TableCell size="small" />
                <TableCell style={{ minWidth: 300 }}>Intitulé</TableCell>
                <TableCell>Créateur</TableCell>
                <TableCell size="small">Visibilité</TableCell>
                <TableCell>#Followers</TableCell>
                <TableCell style={{ minWidth: 300 }}>Description</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {displayedLists
                .sort(
                  (a, b) => new Date(b.last_update) - new Date(a.last_update)
                )
                .map((list) => (
                  <TableRow hover key={list.id} className={classes.row}>
                    <TableCell size="small">
                      <SimplePopupMenu
                        items={[
                          {
                            disabled: false,
                            label: "Confirmer",
                            onClick: () => deleteList(list.id),
                          },
                        ]}
                        renderHolder={({ onClick }) => (
                          <IconButton
                            color="secondary"
                            onClick={onClick}
                            disabled={display !== "owned"}
                          >
                            <DeleteIcon />
                          </IconButton>
                        )}
                      />
                    </TableCell>
                    {/* A lot of onClick to be able to have a different one for delete */}
                    <TableCell onClick={() => goToList(list.id)}>
                      {list.title}
                    </TableCell>
                    <TableCell>
                      {list.is_user_owner ? (
                        <em>Moi</em>
                      ) : (
                        <LinkToUser
                          userId={list.owner}
                          pseudo={list.owner_pseudo}
                        />
                      )}
                    </TableCell>
                    <TableCell size="small" onClick={() => goToList(list.id)}>
                      {list.is_public ? (
                        <PublicIcon />
                      ) : (
                        <PrivateIcon color="secondary" />
                      )}
                    </TableCell>
                    <TableCell onClick={() => goToList(list.id)}>
                      {list.nb_followers}
                    </TableCell>
                    <TableCell onClick={() => goToList(list.id)}>
                      {list.description}
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </div>
      )}
    </>
  );
}

SelectListSubPage.propTypes = {
  lists: PropTypes.array.isRequired,
};

export default compose(
  withNetworkWrapper([
    new NetWrapParam("recommendationLists", "all", {
      addDataToProp: "lists",
    }),
  ])
)(SelectListSubPage);
