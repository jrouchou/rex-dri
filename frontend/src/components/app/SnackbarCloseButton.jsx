import { IconButton } from "@material-ui/core";
import { Close as IconClose } from "@material-ui/icons";
import React from "react";
import PropTypes from "prop-types";
import NotificationService from "../../services/NotificationService";

function SnackbarCloseButton({ notiKey }) {
  return (
    <IconButton onClick={() => NotificationService.closeNotification(notiKey)}>
      <IconClose />
    </IconButton>
  );
}

SnackbarCloseButton.propTypes = {
  notiKey: PropTypes.number.isRequired,
};

export default SnackbarCloseButton;
