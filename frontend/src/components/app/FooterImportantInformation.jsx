import React from "react";
import Divider from "@material-ui/core/Divider";
import { makeStyles } from "@material-ui/styles";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import InformationList from "./InformationList";
import APP_ROUTES from "../../config/appRoutes";

const useStyles = makeStyles((theme) => ({
  divider: {
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(0),
    paddingBottom: theme.spacing(2),
    color: theme.palette.text.secondary,
  },
}));

function FooterImportantInformation({ location }) {
  const classes = useStyles();

  // Don't display it on the home page
  if (location.pathname === APP_ROUTES.base) {
    return <></>;
  }

  return (
    <footer>
      <Divider className={classes.divider} />
      <InformationList includeVariants={["warning", "error"]} />
    </footer>
  );
}

FooterImportantInformation.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
};

export default withRouter(FooterImportantInformation);
