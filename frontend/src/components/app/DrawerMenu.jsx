import React from "react";
import PropTypes from "prop-types";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import SettingsIcon from "@material-ui/icons/Settings";
import InfoIcon from "@material-ui/icons/Info";
import CustomNavLink from "../common/CustomNavLink";
import {
  infoMenuItems,
  mainMenuHome,
  mainMenuItems,
  secondaryMenuItems,
  settingsMenuItems,
} from "./menuItems";

const ListItemHeading = ({ label, Icon }) => (
  <ListItem>
    <ListItemIcon>
      <Icon />
    </ListItemIcon>
    <ListItemText primary={label} />
  </ListItem>
);

ListItemHeading.propTypes = {
  label: PropTypes.node.isRequired,
  Icon: PropTypes.object.isRequired,
};

const ListItemsTmp = ({ items, onClick, inset }) => (
  <>
    {items.map(({ label, route, Icon }, idx) => (
      // eslint-disable-next-line react/no-array-index-key
      <CustomNavLink key={idx} to={route} onClick={onClick}>
        <ListItem button onClick={onClick}>
          {Icon !== null && (
            <ListItemIcon>
              <Icon />
            </ListItemIcon>
          )}
          <ListItemText primary={label} inset={inset} />
        </ListItem>
      </CustomNavLink>
    ))}
  </>
);

ListItemsTmp.propTypes = {
  items: PropTypes.array.isRequired,
  onClick: PropTypes.func.isRequired,
  inset: PropTypes.bool,
};

ListItemsTmp.defaultProps = {
  inset: false,
};

function DrawerMenu({ open, closeDrawer }) {
  const ListItems = (props) => (
    <ListItemsTmp {...props} onClick={closeDrawer} />
  );

  return (
    <div style={{ zIndex: 200000 }}>
      <Drawer open={open} onClose={closeDrawer}>
        <List>
          <ListItems items={[mainMenuHome]} />
        </List>
        <Divider />
        <List>
          <ListItems items={mainMenuItems} />
        </List>
        <Divider />
        <List>
          <ListItems items={secondaryMenuItems} />
          <Divider />
        </List>
        <List>
          <ListItemHeading label={<em>Informations</em>} Icon={InfoIcon} />
          <Divider variant="inset" />
          <ListItems items={infoMenuItems} inset />
        </List>
        <Divider />
        <List>
          <ListItemHeading label={<em>Paramètres</em>} Icon={SettingsIcon} />
          <Divider variant="inset" />
          <ListItems items={settingsMenuItems} inset />
        </List>
      </Drawer>
    </div>
  );
}

DrawerMenu.propTypes = {
  open: PropTypes.bool.isRequired,
  closeDrawer: PropTypes.func.isRequired,
};

export default React.memo(DrawerMenu);
