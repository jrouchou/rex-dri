import React, { useState } from "react";
import PropTypes from "prop-types";
import CircularProgress from "@material-ui/core/CircularProgress";
import { makeStyles } from "@material-ui/styles";
import useInterval from "../../utils/useInterval";

const useStyles = makeStyles((theme) => ({
  progress: {
    margin: theme.spacing(2),
  },
}));

/**
 * React component to display a loading indicator
 */
function Loading({ size }) {
  const classes = useStyles();
  const [completion, setCompletion] = useState(0);

  useInterval(() => {
    setCompletion(completion >= 100 ? 0 : completion + 1);
  }, 20);

  return (
    <CircularProgress
      className={classes.progress}
      variant="determinate"
      size={size}
      value={completion}
    />
  );
}

Loading.propTypes = {
  size: PropTypes.number,
};

Loading.defaultProps = {
  size: 50,
};

export default Loading;
