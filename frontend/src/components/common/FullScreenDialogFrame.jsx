import React from "react";
import AppBar from "@material-ui/core/AppBar/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: "sticky",
  },
  flex: {
    flex: 1,
  },
  paper: {
    padding: theme.spacing(2),
    margin: theme.spacing(2),
  },
}));

function FullScreenDialogFrame({
  children,
  rightButton,
  handleCloseRequest,
  title,
}) {
  const classes = useStyles();

  return (
    <>
      <AppBar className={classes.appBar}>
        <Toolbar>
          <IconButton
            color="inherit"
            onClick={handleCloseRequest}
            aria-label="Close"
          >
            <CloseIcon />
          </IconButton>
          <Typography variant="h6" color="inherit" className={classes.flex}>
            {title}
          </Typography>
          {rightButton}
        </Toolbar>
      </AppBar>
      <Paper className={classes.paper}>{children}</Paper>
    </>
  );
}

FullScreenDialogFrame.propTypes = {
  children: PropTypes.node.isRequired,
  handleCloseRequest: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  rightButton: PropTypes.node,
};

FullScreenDialogFrame.defaultProps = {
  rightButton: <></>,
};

export default FullScreenDialogFrame;
