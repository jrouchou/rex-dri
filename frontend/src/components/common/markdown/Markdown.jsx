/* eslint-disable react/prop-types */
/* eslint-disable react/display-name */

import React from "react";
import parseMoney from "../../../utils/parseMoney";
import BaseMarkdown from "./BaseMarkdown";
import CurrencyService from "../../../services/data/CurrencyService";

function compileSource(source) {
  let compiled = "";

  parseMoney(source).forEach((el) => {
    if (!el.isMoney) {
      compiled += el.text;
    } else {
      const { amount, currency } = el;

      if (currency === "EUR") {
        compiled += `${amount}€`;
      } else {
        const converted = CurrencyService.convertAmountToEur(amount, currency);
        compiled += `${amount} ${currency} `;
        if (converted === null) {
          compiled += `*(\`${currency}\` n'a pas été reconnue comme le code d'une monnaie ; nous n'avons pas pu procéder à une conversion automatique)*`;
        } else {
          compiled += `[*(≈ ${converted}€)*](https://www.xe.com/currencyconverter/convert/?Amount=${amount}&From=${currency}&To=EUR)`; // add money converted information in markdown format
        }
      }
    }
  });

  return compiled;
}

/**
 * Custom Markdown component renderer to make use of material UI
 *
 * A custom tag for currency was adde: `:100.2CHF:` would be understood as `100 CHF (~88€)` for instance.
 *
 * We don't need to fetch them here.
 *
 * @return {string}
 */
export default React.memo((props) => (
  <BaseMarkdown {...props} compileSource={(s) => compileSource(s)} />
));
