import React, { useCallback, useEffect, useState } from "react";
import PropTypes from "prop-types";
import CircularProgress from "@material-ui/core/CircularProgress";
import green from "@material-ui/core/colors/green";
import Button from "@material-ui/core/Button";
import Fab from "@material-ui/core/Fab";
import CheckIcon from "@material-ui/icons/Check";
import SaveIcon from "@material-ui/icons/Save";
import { makeStyles } from "@material-ui/styles";
import useInterval from "../../utils/useInterval";
import classNames from "../../utils/classNames";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    alignItems: "center",
    width: "fit-content",
  },
  wrapper: {
    margin: theme.spacing(1),
    position: "relative",
  },
  buttonSuccess: {
    backgroundColor: green[500],
    "&:hover": {
      backgroundColor: green[700],
    },
  },
  fabProgress: {
    color: green[500],
    position: "absolute",
    top: -6,
    left: -6,
    zIndex: 1,
  },
  buttonProgress: {
    color: green[500],
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12,
  },
}));

/**
 * Component to render a nice save button
 * Inspired by https://material-ui.com/demos/progress/
 */
function SaveButton(props) {
  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const [success, setSuccess] = useState(false);

  const handleButtonClick = useCallback(() => {
    if (!loading && !success) {
      const shouldLoad = props.handleSaveRequested();
      if (shouldLoad !== false) {
        setLoading(true);
      } else {
        setSuccess(true);
      }
    }
  }, []);

  useInterval(() => {
    props.handleSaveRequested();
  }, props.autoSaveTimeout);

  // we smartly remove the loading status
  useEffect(() => {
    setSuccess(props.success);
    if (props.success) {
      setLoading(false);
    }
  }, [props.success]);

  useEffect(() => {
    const { autoResetSuccessTimeout } = props;
    if (autoResetSuccessTimeout !== 0 && success) {
      setTimeout(() => setSuccess(false), autoResetSuccessTimeout);
    }
  }, [success]);

  const { disabled, extraRootClass } = props;
  const buttonClassname = classNames(success ? classes.buttonSuccess : "");

  return (
    <div className={classNames(classes.root, extraRootClass)}>
      <div className={classes.wrapper}>
        <Fab
          color="primary"
          className={buttonClassname}
          onClick={handleButtonClick}
          disabled={disabled}
        >
          {success ? <CheckIcon /> : <SaveIcon />}
        </Fab>
        {loading && (
          <CircularProgress size={68} className={classes.fabProgress} />
        )}
      </div>
      <div className={classes.wrapper}>
        <Button
          variant="contained"
          color="primary"
          className={buttonClassname}
          disabled={loading || disabled}
          onClick={handleButtonClick}
        >
          {success ? props.successLabel : props.label}
        </Button>
        {loading && (
          <CircularProgress size={24} className={classes.buttonProgress} />
        )}
      </div>
    </div>
  );
}

SaveButton.propTypes = {
  /**
   * If func returns false, then loading indicator is not triggered.
   */
  handleSaveRequested: PropTypes.func.isRequired,
  success: PropTypes.bool,
  disabled: PropTypes.bool,
  label: PropTypes.string,
  successLabel: PropTypes.string.isRequired,
  extraRootClass: PropTypes.string,
  // eslint-disable-next-line react/no-unused-prop-types
  autoResetSuccessTimeout: PropTypes.number,
  /**
   * Time in millisecond to wait before an auto save is triggered.
   * If null, no auto save is enabled.
   */
  autoSaveTimeout: PropTypes.number,
};

SaveButton.defaultProps = {
  success: false,
  disabled: true,
  label: "Enregistrer",
  extraRootClass: "",
  autoResetSuccessTimeout: 0,
  autoSaveTimeout: null,
};

export default SaveButton;
