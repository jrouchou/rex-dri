import { NavLink } from "react-router-dom";
import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles((theme) => ({
  link: {
    color: theme.palette.text.primary,
    textDecoration: "none",
  },
}));

/**
 * Renders a NavLink (internal app link) with no decoration
 */
function CustomNavLink(props) {
  const { to, children } = props;

  const classes = useStyles();
  return (
    <NavLink to={to} className={classes.link}>
      {children}
    </NavLink>
  );
}

CustomNavLink.propTypes = {
  to: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};

export default CustomNavLink;
