import React from "react";
import PropTypes from "prop-types";
import useField from "../../../hooks/useField";

function HiddenField({ fieldMapping }) {
  useField(fieldMapping);

  return <></>;
}

HiddenField.propTypes = {
  fieldMapping: PropTypes.string.isRequired,
};

export default HiddenField;
