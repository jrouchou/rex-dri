import React, { useCallback } from "react";

import Button from "@material-ui/core/Button";
import PropTypes from "prop-types";
import CustomError from "../../common/CustomError";
import FieldWrapper from "./FieldWrapper";
import useId from "../../../hooks/wrappers/useId";
import useField from "../../../hooks/useField";
import Field from "./Field";

/**
 * Form field for a file
 * WARNING BETA
 * Doesn't support edit at this time
 * Not tested since switch to hooks
 */
function FileField({ fieldMapping, required, label, comment, type }) {
  const id = useId();

  const getError = useCallback((v) => {
    const messages = [];
    if (typeof v === "undefined" || v === "") {
      messages.push("Aucun fichier n'est sélectioné.");
    }
    return new CustomError(messages);
  }, []);

  const [value, setValueInt, error] = useField(fieldMapping, { getError });
  const setValue = useCallback(() => {
    const input = document.getElementById(id);
    if (input) {
      setValueInt(input.files[0]);
    }
    return setValueInt("");
  }, []);

  let valueDisplayed = value;
  if (typeof value !== "undefined") {
    valueDisplayed = value.split(/(\\|\/)/g).pop();
  }

  // WARNING BETA better style
  return (
    <FieldWrapper
      required={required}
      errorObj={error}
      label={label}
      commentText={comment}
    >
      (Tout fichier dont le tailler dépasse 2mo sera refusé par le serveur)
      <input
        accept={type === "picture" ? "image/*" : "*"}
        style={{ display: "none" }}
        id={id}
        type="file"
        onChange={setValue}
      />
      <label htmlFor={id}>
        <Button variant="contained" component="span">
          Fichier
        </Button>
        {valueDisplayed}
      </label>
    </FieldWrapper>
  );
}

FileField.propTypes = {
  ...Field.propTypes,
  type: PropTypes.oneOf(["picture", "file"]).isRequired,
};

export default FileField;
