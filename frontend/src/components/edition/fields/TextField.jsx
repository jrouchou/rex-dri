import React, { useCallback } from "react";
import PropTypes from "prop-types";

import MuiTextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";

import Field from "./Field";
import stringIsUrl from "../../../utils/stringIsUrl";
import stringHasExtension from "../../../utils/stringHasExtension";
import CustomError from "../../common/CustomError";
import truncateString from "../../../utils/truncateString";
import useField from "../../../hooks/useField";
import FieldWrapper from "./FieldWrapper";

const defaultNullValue = "";
/**
 * Form field for a simple textField (with no markdown)
 */
function TextField({
  fieldMapping,
  required,
  label,
  comment,
  maxLength,
  urlExtensions,
  isUrl,
}) {
  const getError = useCallback((v) => {
    const messages = [];
    if (required && (v === "" || v === null)) {
      messages.push("Ce champ est requis mais il est vide.");
    }
    if (maxLength && v !== null && v.length > maxLength) {
      messages.push("L'URL est trop longue.");
    }
    if (isUrl && v !== "" && !stringIsUrl(v)) {
      messages.push("L'URL entrée n'est pas reconnue.");
    }
    if (isUrl && v !== "" && urlExtensions.length > 0) {
      if (!stringHasExtension(v, urlExtensions)) {
        messages.push("Extension de l'URL non conforme");
      }
    }
    return new CustomError(messages);
  }, []);

  const [text, setTextInt, error] = useField(fieldMapping, {
    getError,
    defaultNullValue,
  });

  const setText = useCallback((e) => {
    const v = truncateString(e.target.value, maxLength);
    setTextInt(v);
  }, []);

  return (
    <FieldWrapper
      required={required}
      errorObj={error}
      label={label}
      commentText={comment}
    >
      {isUrl && (
        <>
          <Typography variant="caption">
            Un URL est attendu ici (Le protocole - http/https/ftp - est requis
            !)
          </Typography>
          {urlExtensions.length > 0 && (
            <Typography variant="caption">
              L'url doit terminer par l'une des extensions suivantes (majuscule
              ou miniscule) :{JSON.stringify(urlExtensions)}
            </Typography>
          )}
        </>
      )}
      {maxLength && (
        <Typography variant="caption">
          Nombre de caractères&nbsp;:&nbsp;{text.length}/{maxLength}
        </Typography>
      )}
      <MuiTextField
        placeholder="Le champ est vide"
        fullWidth
        multiline={false}
        value={text}
        onChange={setText}
      />
    </FieldWrapper>
  );
}

TextField.defaultProps = {
  ...Field.defaultProps,
  maxLength: 0,
  isUrl: false,
  urlExtensions: [],
};

TextField.propTypes = {
  ...Field.propTypes,
  maxLength: PropTypes.number,
  isUrl: PropTypes.bool.isRequired,
  urlExtensions: PropTypes.arrayOf(PropTypes.string.isRequired),
};

export default TextField;
