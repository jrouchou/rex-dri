import PropTypes from "prop-types";

const Field = {};

Field.propTypes = {
  required: PropTypes.bool, // is the field required ?
  label: PropTypes.string, // text to go along the field
  comment: PropTypes.string, // text to give more information on what is expected
  fieldMapping: PropTypes.string.isRequired, // name of the field in the data
};

Field.defaultProps = {
  required: false,
  label: "mon label",
  comment: "",
};

export default Field;
