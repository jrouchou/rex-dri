import React from "react";
import Typography from "@material-ui/core/Typography";
import TextField from "./TextField";
import MultiSelectField from "./MultiSelectField";
import UniversityService from "../../../services/data/UniversityService";
import MarkdownField from "./MarkdownField";
import getObjModerationLevel from "../../../utils/getObjModerationLevels";
import SelectField from "./SelectField";
import CountryService from "../../../services/data/CountryService";
import CurrencyService from "../../../services/data/CurrencyService";
import LanguageService from "../../../services/data/LanguageService";
import { getLatestApiReadData } from "../../../hooks/useGlobalState";

export function TitleField() {
  return <TextField required fieldMapping="title" label="Titre" />;
}

export function UniversitiesField() {
  return (
    <MultiSelectField
      label="Universités concernées"
      fieldMapping="universities"
      required
      options={UniversityService.getUniversitiesOptions()}
    />
  );
}

export function CommentField() {
  return (
    <MarkdownField
      fieldMapping="comment"
      maxLength={500}
      label="Commentaire associé à ces informations"
    />
  );
}

export function CountriesField() {
  return (
    <MultiSelectField
      label="Pays concernés"
      fieldMapping="countries"
      required
      options={CountryService.getCountryOptions()}
    />
  );
}

export function CurrencyField() {
  return (
    <SelectField
      label="Devise"
      required
      fieldMapping="currency"
      options={CurrencyService.getCurrenciesOptions()}
    />
  );
}

export function LanguageField() {
  return (
    <SelectField
      label="Langue principale dans lequel le cours est enseigné"
      required
      fieldMapping="language"
      options={LanguageService.getLanguagesOptions()}
    />
  );
}

export function ObjModerationLevelField() {
  // hack to access directly the store and get the value we need.
  const userData = getLatestApiReadData("userData-one");
  const possibleObjModeration = getObjModerationLevel(
    userData.owner_level,
    true
  );
  if (possibleObjModeration.length > 1) {
    return (
      <>
        <Typography variant="caption">
          Niveau de modération supplémentaire souhaité
        </Typography>
        <SelectField
          label="Niveau de modération pour ce module"
          fieldMapping="obj_moderation_level"
          required
          options={possibleObjModeration}
        />
      </>
    );
  }
  return (
    <Typography variant="caption">
      Votre statut ne vous permet pas modifier le niveau local de modération
      pour ce module.
    </Typography>
  );
}

export function ImportanceLevelField() {
  const options = [
    {
      label: "Normal",
      value: "-",
    },
    {
      label: "Important",
      value: "+",
    },
    {
      label: "Très important",
      value: "++",
    },
  ];

  return (
    <>
      <Typography variant="caption">
        Qualification de l'importance de l'information présentée
      </Typography>
      <SelectField
        label={"Niveau d'importance"}
        fieldMapping="importance_level"
        required
        options={options}
      />
    </>
  );
}
