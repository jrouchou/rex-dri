import React, { useCallback } from "react";
import PropTypes from "prop-types";

import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import Field from "./Field";
import CustomError from "../../common/CustomError";

import DownshiftMultiple from "../../common/DownshiftMultiple";
import FieldWrapper from "./FieldWrapper";
import useField from "../../../hooks/useField";

/**
 * Form field for select (not multiple, use the other one for such thing).
 */
function SelectField({ fieldMapping, required, label, comment, options }) {
  const getError = useCallback((v) => {
    const messages = [];

    if (required && (v === null || typeof v === "undefined")) {
      messages.push("Ce champ est requis.");
    }
    return new CustomError(messages);
  }, []);

  const [value, setValueInt, error] = useField(fieldMapping, { getError });
  const setValue = useCallback((e) => setValueInt(e.target.value), []);

  const RenderComp = () =>
    options.length < 10 ? (
      <Select value={value} onChange={setValue}>
        {options.map((el) => (
          <MenuItem key={el.value} disabled={el.disabled} value={el.value}>
            {el.label}
          </MenuItem>
        ))}
      </Select>
    ) : (
      <DownshiftMultiple
        value={
          typeof value === "undefined" || value === null ? undefined : [value]
        }
        multiple={false}
        options={options}
        fieldPlaceholder={label}
        onChange={setValueInt}
      />
    );

  return (
    <FieldWrapper
      required={required}
      errorObj={error}
      label={label}
      commentText={comment}
    >
      <RenderComp />
    </FieldWrapper>
  );
}

SelectField.defaultProps = {
  ...Field.defaultProps,
};

SelectField.propTypes = {
  ...Field.propTypes,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.oneOfType([
        PropTypes.number.isRequired,
        PropTypes.string.isRequired,
      ]),
      disabled: PropTypes.bool,
    })
  ),
};

export default SelectField;
