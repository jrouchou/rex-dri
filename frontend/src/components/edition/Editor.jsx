/* eslint-disable react/sort-comp */
import React from "react";
import PropTypes from "prop-types";
import Button from "@material-ui/core/Button";
import FormManager from "../../utils/editionRelated/FormManager";
import FullScreenDialogService from "../../services/FullScreenDialogService";
import LicenseNotice from "../common/LicenseNotice";
import FormContext from "../../contexts/FormContext";
import AlertService from "../../services/AlertService";
import FullScreenDialogFrame from "../common/FullScreenDialogFrame";
import EditorManager from "../../utils/editionRelated/EditorManager";

/**
 * Class to handle editions of models on the frontend.
 *
 * It should be used with the hook useEditor
 */
class Editor extends React.Component {
  /**
   * @type {FormManager}
   */
  formManager = undefined;

  /**
   * @type {EditorManager}
   */
  editorManager = undefined;

  /**
   * Creates an instance of Editor. and subscribes to the module wrapper so that it can access its functions.
   *
   * @param {object} props
   */
  constructor(props) {
    super(props);
    // editors can be used outside of a module wrapper.
    this.formManager = new FormManager(
      props.rawModelData,
      props.formLevelErrors
    );
    this.editorManager = new EditorManager();
    props.subscribeToModuleWrapper(this);
  }

  componentDidMount() {
    const { saveOnMount } = this.props;
    // handle moderation request
    if (saveOnMount) {
      this.performSave(this.parseRawModelData(this.props.rawModelData));
    }
  }

  /**
   * Function that extracts the modelData from the raw one.
   *
   * Basically we extract all field that can be edited in the _form associated with the editor.
   * Plut the id.
   *
   * @returns
   */
  parseRawModelData() {
    const out = {};
    this.formManager.getFieldsMapping().forEach((fieldMapping) => {
      out[fieldMapping] = this.props.rawModelData[fieldMapping];
    });

    if ("id" in this.props.rawModelData) {
      const { id } = this.props.rawModelData;
      out.id = id;
    }

    return out;
  }

  /**
   * Function to handle save editor events, eg when clicking on the save button
   * This function is not trivial and checks are performed.
   *
   */
  handleSaveEditorRequest = () => {
    const formErrors = this.formManager.getError();
    if (!formErrors.status) {
      // no error, we can save if necessary
      if (this.formManager.hasChanges()) {
        const formData = this.formManager.getDataFromFields();

        // Copy the model data and copy above the data from the _form
        // So that we don't forget anything.
        this.performSave({
          ...this.parseRawModelData(this.props.rawModelData),
          ...formData,
        });
      } else {
        this.editorManager.notifyNoChangesDetected();
        this.closeEditor(false);
      }
    } else {
      this.editorManager.notifyFormHasErrors();
    }
  };

  /**
   * Function to save the `data` to the server.
   *
   * @param {Object} data
   */
  performSave(data) {
    this.editorManager.notifyIsSaving();
    this.props.saveData(data, (newData) =>
      this.handleSaveRequestWasSuccessful(newData)
    );
  }

  /**
   * Function that is called as soon as we now that the save request to the server was successful
   *
   * This enables not to use weird save detection method.
   *
   * @param {object} newData object returned by the server
   */
  handleSaveRequestWasSuccessful(newData) {
    // We check if data was moderated
    let message = "Les données ont été enregistrées avec succès !";
    const { lastUpdateTimeInModel } = this.props; // at this point it will be still the previously stored data
    const newUpdateTimeInModel = newData.updated_on;

    // Some models are not subject to moderation so they wan't have update_time
    // And the value will be undefined.
    if (this.props.hasPendingModeration !== false) {
      if (
        typeof lastUpdateTimeInModel !== "undefined" &&
        lastUpdateTimeInModel === newUpdateTimeInModel
      ) {
        message =
          "Les données ont été enregistrées et sont en attentes de modération.";
      }
    }
    this.editorManager.notifySaveSuccessful(message);
    this.closeEditor(true);
  }

  /**
   * Function to handle performClose editor request from the user.
   * It checks if there is data to save or not.
   *
   */
  handleCloseEditorRequest = () => {
    if (this.formManager.hasChanges()) {
      this.alertChangesNotSaved();
    } else {
      this.closeEditor(false);
    }
  };

  /**
   * Effectively performClose the editor window and notify if there was something new that was saved
   *
   * @param {Boolean} somethingWasSaved
   */
  closeEditor(somethingWasSaved = false) {
    FullScreenDialogService.closeDialog();
    const { onClose } = this.props;
    onClose(somethingWasSaved);
  }

  /**
   * This function is extended to handle all the logic such as
   * - Detecting when there was a successful save
   * - etc.
   *
   */
  componentDidUpdate() {
    // we make to notify if saving to the server has errors
    const { savingHasError } = this.props;
    if (savingHasError.failed) {
      this.alertSaveFailed(JSON.stringify(savingHasError.error, null, 2));
    }
  }

  render() {
    const { Form, license } = this.props;
    return (
      <FullScreenDialogFrame
        handleCloseRequest={this.handleCloseEditorRequest}
        title="Mode édition"
        rightButton={
          <Button color="inherit" onClick={this.handleSaveEditorRequest}>
            Enregistrer
          </Button>
        }
      >
        <LicenseNotice variant={license} />
        <FormContext.Provider value={{ formManager: this.formManager }}>
          <Form />
        </FormContext.Provider>
      </FullScreenDialogFrame>
    );
  }

  // /////////////////
  // Notification and alert related functions

  // Alerts related
  alertSaveFailed(error) {
    this.editorManager.removeSavingNotification();
    AlertService.open({
      info: true,
      title: "L'enregistrement sur le serveur a échoué.",
      description: `Vous pourrez réessayer après avoir fermer cette alerte. Si l'erreur persiste, vérifier votre connexion internet ou contacter les administrateurs du site.\n\n${error}`,
      infoText: "J'ai compris",
      handleResponse: () => {
        this.props.clearSaveError();
      },
    });
  }

  alertChangesNotSaved() {
    AlertService.open({
      info: false,
      title: "Modifications non enregistrées !",
      description:
        "Vous avez des modifications qui n'ont pas été sauvegardées. Voulez-vous les enregistrer ?",
      agreeText: "Oui, je les enregistre",
      disagreeText: "Non",
      handleResponse: (agree) => {
        if (agree) {
          this.handleSaveEditorRequest();
        } else {
          this.closeEditor();
        }
      },
    });
  }
}

Editor.propTypes = {
  subscribeToModuleWrapper: PropTypes.func,
  rawModelData: PropTypes.object.isRequired,
  // props added in subclasses but are absolutely required to handle state managment
  savingHasError: PropTypes.object.isRequired,
  clearSaveError: PropTypes.func.isRequired,
  lastUpdateTimeInModel: PropTypes.string,
  hasPendingModeration: PropTypes.bool,
  saveData: PropTypes.func.isRequired,
  saveOnMount: PropTypes.bool.isRequired, // should we save the data on editor mount (useful when trying to moderate)
  Form: PropTypes.func.isRequired, // Components with the fields
  formLevelErrors: PropTypes.array,
  license: PropTypes.string,

  onClose: PropTypes.func,
};

Editor.defaultProps = {
  formLevelErrors: [],
  license: "REX-DRI—BY",
  subscribeToModuleWrapper: () => {},
  lastUpdateTimeInModel: undefined,
  hasPendingModeration: undefined,
  // eslint-disable-next-line no-unused-vars
  onClose: (somethingWasSaved) => {},
};

export default React.memo(Editor);
