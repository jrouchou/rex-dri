import React, { useEffect } from "react";

import PropTypes from "prop-types";
import compose from "recompose/compose";
import Button from "@material-ui/core/Button";
import Pagination from "@material-ui/lab/Pagination";
import { makeStyles } from "@material-ui/styles";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import RequestParams from "../../utils/api/RequestParams";
import FullScreenDialogService from "../../services/FullScreenDialogService";
import FullScreenDialogFrame from "../common/FullScreenDialogFrame";
import dateTimeStrToStr from "../../utils/dateTimeStrToStr";
import withNetworkWrapper, { NetWrapParam } from "../../hoc/withNetworkWrapper";
import { useApiInvalidateAll } from "../../hooks/wrappers/api";

const useStyles = makeStyles((theme) => ({
  editButton: {
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
  center: {
    margin: "0 auto",
    width: "max-content",
  },
}));

function VersionInfo({ rawModelData, versionNumber, editFromVersion }) {
  const classes = useStyles();

  let dateInfo = <em>(Information non connue.)</em>;
  const { updated_on } = rawModelData;
  if (updated_on) {
    const data = dateTimeStrToStr(updated_on);
    dateInfo = `${data.date} à ${data.time}`;
  }
  return (
    <>
      <Typography variant="caption" align="center">
        Les versions successives d'un même utilisateur ne sont pas enregistrés
        (dans de tels cas, seul la dernière est conservée).
      </Typography>
      <Typography variant="h6" align="center">
        {`Version n°${versionNumber + 1} du ${dateInfo}`}
      </Typography>
      <Button
        variant="outlined"
        color="primary"
        className={classes.editButton}
        onClick={() => editFromVersion(rawModelData)}
      >
        Éditer à partir de cette version
      </Button>
    </>
  );
}

VersionInfo.propTypes = {
  rawModelData: PropTypes.object.isRequired,
  versionNumber: PropTypes.number.isRequired,
  editFromVersion: PropTypes.func.isRequired,
};

function History({
  versions,
  renderTitle,
  renderCore,
  editFromVersion,
  rawModelDataEx,
}) {
  const classes = useStyles();
  const resetVersions = useApiInvalidateAll("versions");

  useEffect(() => {
    return () => resetVersions(); // only on unmount
  }, []);

  const maxSteps = versions.length;
  const [currentVersion, setCurrentVersion] = React.useState(maxSteps);

  const handleChange = (event, value) => {
    setCurrentVersion(value);
  };

  const newRawModelData = {
    ...rawModelDataEx,
    ...versions[currentVersion - 1].data,
  };

  return (
    <FullScreenDialogFrame
      handleCloseRequest={FullScreenDialogService.closeDialog}
      title="Parcours de l'historique"
    >
      <>
        <div className={classes.center}>
          <Pagination
            count={maxSteps}
            page={currentVersion}
            onChange={handleChange}
            showFirstButton
            showLastButton
            color="secondary"
          />
        </div>
        <br />
        <Divider />
        <VersionInfo
          rawModelData={newRawModelData}
          versionNumber={currentVersion - 1}
          editFromVersion={editFromVersion}
        />
        <br />
        {renderTitle(newRawModelData)}
        {renderCore(newRawModelData)}
      </>
    </FullScreenDialogFrame>
  );
}

History.propTypes = {
  editFromVersion: PropTypes.func.isRequired,
  versions: PropTypes.array.isRequired,
  renderCore: PropTypes.func.isRequired,
  renderTitle: PropTypes.func.isRequired,
  rawModelDataEx: PropTypes.object.isRequired,
};

const buildParams = (modelInfo) => {
  const { contentTypeId, id } = modelInfo;
  return RequestParams.Builder.withEndPointAttrs([contentTypeId, id]).build();
};

export default compose(
  withNetworkWrapper([
    new NetWrapParam("versions", "all", {
      addDataToProp: "versions",
      params: (props) => buildParams(props.modelInfo),
      propTypes: {
        modelInfo: PropTypes.shape({
          contentTypeId: PropTypes.number.isRequired,
          id: PropTypes.number.isRequired,
        }).isRequired,
      },
    }),
  ])
)(History);
