import React from "react";
import { setDisplayName } from "recompose";
import pick from "lodash/pick";
import UnivContext from "../contexts/UnivContext";

/**
 * HOC (higher order component) wrapper to provide information to components.
 *
 *
 * The component being wrapped must me in the same subtree as the UnivInfoProvider children
 *
 * @param {Array.<string>} propertiesToAdd List of the properties to add from UnivInfoProvider
 * @returns {function(*): function(*): *}
 */
export default function withUnivInfo(propertiesToAdd = []) {
  return (Component) =>
    setDisplayName("UnivInfoConsumer")(
      // We need to forward the ref to make sure the styles are correctly applied.
      // eslint-disable-next-line react/display-name
      React.forwardRef((props, ref) => (
        <UnivContext.Consumer>
          {(infos) => (
            <Component
              {...props}
              univId={infos.univId}
              {...pick(infos, propertiesToAdd)}
              ref={ref}
            />
          )}
        </UnivContext.Consumer>
      ))
    );
}
