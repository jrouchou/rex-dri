/**
 * Quick & dirty way to deep copy an object
 * @param obj
 * @returns {object}
 */
export default function deepCopy(obj) {
  return JSON.parse(JSON.stringify(obj));
}
