import {
  getUpdatedMajor,
  getUpdatedMinor,
  getAllMajors,
  getAllMinors,
  getUpdatedUnivMajorMinors,
} from "./majorMinorMappings";

test("GSM => IM", () => {
  const major = "GSM";
  expect(getUpdatedMajor(major)).toBe("IM");
});

test("GM => IM", () => {
  const major = "GM";
  expect(getUpdatedMajor(major)).toBe("IM");
});

test("GI => GI", () => {
  const major = "GI";
  expect(getUpdatedMajor(major)).toBe("GI");
});

test("IM => IM, GM, GSM", () => {
  const major = "IM";
  expect(getAllMajors(major).sort()).toEqual(["IM", "GSM", "GM"].sort());
});

test("GU => GU, GSU", () => {
  const major = "GU";
  expect(getAllMajors(major).sort()).toEqual(["GU", "GSU"].sort());
});

test("(_DUMMY_MAJOR_1, _DUMMY_MINOR_2) => _DUMMY_MINOR_1", () => {
  const major = "_DUMMY_MAJOR_1";
  const minor = "_DUMMY_MINOR_2";
  expect(getAllMinors(major, minor).sort()).toEqual(
    ["_DUMMY_MINOR_2", "_DUMMY_MINOR_1"].sort()
  );
});

test("BR - FIL => BR - FIL", () => {
  const major = "BR";
  const minor = "FIL";
  expect(getUpdatedMinor(major, minor)).toBe("FIL");
});

test("univMajorMinors [] => []", () => {
  expect(getUpdatedUnivMajorMinors([])).toEqual([]);
});

test("univMajorMinors dedup", () => {
  const input = [
    { major: "IM", minors: ["CMI"] },
    { major: "GSM", minors: ["CMI"] },
    { major: "GM", minors: ["MAT"] },
  ];
  const output = [{ major: "IM", minors: ["CMI", "MAT"] }];
  expect(getUpdatedUnivMajorMinors(input)).toEqual(output);
});
