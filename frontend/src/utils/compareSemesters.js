/**
 * Compares two semesters
 *
 * @param {number} year1
 * @param {"a"|"p"} period1
 * @param {number} year2
 * @param {"a"|"p"} period2
 * @export
 * @returns {number}
 */
export default function compareSemesters(year1, period1, year2, period2) {
  if (year1 === year2) {
    return -period1.localeCompare(period2);
  }
  return year1 - year2;
}

/**
 *
 * @param {Array.<String>} listOfSemesters
 * @param {number} n
 * @return {Array.<String>}
 */
export function getMostNRecentSemesters(listOfSemesters, n) {
  const nToUse = Math.min(n, listOfSemesters.length);

  const semesters = listOfSemesters.map((str) => ({
    semester: str[0],
    year: str.slice(1, 5),
  }));
  semesters.sort(
    (a, b) => -compareSemesters(a.year, a.semester, b.year, b.semester)
  );
  return semesters
    .slice(0, nToUse)
    .map(({ semester, year }) => `${semester}${year}`);
}
