import parseMoney from "./parseMoney";

test("parse empty string", () => {
  const str = "";
  expect(parseMoney(str).length).toBe(0);
});

test("Parse string with no money", () => {
  const str = "A random classic string";
  expect(parseMoney(str).length).toBe(1);
  expect(parseMoney(str)[0].text).toBe(str);
});

test("Parse string with only money", () => {
  const str = ":100CHF:",
    parsed = parseMoney(str);
  expect(parsed.length).toBe(1);
  expect(parsed[0].amount).toBe(100);
  expect(parsed[0].currency).toBe("CHF");
});

test("Parse complicated string", () => {
  const str = "Hi, I earn :0,0Chf: but he earned :100.12EUR: this year !",
    parsed = parseMoney(str);

  expect(parsed.length).toBe(5);

  expect(parsed[0].isMoney).toBe(false);
  expect(parsed[1].isMoney).toBe(true);
  expect(parsed[2].isMoney).toBe(false);
  expect(parsed[3].isMoney).toBe(true);
  expect(parsed[4].isMoney).toBe(false);

  expect(parsed[0].text).toBe("Hi, I earn ");
  expect(parsed[2].text).toBe(" but he earned ");
  expect(parsed[4].text).toBe(" this year !");

  expect(parsed[1].amount).toBe(0);
  expect(parsed[3].amount).toBe(100.12);

  expect(parsed[1].currency).toBe("CHF");
  expect(parsed[3].currency).toBe("EUR");
});

test("Money directly in code is returned as text", () => {
  const str = "You can use `:120CHF:` to tag money infos",
    parsed = parseMoney(str);

  expect(parsed.length).toBe(1);
  expect(parsed[0].isMoney).toBe(false);
});
