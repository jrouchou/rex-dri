/*
 * This file contains the functions and class to create the CRUD reducers
 */

import getCrudActionTypes from "./getCrudActionTypes";
import RequestParams from "./RequestParams";

/**
 *
 * Helper functions to help standardize
 *
 */

function getSucceeded(action, readAt = Date.now()) {
  return {
    data: action.data,
    requestParams: action.requestParams,
    readAt,
  };
}

function getDefaultSucceeded(data) {
  return getSucceeded(
    {
      data,
      requestParams: RequestParams.Builder.build(),
    },
    0
  );
}

function getFailed(action) {
  return {
    failed: true,
    error: action.error,
  };
}

function getDefaultFailed() {
  return {
    failed: false,
    error: null,
  };
}

/**
 * Class to build the reducers associated with an api route
 * @class CrudReducers
 */
export default class CrudReducers {
  /**
   *Creates an instance of CrudReducers.
   * @param {string} route api route
   * @memberof CrudReducers
   */
  constructor(route) {
    this.types = getCrudActionTypes(route);
  }

  static get defaultAllState() {
    return {
      isReading: false,
      readSucceeded: getDefaultSucceeded([]),
      readFailed: getDefaultFailed(),
      isInvalidated: false,
    };
  }

  /**
   * Get the reducer for actions related to all (read all)
   *
   * @returns {function}
   * @memberof CrudReducers
   */
  getForAll() {
    const self = this;

    return (state = CrudReducers.defaultAllState, action) => {
      // Performance optimization: we don't go through the big switch
      if (!action.rootType || action.rootType !== self.types.rootType) {
        return state;
      }

      const stateUpdate = {};
      switch (action.type) {
        case self.types.readAllStarted:
          stateUpdate.isReading = true;
          break;
        case self.types.readAllSucceeded:
          stateUpdate.isReading = false;
          stateUpdate.isInvalidated = false;
          stateUpdate.readSucceeded = getSucceeded(action);
          break;
        case self.types.readAllFailed:
          stateUpdate.isReading = false;
          stateUpdate.readFailed = getFailed(action);
          break;
        case self.types.clearReadAllFailed:
          stateUpdate.readFailed = getDefaultFailed();
          break;
        case self.types.invalidateAll:
          stateUpdate.isInvalidated = true;
          break;
        case self.types.clearInvalidationAll:
          stateUpdate.isInvalidated = false;
          break;
        default:
          break;
      }

      // Create a new object with the correct state
      return { ...state, ...stateUpdate };
    };
  }

  static get defaultOneState() {
    return {
      isReading: false,
      readSucceeded: getDefaultSucceeded(Object()),
      readFailed: getDefaultFailed(),
      //
      isCreating: false,
      createSucceeded: getDefaultSucceeded(Object()),
      createFailed: getDefaultFailed(),
      //
      isUpdating: false,
      updateSucceeded: getDefaultSucceeded(Object()),
      updateFailed: getDefaultFailed(),
      //
      isDeleting: false,
      deleteSucceeded: getDefaultSucceeded(""),
      deleteFailed: getDefaultFailed(),
      //
      isInvalidated: false,
    };
  }

  /**
   * Get the reducer for all actions releted to one element
   *
   * @returns {function}
   * @memberof CrudReducers
   */
  getForOne() {
    const self = this;

    return (state = CrudReducers.defaultOneState, action) => {
      // Performance optimization: we don't go through the big switch
      if (!action.rootType || action.rootType !== self.types.rootType) {
        return state;
      }

      const stateUpdate = {};

      switch (action.type) {
        // ////////
        // READ
        // ////////
        case self.types.readOneStarted:
          stateUpdate.isReading = true;
          break;
        case self.types.readOneSucceeded:
          stateUpdate.isReading = false;
          stateUpdate.isInvalidated = false;
          stateUpdate.readFailed = getDefaultFailed();
          stateUpdate.readSucceeded = getSucceeded(action);
          break;
        case self.types.readOneFailed:
          stateUpdate.isReading = false;
          stateUpdate.readFailed = getFailed(action);
          break;
        case self.types.clearReadOneFailed:
          stateUpdate.readFailed = getDefaultFailed();
          break;
        // ////////
        // CREATE
        // ////////
        case self.types.createStarted:
          stateUpdate.isCreating = true;
          break;
        case self.types.createSucceeded:
          stateUpdate.isCreating = false;
          stateUpdate.createFailed = getDefaultFailed();
          stateUpdate.createSucceeded = getSucceeded(action);
          break;
        case self.types.createFailed:
          stateUpdate.isCreating = false;
          stateUpdate.createFailed = getFailed(action);
          break;
        case self.types.clearCreateFailed:
          stateUpdate.createFailed = getDefaultFailed();
          break;
        // ////////
        // UPDATE
        // ////////
        case self.types.updateStarted:
          stateUpdate.isUpdating = true;
          break;
        case self.types.updateSucceeded:
          stateUpdate.isUpdating = false;
          stateUpdate.updateFailed = getDefaultFailed();
          stateUpdate.updateSucceeded = getSucceeded(action);
          break;
        case self.types.updateFailed:
          stateUpdate.isUpdating = false;
          stateUpdate.updateFailed = getFailed(action);
          break;
        case self.types.clearUpdateFailed:
          stateUpdate.updateFailed = getDefaultFailed();
          break;
        // ////////
        // DELETE
        // ////////
        case self.types.deleteStarted:
          stateUpdate.isDeleting = true;
          break;
        case self.types.deleteSucceeded:
          stateUpdate.isDeleting = false;
          stateUpdate.deleteFailed = getDefaultFailed();
          stateUpdate.deleteSucceeded = getSucceeded(action);
          break;
        case self.types.deleteFailed:
          stateUpdate.isDeleting = false;
          stateUpdate.deleteFailed = getFailed(action);
          break;
        case self.types.clearDeleteFailed:
          stateUpdate.deleteFailed = getDefaultFailed();
          break;
        // ///////////
        // Invalidate
        // ///////////
        case self.types.invalidateOne:
          stateUpdate.isInvalidated = true;
          break;
        case self.types.clearInvalidationOne:
          stateUpdate.isInvalidated = false;
          break;
        default:
          break;
      }

      // Create a new object with the correct state
      return { ...state, ...stateUpdate };
    };
  }
}
