/**
 * This file contains utilities linked to the use of the API.
 */

// Stores the name of the reducers/actions that result in read data
export const successActionsWithReads = [
  "readSucceeded",
  "createSucceeded",
  "updateSucceeded",
];

/**
 * Smartly retrieve the latest read (create and update included) data from the server
 *
 * @export
 * @param {object} stateExtract
 */
export function getLatestRead(stateExtract) {
  return successActionsWithReads
    .filter((action) => action in stateExtract) // general handling of all types of API reducers
    .map((action) => stateExtract[action])
    .reduce((prev, curr) => (prev.readAt < curr.readAt ? curr : prev), {
      readAt: 0,
    });
}

export function apiDataIsUsable(stateExtract) {
  if (typeof stateExtract === "undefined") return false;
  return (
    !stateExtract.isInvalidated &&
    successActionsWithReads
      .filter((action) => action in stateExtract) // general handling of all types of API reducers
      .some((action) => stateExtract[action].readAt !== 0) && // makes sure will consider all success actions
    ["isReading"] // , "isUpdating", "isCreating"] Don't put those in here it may cause unwanted rerendering whole tree when saving
      .filter((action) => action in stateExtract)
      .every((action) => stateExtract[action] === false)
  );
}
