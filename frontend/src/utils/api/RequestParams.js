import isEqual from "lodash/isEqual";

/**
 * Class to hold data to pass to actions and the axios to make network request.
 *
 * Comes in with a nice Builder (that you shall use)...
 */
class RequestParams {
  /**
   * @param {Builder} builder
   * @protected
   */
  constructor(builder) {
    this._id = builder._id;
    this._data = builder._data;
    this._queryParams = builder._queryParams;
    this._endPointAttrs = builder._endPointAttrs;
    this._onSuccessCallback = builder._onSuccessCallback;
  }

  static get Builder() {
    class Builder {
      /**
       * @type {(string|number)}
       * @protected
       */
      _id = undefined;

      /**
       * @type {object.<string, (string|number)>}
       * @protected
       */
      _queryParams = {};

      /**
       * @type {Array.<string>}
       * @protected
       */
      _endPointAttrs = [];

      /**
       * @type {function(object):*}
       * @protected
       */
      _onSuccessCallback = () => {};

      /**
       * @type {object}
       * @protected
       */
      _data = {};

      /**
       * Specify the id of the object (will be appended last to the URL of the request)
       *
       * @param {string|number} id
       * @returns {Builder}
       */
      withId(id) {
        this._id = id;
        return this;
      }

      /**
       * Data to transfer to the API as a JSON payload.
       * @param {object} data
       */
      withData(data) {
        this._data = data;
        return this;
      }

      /**
       * Add a queryParams to the request object, will be overriden if already present
       *
       * @param {string} key
       * @param {(string|number)} value
       * @returns {Builder}
       */
      withQueryParam(key, value) {
        this._queryParams[key] = value;
        return this;
      }

      /**
       * Add the endPointAttrs to the request
       *
       * @param {Array.<string>} endPointAttrs
       * @returns {Builder}
       */
      withEndPointAttrs(endPointAttrs) {
        this._endPointAttrs = endPointAttrs;
        return this;
      }

      /**
       * Register a callback that will be called when the request is successful.
       * The data that is returned by the API will be given as parameter to the callback.
       *
       * @param {function(object):*} callback
       * @returns {Builder}
       */
      withOnSuccessCallback(callback) {
        this._onSuccessCallback = callback;
        return this;
      }

      /**
       * Validate the builder setup
       * @private
       */
      _validate() {
        if (
          typeof this._id !== "undefined" &&
          this._id !== "" &&
          Object.entries(this._queryParams).length !== 0
        ) {
          throw new Error(
            "An 'id' cannot be configured alongside some query params."
          );
        }
      }

      /**
       * Build the RequestParams instance (to be called last)
       * @returns {RequestParams}
       */
      build() {
        this._validate();
        return new RequestParams(this);
      }
    }

    return new Builder();
  }

  get data() {
    return this._data;
  }

  get id() {
    return this._id;
  }

  get queryParams() {
    return this._queryParams;
  }

  get endPointAttrs() {
    return this._endPointAttrs;
  }

  get onSuccessCallback() {
    return this._onSuccessCallback;
  }

  /**
   * Builds the correct URL for the request
   *
   * @returns {string}
   */
  get url() {
    let queryParams = Object.entries(this.queryParams)
      .map(([key, val]) => `${key}=${val}`)
      .join("&");
    if (queryParams !== "") {
      queryParams = `?${queryParams}`;
    }

    // Then we build the final URL
    let out;
    if (this.checkHasId(false)) {
      out = [...this.endPointAttrs, this.id, ""].join("/");
    } else {
      out = [...this.endPointAttrs, queryParams].join("/");
    }

    // clean the end url without doubles //
    return out.replace(/\/+/g, "/");
  }

  /**
   * @param {RequestParams} other
   * @returns {boolean}
   */
  equals(other) {
    if (typeof other === "undefined") return false;
    return (
      String(this.id) === String(other.id) &&
      isEqual(this.endPointAttrs, other.endPointAttrs) &&
      isEqual(this.queryParams, other.queryParams) &&
      isEqual(this.data, other.data)
    );
  }

  /**
   * Function to check that a valid id is present.
   *
   * @param {boolean} shouldThrow if not, should we throw ?
   * @returns {boolean}
   */
  checkHasId(shouldThrow = true) {
    if (typeof this.id === "undefined" || this.id === "") {
      if (shouldThrow) {
        throw new Error("Missing id in 'RequestParams'");
      }
      return false;
    }
    return true;
  }

  /**
   * Function to check that no valid id is present.
   *
   * @param {boolean} shouldThrow if not, should we throw ?
   * @returns {boolean}
   */
  checkDoesntHaveId(shouldThrow = true) {
    if (typeof this.id !== "undefined" && this.id !== "") {
      if (shouldThrow) {
        throw new Error("You shouldn't specify an id in 'RequestParams'");
      }
      return false;
    }
    return true;
  }
}

export default RequestParams;
