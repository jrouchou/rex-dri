/**
 * Function to generate CRUD action types for actions and reducers
 *
 * @export
 * @param {string} name
 * @returns
 */
export default function getCrudActionTypes(name) {
  return {
    readAllStarted: `API_${name}_READ_ALL_STARTED`,
    readAllSucceeded: `API_${name}_READ_ALL_SUCCEEDED`,
    readAllFailed: `API_${name}_READ_ALL_FAILED`,
    clearReadAllFailed: `API_${name}_CLEAR_READ_ALL_FAILED`,
    //
    readOneStarted: `API_${name}_READ_ONE_STARTED`,
    readOneSucceeded: `API_${name}_READ_ONE_SUCCEEDED`,
    readOneFailed: `API_${name}_READ_ONE_FAILED`,
    clearReadOneFailed: `API_${name}_CLEAR_READ_ONE_FAILED`,
    //
    createStarted: `API_${name}_CREATE_STARTED`,
    createSucceeded: `API_${name}_CREATE_SUCCEEDED`,
    createFailed: `API_${name}_CREATE_FAILED`,
    clearCreateFailed: `API_${name}_CLEAR_CREATE_FAILED`,
    //
    updateStarted: `API_${name}_UPDATE_STARTED`,
    updateSucceeded: `API_${name}_UPDATE_SUCCEEDED`,
    updateFailed: `API_${name}_UPDATE_FAILED`,
    clearUpdateFailed: `API_${name}_CLEAR_UPDATE_FAILED`,
    //
    deleteStarted: `API_${name}_DELETE_STARTED`,
    deleteSucceeded: `API_${name}_DELETE_SUCCEEDED`,
    deleteFailed: `API_${name}_DELETE_FAILED`,
    clearDeleteFailed: `API_${name}_CLEAR_DELETE_FAILED`,
    //
    // Not directly a CRUD action but needed for in app behavior
    invalidateAll: `API_${name}_ALL_INVALIDATED`,
    clearInvalidationAll: `API_${name}_ALL_INVALIDATION_CLEARED`,
    invalidateOne: `API_${name}_ONE_INVALIDATED`,
    clearInvalidationOne: `API_${name}_ONE_INVALIDATION_CLEARED`,
    // One more to optimize performances a bit
    rootType: `API_ROOT_${name}`,
  };
}
