/**
 * React context to hold university information for optimization purposes.
 */
import React from "react";

const UnivContext = React.createContext();
export default UnivContext;
