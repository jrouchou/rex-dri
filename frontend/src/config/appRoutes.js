import { CURRENT_USER_ID } from "./user";

const base = "/app/";
const map = `${base}map/`;
const search = `${base}search/`;
const university = `${base}university/`;
const forUniversityTab = (univId, tab = "general") =>
  `${university}${univId}/${tab}`;
const forUniversity = (univId) => forUniversityTab(univId);
const universityWithParams = `${university}:univId?/:tabName?`;
const user = `${base}user/`;
const forUser = (userId) => `${user}${userId}`;
const userCurrent = forUser(CURRENT_USER_ID);
const userWithParams = `${user}:userId`;
const myExchanges = `${base}my/exchanges/`;
const editPreviousExchange = `${base}edit/exchangeFeedback/`;
const editPreviousExchangeWithParams = `${editPreviousExchange}:exchangeId`;
const editForPreviousExchange = (exchangeId) =>
  `${editPreviousExchange}${exchangeId}`;
const userFiles = `${base}files/`;
const userFilesWithParams = `${userFiles}:userId`;
// eslint-disable-next-line no-undef
const userFilesCurrent = `${userFiles}${__AppUserId}`;
const lists = `${base}lists/`;
const listsWithParams = `${lists}:listId?`;
const forList = (listId) => `${lists}${listId}`;
const settings = `${base}settings/`;
const themeSettings = `${settings}theme/`;
const about = `${base}about/`;
const aboutProject = `${about}project/`;
const aboutRgpd = `${about}rgpd/`;
const aboutCgu = `${about}cgu/`;
const aboutUnlinkedPartners = `${about}unlinked-partners/`;
const logout = "/user/logout";
const stats = "/stats";

const APP_ROUTES = {
  base,
  map,
  search,
  university,
  forUniversity,
  forUniversityTab,
  user,
  forUser,
  userCurrent,
  userWithParams,
  myExchanges,
  editPreviousExchange,
  editPreviousExchangeWithParams,
  editForPreviousExchange,
  userFiles,
  userFilesCurrent,
  userFilesWithParams,
  lists,
  forList,
  listsWithParams,
  settings,
  universityWithParams,
  themeSettings,
  about,
  aboutProject,
  aboutCgu,
  aboutRgpd,
  aboutUnlinkedPartners,
  stats,
  logout,
};

export default APP_ROUTES;
