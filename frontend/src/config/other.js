import cgu from "../../../documentation/Other/cgu.md";
import rgpd from "../../../documentation/Other/rgpd.md";

export const RGPD_MARKDOWN_SOURCE = rgpd;
export const CGU_MARKDOWN_SOURCE = cgu;
