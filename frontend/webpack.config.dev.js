const merge = require("webpack-merge");
const webpackConfig = require("./webpack.config.base");
const webpack = require("webpack");

const devConfig = merge(webpackConfig, {
  mode: "development",
  devtool: "eval-source-map",
  entry: {
    main: [
      "webpack-dev-server/client?http://localhost:3000",
      "webpack/hot/only-dev-server",
    ],
    stats: [
      "webpack-dev-server/client?http://localhost:3000",
      "webpack/hot/only-dev-server",
    ],
    rgpdCgu: [
      "webpack-dev-server/client?http://localhost:3000",
      "webpack/hot/only-dev-server",
    ],
    rgpdRaw: [
      "webpack-dev-server/client?http://localhost:3000",
      "webpack/hot/only-dev-server",
    ],
  },
  optimization: {
    minimize: false,
  },
  output: {
    publicPath: "http://localhost:3000/dist/bundles/",
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.NoEmitOnErrorsPlugin(), // don't reload if there is an error
  ],
});

module.exports = devConfig;
