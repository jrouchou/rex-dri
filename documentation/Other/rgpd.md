# Mention d'information RGPD plateforme REX-DRI

_N.B.: Les URLs [https://rex.dri.utc.fr](https://rex.dri.utc.fr) et [https://assos.utc.fr/rex-dri/](https://assos.utc.fr/rex-dri/) sont équivalentes._

Le SIMDE (Service Informatique de la Maison Des Étudiants – commission du Bureau Des Étudiants de l'Université de Technologie de Compiègne) met à disposition la plateforme REX-DRI ([https://rex.dri.utc.fr](https://rex.dri.utc.fr)). Elle a pour finalité le partage de retours d'expérience et d'informations générales sur les échanges universitaires. Ce afin d'aider les étudiants à choisir leurs destinations et vivre au mieux leurs expériences d'échanges (de la candidature au retour en France).

La base légale du traitement est le **consentement** (article 6 du règlement européen 2016/679, dit RGPD - Règlement général sur la protection des données personnelles).

---

**Les données traitées sur tout utilisateur de la plateforme sont :**

1. Vos nom **\***, prénom **\***, login UTC **\***, identifiant unique **\*** et adresse mail UTC **\*** fournis par le système d'authentification de l'UTC ([https://cas.utc.fr/](https://cas.utc.fr/)),
2. Si vous les renseignez dans l'application, une adresse mail secondaire et votre pseudo,
3. Si vous en donnez l'autorisation sur l'ENT, les cours que vous avez suivis à l'étranger, associés à votre login UTC,
4. La date de votre dernière connexion,
5. Les données sous licence REX-DRI private,
6. Les traces de toutes les requêtes effectuées sur la plateforme.

**Les destinataires de ces données sont :**

- Les administrateurs de la plateforme: toutes ces données,
- Les utilisateurs de la plateforme peuvent accéder aux catégories de données 1, 2 et 3 (données d'identification et cours suivis à l'étranger) uniquement après votre consentement explicite.

**Durée de conservation des données**

- 1, 2, 3, 4 et 5 : au plus 5 ans après la dernière connexion,
- 6 : au plus 31 jours.

**Gestion des traces informatiques (6)**

La gestion des traces informatiques est obligatoire et a notamment pour finalité l'analyse de l'utilisation de la plateforme et la détection de défaillances. Passer le délai de 31 jours précédemment mentionné, les traces informatiques sont susceptibles d'être conservées dans un format garantissant l'anonymat des données.

---

Conformément au règlement européen 2016/679 dit RGPD, vous pouvez retirer votre consentement à tout moment et demander l'effacement des données vous concernant. Pour cela, rendez vous simplement sur la page prévue à cet effet ([https://rex.dri.utc.fr/app/user/me](https://rex.dri.utc.fr/app/user/me)) et cliquez sur le bouton “Supprimer mon compte”. Vous pouvez à tout moment vous rendre sur l'ENT et supprimer l'autorisation de partage des cours que vous avez suivis lors de votre échange.

Vous disposez également d'un droit d'accès, de rectification et d'opposition aux informations qui vous concernent ainsi qu'un droit à la limitation du traitement et à la portabilité de ces données, droits que vous pouvez exercer en vous adressant au SIMDE ([simde@assos.utc.fr](mailto:simde@assos.utc.fr)).

Pour votre droit à la portabilité de vos données, nous vous invitons à vous rendre dans un premier temps sur l'API ([https://rex.dri.utc.fr/api/](https://rex.dri.utc.fr/api/)) de la plateforme, et à télécharger vos données.

Si vous estimez, après nous avoir contactés, que vos droits sur vos données ne sont pas respectés, vous pouvez adresser une réclamation à la CNIL.

**\***: les données marquées d'un astérisque sont fournies par le système d'authentification centralisé du SIMDE et ne sont pas modifiables via la plateforme REX-DRI.
