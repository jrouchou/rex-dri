# Credits

- Favicon & website logo: Icon made by [Smashicons](https://www.flaticon.com/authors/smashicons) from [flaticon.com](https://www.flaticon.com/). It is licensed under the [CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/) license.
