# Conditions générales d'utilisation de la plateforme REX-DRI

_N.B.: Les URLs [https://rex.dri.utc.fr](https://rex.dri.utc.fr) et [https://assos.utc.fr/rex-dri/](https://assos.utc.fr/rex-dri/) sont équivalentes._

Les présentes « Conditions générales d'utilisation » (CGU) ont pour objet l'encadrement juridique des modalités de mise à disposition des services de la plateforme **REX-DRI** à ses utilisateurs.

_Le terme **« utilisateur »** désigne toute personne accédant à la plateforme et qui a accepté les présentes CGU._

La plateforme REX-DRI a pour finalité le partage de retours d'expérience et d'informations générales sur les échanges universitaires. Ce afin d'aider les étudiants à choisir leurs destinations et vivre au mieux leurs expériences d'échanges (de la candidature au retour en France).

## Valeurs

Les valeurs de la plateforme REX-DRI sont : **bienveillance**, **partage**, **contribution** et **collaboration**. Aidez-nous à les diffuser !

## Responsabilité

Toute contribution sur la plateforme est associée à son contributeur. La responsabilité d'éditeur incombe au contributeur.

L'utilisateur s'engage à ne pas mettre en ligne de contenu volontairement inexact, trompeur, ou pouvant porter atteinte à autrui. Aucun contenu irrespectueux ne sera toléré. _La législation française s'applique à toute contribution._

En raison du format de la plateforme, le degré de fiabilité des informations mises à disposition est laissé à l'appréciation des utilisateurs ; chacun étant invité à l'améliorer.

## Licences associées aux données

### Définitions des licences

La licence « **REX-DRI—BY** » se définit comme étant une licence CC-BY ([https://creativecommons.org/licenses/by/2.0/fr/](https://creativecommons.org/licenses/by/2.0/fr/)) restreinte dont :

- Le droit à « l'adaptation » est restreint à toute personne physique pouvant accéder à la plateforme REX-DRI ([https://rex.dri.utc.fr](https://rex.dri.utc.fr))
- Le droit au « partage » est restreint au fait que le contenu partagé (adapté ou non) n'est accessible qu'aux utilisateurs de la plateforme REX-DRI ou derrière le système d'authentification central de l'UTC ([https://cas.utc.fr/cas/login](https://cas.utc.fr/cas/login))

La licence « **REX-DRI—PRIVATE** » se définit comme suit :

Sauf mention contraire de votre part, vous détenez tous les droits sur le contenu concerné par cette licence.

Toutefois, vous autorisez le traitement de ce contenu (tant qu'il est accessible) par les administrateurs du site à des fins statistiques.

### Cadre d'utilisation

Sauf mention contraire toute donnée transférée sur la plateforme REX-DRI est sous licence **REX-DRI—BY**.

Pour information, sont concernées par la licence REX-DRI—PRIVATE les listes privées des utilisateurs, les universités que vous avez consultées, la configuration utilisateur, etc.

## Données personnelles

Se référer à la mention d'information ([https://rex.dri.utc.fr/app/about/rgpd/](https://rex.dri.utc.fr/app/about/rgpd/)).

## Condition de mise à disposition

REX-DRI est une ressource partagée. Afin de garantir le meilleur service possible à tous, aucun ne doit en abuser ou tenter de nuire à toute ou partie de la plateforme.

Tout manquement aux présentes CGU pourra donner lieu à des sanctions proportionnées.

## Mentions légales

Le **SIMDE** (Service informatique de la Maison Des Étudiants — commission du Bureau Des Étudiants de l'Université de Technologie de Compiègne) met à disposition la plateforme REX-DRI ([https://rex.dri.utc.fr](https://rex.dri.utc.fr)) pour une durée indéterminée.

contact : [simde@assos.utc.fr](mailto:simde@assos.utc.fr)

Le site est hébergé par l'**UTC** (Université de Technologie de Compiègne).
