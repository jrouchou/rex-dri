# Creating a database dump

To create a backup of the production database follow these instructions:

- SSH to the REX-DRI VM @UTC, then

```bash
rm -rf /tmp/rex-dri.*
sudo docker exec server_database_1 bash -c 'pg_dump -Fc --no-owner -d postgres -U postgres > /tmp/rex-dri.dump'

sudo docker cp server_database_1:/tmp/rex-dri.dump /tmp/rex-dri.dump

sudo chown $(whoami):$(whoami) /tmp/rex-dri.dump
# Encrypt the database dump
cd /tmp
GPG_TTY=$(tty) gpg-zip -c -o rex-dri.dump.gpg rex-dri.dump
```

- Copy the encrypted dump to your computer:

```bash
rm -rf /tmp/rex-dri.*
scp user@rex-dri-host:/tmp/rex-dri.dump.gpg /tmp/rex-dri.dump.gpg
```

The dump will be available on your computer at location `/tmp/rex-dri.dump.gpg`.

:warning: **Never share the decrypted version of the dump.**
