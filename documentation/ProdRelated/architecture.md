# Production architecture

Bellow is a quick nasty schema of the deploy architecture:

![deploy architecture](./deploy_architecture.svg)

The important stuff to remember:

- All clients/browsers request comes in on port `80` where `nginx` is listening.
- Depending on the requested URI, `nginx` will:
  - Transfer the request to the backend (through a unix socket stored in docker volume),
  - Serve static files directly.
  - (media files are served by `nginx` after authentification)
- The backend runs on top of `uWSGI`.
- The database and the backend are on a private network.
- All logs are handled through docker volumes and a dedicated service that rotate the logs each day and keep 30 rotations (/days) of logs.

All request coming in `nginx` are logged. backend server errors are logged. If a frontend crash occurs a logging request is sent to the backend that will log it in a dedicated file.
