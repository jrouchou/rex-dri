# Frontend tests

_Rex-DRI_ comes in with several _tests_ that you can perform locally using the commands `make test_backend` and `make test_frontend` once the project is up and running (`make up`).

?> :information_desk_person: As of now, the frontend tests only concern specific functions and not react components.

The frontend tests are performed with `jest` framework and are contained in the `frontend/tests` directory. For your tests to be taken into account, the file name must end with `.test.js`.

To create tests, the syntaxe is fairly simple:

```js
test("parse empty string", () => {
  const str = "";
  expect(parseMoney(str).length).toBe(0);
});
```

You simply need to wrap your test in the `test` function (no need to import it, it will be provided automatically when testing). This function takes the name of your test as first argument and a function to execute as second argument.

Inside the second argument function you can use the wrapper `expect` and then use the `toBe` attribute to check that what is returned is something specific.

Don't miss the [`jest` documentation](https://jestjs.io/docs/en/getting-started) for more examples.
