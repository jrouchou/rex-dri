# Map -- info

## Why hosting our own maps ?

We decided to host our own maps in order to provide this service for free for ever.

This has several drawbacks:

- The tiles are not really up to date (unless we update them regurlarly but this process is complicated and costly),
- We will provide only a reasonable level zoom so that we don't store a huge volumetry of data.

And some awesome advantages:

- Custom styling,
- Vector styles,
- Free for ever :)

## Custom styling

Two styles have been derived from the ones available [here](http://editor.openmaptiles.org/).

You can import the one in the project `server/map/styles` to update them.

!> :warning: Before editing them, on the service mentioned above, you must replace some lines.

In `light/styles.json`, replace:

```json
    "sources": {
    "openmaptiles": {
            "type": "vector",
            "url": "mbtiles://{v3}"
        }
    },
    "sprite": "{styleJsonFolder}/sprite",
    "glyphs": "{fontstack}/{range}.pbf",
```

by:

```json
    "sources": {
        "openmaptiles": {
            "type": "vector",
            "url": "https://free.tilehosting.com/data/v3.json?key={key}"
        }
    },
    "sprite": "https://openmaptiles.github.io/osm-bright-gl-style/sprite",
    "glyphs": "https://free.tilehosting.com/fonts/{fontstack}/{range}.pbf?key={key}",
```

---

In `dark/styles.json`, replace:

```json
    "sources": {
    "openmaptiles": {
            "type": "vector",
            "url": "mbtiles://{v3}"
        }
    },
    "sprite": "{styleJsonFolder}/sprite",
    "glyphs": "{fontstack}/{range}.pbf",
```

by:

```json
    "sources": {
        "openmaptiles": {
            "type": "vector",
            "url": "https://free.tilehosting.com/data/v3.json?key={key}"
        }
    },
    "sprite": "https://openmaptiles.github.io/dark-matter-gl-style/sprite",
    "glyphs": "https://free.tilehosting.com/fonts/{fontstack}/{range}.pbf?key={key}",
```

?> Don't forget to make the reverse process when using them in our project.
