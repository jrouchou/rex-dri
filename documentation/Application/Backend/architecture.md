# Architecture

Visualization of the `backend` app architecture.

Vue complète :
![Architecture](../../generated/architecture.svg)

Vue plus précise :
![abstract](../../generated/abstract.svg)
![other_core](../../generated/core.svg)
![AbstractModules](../../generated/university.svg)
![Architecture](../../generated/location.svg)
![Architecture](../../generated/user.svg)
