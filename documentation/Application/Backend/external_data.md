# External data

`REX-DRI` interacts with several external services to provide consistent and up-to-date data to the application.

All the fetching of remote data is handled in a dedicated backend app: `external_data`. A command line interface has also been developed. Once connected to the backend app container, run:

```bash
./manage.py update_external_data --help
```

```txt
usage: manage.py update_external_data [-h] [--version] [-v {0,1,2,3}]
                                      [--settings SETTINGS]
                                      [--pythonpath PYTHONPATH] [--traceback]
                                      [--no-color]
                                      {all,currencies} ...

Command to handle updating remote data

[...]

subcommands:
  {all,currencies}
    all                 (default) Update all external data
    currencies          Update currencies from fixer
```

## Currencies

Currencies exchange rates are taken from [`fixer`](https://fixer.io/). You must provide a consistent API key in `server/envs/external_data.env` to be able to use the service properly.

To trigger un update of currencies, run `./manage.py update_external_data currencies`.

## UTC

TODO

## Auto update

TODO
