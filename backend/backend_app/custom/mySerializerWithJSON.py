from backend_app.fields import JSONField
from rest_framework import serializers

field_mapping = serializers.ModelSerializer.serializer_field_mapping
# Small hack to register our custom JSONField class as a regular JSONfield
field_mapping[JSONField] = serializers.JSONField


class MySerializerWithJSON(serializers.ModelSerializer):
    """
    Simple class to add serializing support for custom JSONField
    """

    serializer_field_mapping = field_mapping
