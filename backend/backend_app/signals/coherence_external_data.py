import logging

from django.db.models.signals import post_save

from backend_app.models.course import Course
from backend_app.models.exchange import Exchange
from backend_app.models.exchangeFeedback import ExchangeFeedback
from base_app.models import User

logger = logging.getLogger("django")


def update_exchange_on_course_save(sender, instance: Course, created, **kwargs):
    try:
        exchange = instance.exchange
        if exchange is not None:
            student_login = instance.student_login
            if student_login is None:
                exchange.student = None
            else:
                exchange.student = User.objects.get(username=student_login)
            exchange.save()
    except User.DoesNotExist:
        pass


def update_exchange_feedback_on_exchange_save(
    sender, instance: Exchange, created, **kwargs
):
    try:
        feedbacks: ExchangeFeedback = instance.feedbacks
        feedbacks.university = instance.university
        feedbacks.save()
    except ExchangeFeedback.DoesNotExist:
        pass


def enable_external_data_coherence():
    post_save.connect(update_exchange_on_course_save, sender=Course)
    post_save.connect(update_exchange_feedback_on_exchange_save, sender=Exchange)
