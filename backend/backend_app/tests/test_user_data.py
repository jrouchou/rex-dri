import json

from backend_app.models.userData import UserData, UserDataViewSet
from backend_app.tests.utils import WithUserTestCase


class UserDataTestCase(WithUserTestCase):
    """
    Some basic tests on the userData viewset.
    """

    @classmethod
    def setUpMoreTestData(cls):
        cls.api_user_data = "/api/{}/".format(UserDataViewSet.end_point_route)

    def test_none_is_returned(self):
        # make sure there is data in the db
        self.assertNotEqual((UserData.objects.all()), 0)
        # get the data from the apo
        response = self.staff_client.get(self.api_user_data)
        content = json.loads(response.content)
        # make sure only one is returned ant that it is the one corresponding to the requester
        self.assertEqual(len(content), 0)

    def test_get_working(self):
        """
        Will also indirectly check that backend_app_permissions_request works
        :return:
        """

        def test_for_user(user_to_get, client, user_client):
            response = client.get(self.api_user_data + "{}/".format(user_to_get.pk))
            if user_to_get.pk != user_client.pk:
                self.assertEqual(response.status_code, 404)
            else:
                content = json.loads(response.content)
                self.assertEqual(content["id"], user_client.pk)

        clients = [
            self.authenticated_client,
            self.moderator_client,
            self.dri_client,
            self.staff_client,
        ]
        users = [
            self.authenticated_user,
            self.moderator_user,
            self.dri_user,
            self.staff_user,
        ]
        for (client, client_user) in zip(clients, users):
            for user in users:
                test_for_user(user, client, client_user)
