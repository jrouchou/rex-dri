from django.test import override_settings
from reversion.models import Version

from backend_app.models.for_testing.versioning import ForTestingVersioning
from backend_app.tests.utils import WithUserTestCase
from backend_app.models.for_testing.moderation import ForTestingModeration
from backend_app.models.pendingModeration import PendingModeration
from django.contrib.contenttypes.models import ContentType


def retrieve_instance_in_moderation(instance, should_be_empty=False):
    ct = ContentType.objects.get_for_model(ForTestingModeration)
    pending = PendingModeration.objects.filter(content_type=ct, object_id=instance.pk)

    if should_be_empty:
        assert len(pending) == 0
        return None
    else:
        assert len(pending) > 0
        return pending[0]


def get_object_moder(pk):
    return ForTestingModeration.objects.filter(pk=pk)[0]


def get_object_versions(obj):
    return Version.objects.get_for_object(obj)


class CascadeDeleteModerationTestCase(WithUserTestCase):
    @classmethod
    def setUpMoreTestData(cls):
        cls.obj = ForTestingModeration.objects.create(aaa="v0")
        cls.api_moderation = "/api/test/moderation/{}/".format(cls.obj.pk)

    @override_settings(MODERATION_ACTIVATED=True)
    def test_delete_model_cascade_to_pending_moderation(self):
        """
        Test to check that when a model instance is deleted, all pending moderation
        data related to that instance is also deleted.
        """

        data_1 = {"aaa": "Test"}
        self.authenticated_client.put(self.api_moderation, data_1)

        new_obj_in_db = get_object_moder(self.obj.pk)
        self.assertTrue(new_obj_in_db.has_pending_moderation)

        retrieve_instance_in_moderation(new_obj_in_db, should_be_empty=False)
        new_obj_in_db.delete()
        retrieve_instance_in_moderation(new_obj_in_db, should_be_empty=True)

        # Really make sure there is nothing pending moderation
        self.assertEqual(len(PendingModeration.objects.all()), 0)


class CascadeDeleteVersioningTestCase(WithUserTestCase):
    @classmethod
    def setUpMoreTestData(cls):
        cls.obj = ForTestingVersioning.objects.create(bbb="v0")
        cls.api_version = "/api/test/versioning/{}/".format(cls.obj.pk)

    def test_delete_model_cascade_to_versions(self):
        """
        Test to check that when a versioned model instance is deleted, all
        other versions of this model is also deleted.
        """

        data_1 = {"bbb": "Test"}
        self.staff_client.put(self.api_version, data_1)
        data_2 = {"bbb": "Test"}
        self.moderator_client.put(self.api_version, data_2)
        self.assertEqual(len(get_object_versions(self.obj)), 2)

        self.obj.delete()
        self.assertEqual(len(get_object_versions(self.obj)), 0)

        # Really make sure all versions have been deleted
        self.assertEqual(len(Version.objects.all()), 0)
