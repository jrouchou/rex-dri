from reversion.models import Version

from backend_app.models.for_testing.versioning import ForTestingVersioning
from backend_app.tests.utils import WithUserTestCase


def get_object_ver(obj):
    return ForTestingVersioning.objects.filter(pk=obj.pk)[0]


def get_object_versions(obj):
    return Version.objects.get_for_object(obj)


class CascadeDeleteVersionsTestCase(WithUserTestCase):
    @classmethod
    def setUpMoreTestData(cls):
        cls.obj = ForTestingVersioning.objects.create(bbb="v0")
        cls.api_version = "/api/test/versioning/{}/".format(cls.obj.pk)

    def test_delete_model_cascade_to_versions(self):
        """
        Test to check that when a versioned model instance is deleted, all
        other versions of this model is also deleted.
        """

        data_1 = {"bbb": "Test"}
        self.staff_client.put(self.api_version, data_1)

        # Make sure we have a version
        versions = get_object_versions(self.obj)
        self.assertEqual(len(versions), 1)

        # Make sure we have the correct version number
        new_obj = get_object_ver(self.obj)
        self.assertEqual(new_obj.nb_versions, 1)

        versions.delete()
        # Make sure they really have been deleted
        versions = get_object_versions(self.obj)
        self.assertEqual(len(versions), 0)

        # Re-get the object, to make sure we have updated fields
        new_obj = get_object_ver(self.obj)

        # Final test
        self.assertEqual(new_obj.nb_versions, 0)
