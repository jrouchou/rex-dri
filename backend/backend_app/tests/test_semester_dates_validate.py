from django.test import TestCase
from backend_app.models.universitySemestersDates import (
    UniversitySemestersDatesSerializer,
)
import pytest
from rest_framework.validators import ValidationError

semesters = ["spring_begin", "spring_end", "autumn_begin", "autumn_end"]


class SemesterDatesTestCase(TestCase):
    def test_validation(self):
        def build(l):
            tmp = {sem: val for sem, val in zip(semesters, l)}
            tmp["comment"] = ""
            tmp["useful_links"] = []
            return tmp

        def _test_attrs_error(attrs):
            with pytest.raises(ValidationError):
                self.ser.validate(attrs)

        self.ser = UniversitySemestersDatesSerializer()
        _test_attrs_error(build([None, 3, None, None]))
        _test_attrs_error(build([2, 3, 3, 2]))
        self.ser.validate(build([None] * 4))
        self.ser.validate(build([2, 3, None, None]))
        self.ser.validate(build([2, 3, 2, 3]))
