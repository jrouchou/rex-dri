import reversion
from django.db import models
from rest_framework import serializers
from reversion.models import Version

from backend_app.models.abstract.essentialModule import (
    EssentialModule,
    EssentialModuleSerializer,
    EssentialModuleViewSet,
)
from backend_app.signals.squash_revisions import new_revision_saved


class VersionedEssentialModule(EssentialModule):
    """
    Custom EssentialModule that will be versioned in the app
    """

    # We store the current number of versions for better performance
    nb_versions = models.PositiveIntegerField(default=0)

    def delete(self, using=None, keep_parents=False):
        """
        Override the default delete behavior to make sure
        versions instances corresponding to the
        deleted instance are also deleted.
        """
        # We need to delete the versions first. Otherwise for some reason it wouldn't work.
        Version.objects.get_for_object(self).delete()
        super().delete(using, keep_parents)

    class Meta:
        abstract = True


class VersionedEssentialModuleSerializer(EssentialModuleSerializer):
    """
    Serializer for versioned models
    """

    # Add a nb_versions field
    nb_versions = serializers.IntegerField(read_only=True)

    def save(self, *args, **kwargs):
        """
        Custom save function to use reversion.
        """

        with reversion.create_revision():
            res = super().save(*args, **kwargs)
            reversion.set_user(res.updated_by)
        new_revision_saved.send(sender=self.__class__, obj=self.instance)
        return res

    def get_obj_info(self, obj) -> dict:
        """
        Serializer for the `obj_info` *dynamic* field. Redefined.
        """
        obj_info = super().get_obj_info(obj)
        obj_info["versioned"] = True
        return obj_info

    class Meta:
        model = VersionedEssentialModule
        fields = EssentialModuleSerializer.Meta.fields + ("nb_versions",)


class VersionedEssentialModuleViewSet(EssentialModuleViewSet):
    """
    Viewset for the versioned models
    """

    serializer_class = VersionedEssentialModuleSerializer
