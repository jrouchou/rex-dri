from django.db import models

from backend_app.models.abstract.scholarship import (
    Scholarship,
    ScholarshipSerializer,
    ScholarshipViewSet,
)
from backend_app.models.university import University
from backend_app.permissions.moderation import ModerationLevels


class UniversityScholarship(Scholarship):
    moderation_level = ModerationLevels.DEPENDING_ON_SITE_SETTINGS

    universities = models.ManyToManyField(
        University, related_name="university_scholarships"
    )


class UniversityScholarshipSerializer(ScholarshipSerializer):
    class Meta:
        model = UniversityScholarship
        fields = ScholarshipSerializer.Meta.fields + ("universities",)


class UniversityScholarshipViewSet(ScholarshipViewSet):
    permission_classes = ScholarshipViewSet.permission_classes
    queryset = UniversityScholarship.objects.all()  # pylint: disable=E1101
    serializer_class = UniversityScholarshipSerializer
    end_point_route = "universityScholarships"
    filterset_fields = ("universities",)
