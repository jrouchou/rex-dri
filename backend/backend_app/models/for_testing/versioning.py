import reversion
from django.db import models

from backend_app.models.abstract.versionedEssentialModule import (
    VersionedEssentialModule,
    VersionedEssentialModuleSerializer,
    VersionedEssentialModuleViewSet,
)
from backend_app.permissions.moderation import ModerationLevels


@reversion.register()
class ForTestingVersioning(VersionedEssentialModule):
    """
        Simple model for testing purposes (versioning)
    """

    moderation_level = ModerationLevels.DEPENDING_ON_SITE_SETTINGS

    bbb = models.CharField(max_length=100)


class ForTestingVersioningSerializer(VersionedEssentialModuleSerializer):
    """
        Simple Serializer for testing purposes (versioning)
    """

    class Meta:
        model = ForTestingVersioning
        fields = VersionedEssentialModuleSerializer.Meta.fields + ("bbb",)


class ForTestingVersioningViewSet(VersionedEssentialModuleViewSet):
    """
        Simple Viewset for testing purposes (versioning)
    """

    permission_classes = tuple()
    serializer_class = ForTestingVersioningSerializer
    queryset = ForTestingVersioning.objects.all()
    end_point_route = "test/versioning"
