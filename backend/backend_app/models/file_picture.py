from django.db import models
from rest_framework import serializers

from backend_app.models.abstract.base import (
    BaseModel,
    BaseModelSerializer,
    BaseModelViewSet,
)
from backend_app.permissions.app_permissions import NoDelete, IsStaff, IsOwner, ReadOnly
from backend_app.permissions.moderation import ModerationLevels
from backend_app.validation.validators import ImageValidator
from base_app.models import User


#########
# Models
#########


class AbstractFile(BaseModel):
    moderation_level = ModerationLevels.NO_MODERATION
    owner = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    file = models.FileField(upload_to="files/%Y/%m/%d/", blank=True, null=True)
    title = models.CharField(max_length=200, default="", blank=True, null=True)
    licence = models.CharField(max_length=100, default="", blank=True, null=True)
    description = models.CharField(max_length=500, default="", blank=True, null=True)

    class Meta:
        abstract = True


class File(AbstractFile):
    class Meta:
        pass


class Picture(AbstractFile):
    file = models.FileField(
        upload_to="pictures/%Y/%m/%d/",
        blank=True,
        null=True,
        validators=[ImageValidator()],
    )


#########
# Serializers
#########

# WARNING FIX BETA delete also file on delete
class FileSerializer(BaseModelSerializer):
    owner = serializers.StringRelatedField(read_only=True)

    def save(self, *args, **kwargs):
        instance = super().save(*args, **kwargs)
        instance.owner = self.get_user_from_request()
        instance.save()
        return instance

    class Meta:
        model = File
        fields = BaseModelSerializer.Meta.fields + (
            "owner",
            "file",
            "title",
            "licence",
            "description",
        )


class PictureSerializer(FileSerializer):
    class Meta:
        model = Picture
        fields = FileSerializer.Meta.fields


class FileSerializerFileReadOnly(FileSerializer):
    file = serializers.FileField(read_only=True)


class PictureSerializerFileReadOnly(PictureSerializer):
    file = serializers.FileField(read_only=True)


#########
# ViewSets
#########


class BaseFileViewSet(BaseModelViewSet):
    _serializer_not_read_only = None
    _serializer_read_only = None

    def get_serializer_class(self):
        """
        Custom get serializer to make file field readonly after it has been created
        """
        if hasattr(self, "request") and self.request.method == "PUT":
            return self._serializer_read_only
        else:
            return self._serializer_not_read_only

    permission_classes = (
        (NoDelete | IsStaff | IsOwner) & (ReadOnly | IsOwner | IsStaff),
    )


class FileViewSet(BaseFileViewSet):
    _serializer_not_read_only = FileSerializer
    _serializer_read_only = FileSerializerFileReadOnly
    queryset = File.objects.all()
    filterset_fields = ("owner",)
    end_point_route = "files"


class PictureViewSet(BaseFileViewSet):
    _serializer_not_read_only = PictureSerializer
    _serializer_read_only = PictureSerializerFileReadOnly
    queryset = Picture.objects.all()
    filterset_fields = ("owner",)
    end_point_route = "pictures"
