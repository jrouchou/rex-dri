from django.db import models

from backend_app.models.abstract.module import Module, ModuleSerializer, ModuleViewSet
from backend_app.models.university import University
from backend_app.permissions.app_permissions import IsStaff, IsDri, ReadOnly
from backend_app.permissions.moderation import ModerationLevels


class UniversityDri(Module):
    moderation_level = ModerationLevels.ENFORCED

    universities = models.ManyToManyField(University, related_name="university_dri")


class UniversityDriSerializer(ModuleSerializer):
    class Meta:
        model = UniversityDri
        fields = ModuleSerializer.Meta.fields + ("universities",)


class UniversityDriViewSet(ModuleViewSet):
    queryset = UniversityDri.objects.all()  # pylint: disable=E1101
    serializer_class = UniversityDriSerializer
    permission_classes = (IsStaff | IsDri | ReadOnly,)
    end_point_route = "universityDri"
    filterset_fields = ("universities",)
