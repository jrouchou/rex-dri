from django.db import models

from backend_app.models.abstract.base import BaseModel

from backend_app.permissions.moderation import ModerationLevels

from base_app.models import User
from backend_app.models.university import University


class LastVisitedUniversity(BaseModel):
    moderation_level = ModerationLevels.NO_MODERATION

    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    university = models.ForeignKey(University, on_delete=models.CASCADE, null=True)
    ts = models.DateTimeField(auto_now=True)
