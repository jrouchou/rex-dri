from django.db import models
from rest_framework import serializers
from rest_framework.response import Response

from backend_app.fields import JSONField
from backend_app.models.abstract.base import (
    BaseModel,
    BaseModelSerializer,
    BaseModelViewSet,
)
from backend_app.permissions.app_permissions import IsOwner, IsStaff, ReadOnly
from backend_app.permissions.moderation import ModerationLevels
from backend_app.utils import get_user_level, get_default_theme_settings
from backend_app.validation.validators import ThemeValidator
from base_app.models import User


class UserData(BaseModel):
    moderation_level = ModerationLevels.NO_MODERATION

    owner = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    theme = JSONField(default=get_default_theme_settings, validators=[ThemeValidator()])


class UserDataSerializer(BaseModelSerializer):
    owner = serializers.CharField(read_only=True)
    owner_level = serializers.SerializerMethodField()
    owner_can_post_to = serializers.SerializerMethodField()

    def get_owner_level(self, obj):
        return get_user_level(obj.owner)

    _list_user_post_function = None

    def get_owner_can_post_to(self, obj):
        """
        Serializer for the field `get_owner_level`.
        The function is imported at runtime to prevent annoying
        cyclic imports.
        """

        if self._list_user_post_function is None:
            from backend_app.permissions.request import (
                list_user_permission_for_request_type,
            )

            self._list_user_post_function = list_user_permission_for_request_type

        return self._list_user_post_function(obj.owner, "POST")

    def create(self, validated_data):
        validated_data["owner"] = self.get_user_from_request()
        return super().create(validated_data)

    class Meta:
        model = UserData
        fields = BaseModelSerializer.Meta.fields + (
            "owner",
            "owner_level",
            "owner_can_post_to",
            "owner",
            "theme",
        )


class UserDataViewSet(BaseModelViewSet):
    serializer_class = UserDataSerializer
    permission_classes = (IsOwner | (IsStaff & ReadOnly),)
    end_point_route = "userData"

    def list(self, request, *args, **kwargs):
        # Prevent the querying of all objects.
        return Response(list())

    def get_queryset(self):
        def get_for_querier():
            return UserData.objects.filter(
                owner=self.request.user
            )  # pylint: disable=E1101

        queryset = get_for_querier()
        if len(queryset) == 0:
            UserData.objects.update_or_create(owner=self.request.user)
            queryset = get_for_querier()
        return queryset
