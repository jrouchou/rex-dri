from django.db import models

from backend_app.models.abstract.base import (
    BaseModel,
    BaseModelSerializer,
    BaseModelViewSet,
)
from backend_app.permissions.app_permissions import ReadOnly


class Language(BaseModel):
    code_iso = models.CharField(max_length=5, primary_key=True)
    name = models.CharField(max_length=100, blank=False)


class LanguageSerializer(BaseModelSerializer):
    class Meta:
        model = Language
        fields = BaseModelSerializer.Meta.fields + ("name",)


class LanguageViewSet(BaseModelViewSet):
    serializer_class = LanguageSerializer
    queryset = Language.objects.all()  # pylint: disable=E1101
    end_point_route = "languages"
    permission_classes = (ReadOnly,)
