from backend_app.models.university import University
from backend_app.models.campus import Campus

campuses = Campus.objects.filter(is_main_campus=True).prefetch_related(
    "university", "city"
)


for campus in campuses:
    university = campus.university
    university.main_campus_lat = campus.lat
    university.main_campus_lon = campus.lon

    city = campus.city
    university.country = city.country
    university.city = city.name

    university.save()


for university in University.objects.all():
    assert university.city != "", "city failed"
    assert university.main_campus_lat != 0, "failed campus lat"
    assert university.main_campus_lon != 0, "failed campus lon"
    # can't check as there are universities in France
    # assert university.country.pk != "FR", "country failed"
