from django.contrib.auth.models import Group


class LoadGroups(object):
    """Class to add the default user groups to the app
    """

    def __init__(self):
        Group.objects.get_or_create(name="Moderators")
        Group.objects.get_or_create(name="DRI")
