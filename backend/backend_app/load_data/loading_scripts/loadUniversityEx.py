from datetime import datetime

from backend_app.models.country import Country
from backend_app.models.countryScholarship import CountryScholarship
from backend_app.models.course import Course
from backend_app.models.courseFeedback import CourseFeedback
from backend_app.models.currency import Currency
from backend_app.models.exchange import Exchange
from backend_app.models.exchangeFeedback import ExchangeFeedback
from backend_app.models.language import Language
from backend_app.models.partner import Partner
from backend_app.models.taggedItems import UniversityTaggedItem
from backend_app.models.university import University
from backend_app.models.universityDri import UniversityDri
from backend_app.models.universityInfo import UniversityInfo
from backend_app.models.universitySemestersDates import UniversitySemestersDates
from base_app.models import User
from .loadGeneric import LoadGeneric


class LoadUniversityEx(LoadGeneric):
    """
    Load some exemple data for the EPFL
    """

    def __init__(self, admin: User):
        self.admin = admin

    def load(self):
        EPFL = University.objects.get(acronym="EPFL")
        CHF = Currency.objects.get(pk="CHF")
        SWITZERLAND = Country.objects.get(pk="CH")

        univ_dri_1 = UniversityDri(
            title="Cours en anglais",
            importance_level="+",
            comment="Les cours de master en computer science sont 100% en anglais",
        )
        univ_dri_1.save()
        univ_dri_1.universities.add(EPFL)
        self.add_info_and_save(univ_dri_1, self.admin)

        univ_info = UniversityInfo.objects.get(university=EPFL)
        univ_info.cost_exchange = 0
        univ_info.costs_currency = CHF
        self.add_info_and_save(univ_info, self.admin)

        usd = UniversitySemestersDates.objects.get(university=EPFL)
        usd.autumn_begin = datetime.strptime("17/09/2018", "%d/%m/%Y")
        usd.autumn_end = datetime.strptime("29/01/2019", "%d/%m/%Y")
        usd.useful_links = [
            {
                "url": "https://memento.epfl.ch/academic-calendar",
                "description": "Site de l'EPFL",
            }
        ]
        self.add_info_and_save(usd, self.admin)

        country_scholarship = CountryScholarship(
            title="Swiss European Mobility Programme",
            short_description="Bourse du gouvernement suisse",
            currency=CHF,
            frequency="s",
            amount_min=2200,
            amount_max=2200,
            comment="Bourse attribuée de manière automatique.",
        )
        country_scholarship.save()
        country_scholarship.countries.add(SWITZERLAND)
        self.add_info_and_save(country_scholarship, self.admin)

        univ_tag_1 = UniversityTaggedItem(
            university=EPFL, comment="C'est compliqué de trouver un logement"
        )
        self.add_info_and_save(univ_tag_1, self.admin)

        EPFL_as_partner = Partner.objects.filter(university__pk=EPFL.pk)[0]
        exchange1 = Exchange.objects.create(
            partner=EPFL_as_partner,
            utc_id=1,
            utc_partner_id=EPFL_as_partner.pk,
            student=self.admin,
            year=2019,
            semester="a",
            duration=1,
            double_degree=False,
            master_obtained=False,
            student_major_and_semester="GI6",
            student_minor="FDD",
            student_option="No",
            utc_allow_courses=True,
            utc_allow_login=True,
        )

        ef = ExchangeFeedback.objects.update_or_create(
            exchange=exchange1,
            defaults=dict(
                university=EPFL,
                general_comment="Very good",
                academical_level_appreciation=5,
                foreign_student_welcome=5,
                cultural_interest=5,
            ),
        )[0]
        self.add_info_and_save(ef, self.admin)

        exchange2 = Exchange.objects.update_or_create(
            utc_id=2,
            defaults=dict(
                utc_partner_id=EPFL_as_partner.pk,
                student=self.admin,
                year=2018,
                semester="a",
                duration=1,
                double_degree=False,
                master_obtained=False,
                student_major_and_semester="GI4",
                student_minor="FDD",
                student_option="No",
                utc_allow_courses=False,
                utc_allow_login=False,
            ),
        )[0]

        ef = ExchangeFeedback.objects.update_or_create(
            exchange=exchange2,
            defaults=dict(
                university=EPFL,
                general_comment="Very good trop bien",
                academical_level_appreciation=4,
                foreign_student_welcome=3,
                cultural_interest=4,
            ),
        )[0]
        self.add_info_and_save(ef, self.admin)

        course1 = Course.objects.update_or_create(
            utc_id=1,
            defaults=dict(
                exchange=exchange1,
                utc_exchange_id=1,
                code="COM-401",
                title="Applied data science",
                link="",
                ects=5,
                category="TM",
                profile="PSF",
                tsh_profile="",
                student_login="admin",
            ),
        )[0]

        Course.objects.update_or_create(
            utc_id=2,
            defaults=dict(
                exchange=exchange1,
                utc_exchange_id=1,
                code="COM-480",
                title="Data vizualization",
                link="",
                ects=5,
                category="TM",
                profile="PSF",
                tsh_profile="",
                student_login="admin",
            ),
        )

        cf = CourseFeedback.objects.update_or_create(
            course=course1,
            defaults=dict(
                language=Language.objects.first(),
                comment="Trop bien",
                adequation=5,
                working_dose=4,
                following_ease=3,
            ),
        )[0]
        self.add_info_and_save(cf, self.admin)
