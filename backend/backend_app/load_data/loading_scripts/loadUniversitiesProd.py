import logging
from os.path import abspath, join

from backend_app.load_data.utils import ASSETS_PATH, csv_2_dict_list
from backend_app.models.campus import Campus
from backend_app.models.city import City
from backend_app.models.country import Country
from backend_app.models.partner import Partner
from backend_app.models.university import University
from base_app.models import User
from .loadGeneric import LoadGeneric

logger = logging.getLogger("django")


class LoadUniversitiesProd(LoadGeneric):
    """
    Load the universities in the app
    """

    def __init__(self):
        self.admin = User.objects.get(username="admin")

    @staticmethod
    def get_destination_data():
        destinations_path = abspath(join(ASSETS_PATH, "univsv0.9.3.csv"))
        return csv_2_dict_list(destinations_path)

    def load(self):
        for row in self.get_destination_data():
            partner_id = int(row["partner_id"])
            univ_id = int(row["univ_id"])

            logger.info("Partner : {} ---- univ: {}".format(partner_id, univ_id))
            if univ_id == partner_id:
                lat = round(float(row["lat"]), 6)
                lon = round(float(row["lon"]), 6)

                country = Country.objects.get(pk=row["code_iso"])
                city = City.objects.update_or_create(
                    name=row["ville"].title(), country=country
                )[0]
                self.add_info_and_save(city, self.admin)

                univ = University.objects.update_or_create(
                    pk=univ_id,  # Not perfect but should do the trick
                    defaults={
                        "name": row["name"],
                        "acronym": row["acronym"],
                        "website": row["website"],
                        # "logo": row["logo"],  # WARNING FIX BETA not ok
                    },
                )[0]
                self.add_info_and_save(univ, self.admin)
                main_campus = Campus.objects.update_or_create(
                    is_main_campus=True,
                    university=univ,
                    defaults=dict(
                        name="Campus - " + univ.name, city=city, lat=lat, lon=lon
                    ),
                )[0]
                self.add_info_and_save(main_campus, self.admin)

            else:
                univ = University.objects.get(pk=univ_id)

            partner = Partner.objects.get(utc_id=partner_id)
            partner.university = univ
            partner.save()
