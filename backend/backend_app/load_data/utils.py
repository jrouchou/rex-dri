import csv
from os import path

ASSETS_PATH = path.join(path.realpath(__file__), "../assets/")  # noqa: E402


def csv_2_dict_list(fp: str):
    """
    Reads a CSV file (with header row!) and returns it as a list of OrderedDict
    :param fp: csv file path
    :type fp: str
    :return:
    """
    with open(fp, "r") as f:
        reader = csv.DictReader(f, delimiter=",")
        return [r for r in reader]
