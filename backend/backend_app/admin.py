from base_app.admin import admin_site
from reversion_compare.admin import CompareVersionAdmin

from backend_app.models.abstract.versionedEssentialModule import (
    VersionedEssentialModule,
)
from backend_app.models.country import Country
from backend_app.models.countryDri import CountryDri
from backend_app.models.countryScholarship import CountryScholarship
from backend_app.models.course import Course
from backend_app.models.courseFeedback import CourseFeedback
from backend_app.models.currency import Currency
from backend_app.models.exchange import Exchange, UnivMajorMinors
from backend_app.models.exchangeFeedback import ExchangeFeedback
from backend_app.models.for_testing.moderation import ForTestingModeration
from backend_app.models.for_testing.versioning import ForTestingVersioning
from backend_app.models.lastVisitedUniversities import LastVisitedUniversity
from backend_app.models.offer import Offer
from backend_app.models.partner import Partner
from backend_app.models.pendingModeration import PendingModeration
from backend_app.models.recommendationList import RecommendationList
from backend_app.models.sharedUnivFeedback import SharedUnivFeedback
from backend_app.models.taggedItems import UniversityTaggedItem, CountryTaggedItem
from backend_app.models.university import University
from backend_app.models.universityDri import UniversityDri
from backend_app.models.universityInfo import UniversityInfo
from backend_app.models.universityScholarship import UniversityScholarship
from backend_app.models.universitySemestersDates import UniversitySemestersDates
from backend_app.models.userData import UserData
from backend_app.models.version import Version
from base_app.models import SiteInformation

ALL_MODELS = [
    SiteInformation,
    Country,
    CountryDri,
    CountryScholarship,
    CountryTaggedItem,
    Course,
    CourseFeedback,
    Currency,
    LastVisitedUniversity,
    Offer,
    PendingModeration,
    Exchange,
    ExchangeFeedback,
    UnivMajorMinors,
    RecommendationList,
    Partner,
    University,
    SharedUnivFeedback,
    UniversityDri,
    UniversityInfo,
    UniversityScholarship,
    UniversitySemestersDates,
    UniversityTaggedItem,
    UserData,
    Version,
]

# We also register testing to models to make sure migrations are created for them

ALL_MODELS += [ForTestingModeration, ForTestingVersioning]

CLASSIC_MODELS = filter(
    lambda m: not issubclass(m, VersionedEssentialModule), ALL_MODELS
)
VERSIONED_MODELS = filter(lambda m: issubclass(m, VersionedEssentialModule), ALL_MODELS)

#######
# Register the models
#######

for Model in CLASSIC_MODELS:
    # Register the model in the admin in a standard way
    admin_site.register(Model)

for Model in VERSIONED_MODELS:
    # Register the model in the admin with versioning
    admin_site.register(Model, CompareVersionAdmin)
