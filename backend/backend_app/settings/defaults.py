from dotmap import DotMap

from .utils import get_yml_file


DEFAULTS = DotMap(get_yml_file("defaults.yml"))

tmp = DEFAULTS.DEFAULT_OBJ_MODERATION_PERMISSIONS
OBJ_MODERATION_PERMISSIONS = {}

for key in tmp:
    OBJ_MODERATION_PERMISSIONS[key] = tmp[key]["level"]

DEFAULT_OBJ_MODERATION_LV = OBJ_MODERATION_PERMISSIONS["authenticated_user"]
