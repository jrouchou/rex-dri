from datetime import datetime, timedelta
import logging
import json

from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.shortcuts import render
from django.utils.timezone import make_aware
from webpack_loader.utils import get_files

from backend_app.utils import clean_route
from backend_app.viewsets import ALL_VIEWSETS
from base_app.forms import UserForm
from base_app.models import User
from _cron_tasks import (
    update_currencies,
    update_utc_ent,
    update_extra_denormalization,
    clear_and_clean_sessions,
    update_daily_stats,
)
from stats_app.models import DailyConnections, DailyExchangeContributionsInfo

logger = logging.getLogger("django")


def get_bundle_loc(name):
    return "/".join(get_files(name)[0]["url"].split("/")[:-1]) + "/"


def index(request):
    """
    View to to display the index app that contains the JS / CSS
    The "template" displayed is in ./templates/index.html
    """

    # We give the user object so that we can access its id in JS
    # and fetch userData
    user = request.user

    # small hack to get the correct location of the frontend bundled files
    front_bundle_loc = get_bundle_loc("main")

    # We also retrieve the list of all routes endpoints
    endpoints = list(map(lambda v: clean_route(v.end_point_route), ALL_VIEWSETS))
    return render(
        request,
        "index.html",
        dict(user=user, endpoints=endpoints, front_bundle_loc=front_bundle_loc),
    )


def stats(request):
    """
    Render the view that displays stats
    """
    dataset = request.GET.get("dataset")
    date_min = request.GET.get("date_min")
    date_max = request.GET.get("date_max")

    get_params_are_valid = True

    if (dataset != "daily_connections") and (dataset != "daily_exchange_contributions"):
        get_params_are_valid = False
        # set default dataset
        dataset = "daily_connections"

    try:
        date_min_parsed = datetime.strptime(date_min, "%Y-%m-%d")
        date_max_parsed = datetime.strptime(date_max, "%Y-%m-%d")
        if date_min < date_max:
            date_max_parsed, date_min_parsed = date_max_parsed, date_min_parsed
    except:  # noqa: E722
        get_params_are_valid = False
        now = make_aware(datetime.now())
        date_min = (now - timedelta(days=365)).strftime("%Y-%m-%d")
        date_max = now.strftime("%Y-%m-%d")

    if not get_params_are_valid:
        url = f"/stats/?dataset={dataset}&date_min={date_min}&date_max={date_max}"
        request_info = request.GET.get("request_info")

        if request_info:
            url = f"{url}&request_info={request_info}"

        return HttpResponseRedirect(url)

    if dataset == "daily_connections":
        daily_connections = DailyConnections.objects.filter(
            date__gte=date_min_parsed, date__lt=date_max_parsed
        )
        raw_data = [
            {"date": dc.date.strftime("%Y-%m-%d"), "nb_connections": dc.nb_connections}
            for dc in daily_connections
        ]

        cols = ["date", "nb_connections"]

    elif dataset == "daily_exchange_contributions":
        daily_contributions = DailyExchangeContributionsInfo.objects.filter(
            date__gte=date_min_parsed, date__lt=date_max_parsed
        ).prefetch_related("university")

        raw_data = [
            {
                "date": dc.date.strftime("%Y-%m-%d"),
                "type": dc.type,
                "university": f"{dc.university.pk} - {dc.university.name}",
                "major": dc.major,
                "minor": dc.minor,
                "exchange_semester": dc.exchange_semester,
                "nb_contributions": dc.nb_contributions,
            }
            for dc in daily_contributions
        ]

        cols = [
            "date",
            "type",
            "university",
            "major",
            "minor",
            "exchange_semester",
            "nb_contributions",
        ]
    else:
        raise NotImplementedError()

    stats_data = {c: [d[c] for d in raw_data] for c in cols}
    return render(request, "stats.html", dict(stats_data=json.dumps(stats_data)))


def rgpd_raw(request):
    """
    Render the view that displays only the RGPD conditions
    """
    return render(request, "rgpd_raw.html")


def cgu_rgpd(request):
    """
    Render the view that handles user accepting CGU and RGPD conditions
    """
    if request.method == "POST":
        user = User.objects.get(pk=request.user.pk)
        form = UserForm(request.POST, instance=user)
        if form.is_valid():
            user = form.save(commit=True)
            user.save()
    else:
        user = User.objects.get(pk=request.user.pk)
        form = UserForm(instance=user)

    if "next" in request.GET and user.has_validated_cgu_rgpd:
        # if the user has just validated everything, we redirect him to the location he requested
        return HttpResponseRedirect(request.GET["next"])
    else:
        return render(request, "cgu_rgpd.html", dict(user=user, form=form))


def banned(request):
    user = request.user
    if user.is_banned:
        return render(request, "banned.html", dict(user=user))
    else:
        return HttpResponseNotFound()


def media_files_view(request, path):
    """
    Media files are served by nginx only if the user is connected.
    The authentication checked is performed through the middleware
    so here we only need to return a dumb request with the right headers
    that will be read by nginx.
    """
    response = HttpResponse()
    del response["Content-Type"]
    response["X-Accel-Redirect"] = "/protected-assets/media/" + path
    return response


def trigger_cron(request):
    """
    Render the view that displays cron tasks
    """
    if request.method == "POST":
        cron_task = request.POST.get("cron_name")
        if cron_task == "update_currencies":
            update_currencies()
        elif cron_task == "update_utc_ent":
            update_utc_ent()
        elif cron_task == "update_extra_denormalization":
            update_extra_denormalization()
        elif cron_task == "clear_and_clean_sessions":
            clear_and_clean_sessions()
        elif cron_task == "update_daily_stats":
            update_daily_stats()
        else:
            return HttpResponseNotFound()

    return render(request, "admin/trigger_cron.html")
