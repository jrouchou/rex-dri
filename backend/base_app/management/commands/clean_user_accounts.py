import logging

from django.contrib.sessions.management.commands.clearsessions import (
    Command as ClearSessionCommand,
)
from django.core.management.base import BaseCommand
from django_cas_ng.models import ProxyGrantingTicket, SessionTicket

from backend_app.models.recommendationList import RecommendationList
from backend_app.models.userData import UserData
from backend_app.utils import get_default_theme_settings
from base_app.models import User

logger = logging.getLogger("django")


class Command(BaseCommand):
    help = "Command to handle user accounts emptying"

    def handle(self, *args, **options):
        ClearUserAccounts.run()
        ClearSessions.run()


class ClearUserAccounts(object):
    @staticmethod
    def run():
        for user in User.objects.filter(delete_next_time=True):
            logger.info("Emptying account of user {}".format(user.pk))

            # for user Data we don't delete it but restore it to default
            # We do this to be consistent with the assumption that each user has some userData
            user_data = UserData.objects.get(pk=user.pk)
            user_data.theme = get_default_theme_settings()
            user_data.save()

            # deleting all owned private lists
            RecommendationList.objects.filter(owner=user, is_public=False).delete()

            # Emptying user model
            user.allow_sharing_personal_info = False
            user.secondary_email = ""
            user.pseudo = "Del."
            user.has_validated_cgu_rgpd = False
            user.is_banned = False

            user.username = "__Deleted__{}".format(user.pk)
            user.email = ""
            user.is_active = False

            user.delete_next_time = False
            user.is_deleted = True

            user.save()


class ClearSessions(object):
    @staticmethod
    def run():
        ClearSessionCommand().handle()
        ProxyGrantingTicket.clean_deleted_sessions()
        SessionTicket.clean_deleted_sessions()
        # Still error 500 on delete then reconnect... Unknown
