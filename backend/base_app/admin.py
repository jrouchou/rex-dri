from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm
from rest_framework.authtoken.admin import TokenAdmin
from django.urls import path

from base_app.models import User

# create a custom admin site
from base_app.views import trigger_cron


class CustomAdminSite(admin.AdminSite):
    """
    Custom admin site used to add a trigger_cron view
    on the admin site provided by django
    """

    def get_urls(self):
        urls = super().get_urls()
        urls += [path("trigger_cron/", self.admin_view(trigger_cron))]
        return urls


admin_site = CustomAdminSite(name="custom_admin_site")


# Handling of the registration of the custom User model to make sure
# we can see all the fields.
# taken from: https://stackoverflow.com/a/15013810
class CustomUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = User


class CustomUserAdmin(UserAdmin):
    form = CustomUserChangeForm

    fieldsets = UserAdmin.fieldsets + (
        (
            None,
            {"fields": ("allow_sharing_personal_info", "secondary_email", "pseudo")},
        ),
    )


admin_site.register(User, CustomUserAdmin)

# Pour la génération de token dans l'administration du site.
TokenAdmin.raw_id_fields = ("user",)
