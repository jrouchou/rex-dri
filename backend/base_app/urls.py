import django_cas_ng.views
from django.conf import settings
from django.conf.urls import include, url
from django.views.generic.base import RedirectView

from base_app.views import media_files_view
from base_app.admin import admin_site
from . import views

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [url(r"^__debug__/", include(debug_toolbar.urls))]
else:
    urlpatterns = []

urlpatterns += [
    url(r"^admin/", admin_site.urls),
    url(r"^user/login$", django_cas_ng.views.LoginView.as_view(), name="cas_ng_login"),
    url(
        r"^user/logout$", django_cas_ng.views.LogoutView.as_view(), name="cas_ng_logout"
    ),
    url(
        r"^user/callback$",
        django_cas_ng.views.CallbackView.as_view(),
        name="cas_ng_proxy_callback",
    ),
    url(r"^app/.*", views.index),
    url(r"^stats/.*", views.stats),
    url(r"^cgu-rgpd/.*", views.cgu_rgpd),
    url(r"^rgpd-raw/.*", views.rgpd_raw),
    url(r"^banned_note/", views.banned),
    url(r"^$", RedirectView.as_view(url="./app/"), name="go to real home"),
    url(r"", include("backend_app.urls")),
    url(r"^media/(?P<path>.*)", media_files_view, name="media"),
]
