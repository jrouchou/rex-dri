# This image is based on a python image.

# Use of stretch instead of Alpine for faster install of python packages
# Overall performance might be slightly better dut to the use of different lib (but with bigger image size obviously)
FROM python:3.7.2-slim-stretch
SHELL ["/bin/bash", "-c"]

WORKDIR /usr/src/app

RUN pip install --upgrade pip
# Installing main python packages
COPY requirements.txt /usr/src/app/requirements.txt

# python3-dev, libpq-dev and gcc  is for psycopg2-binary and uwsgi
# We do a lot of && to keep the image size small :)
RUN BUILD_DEPENCIES='libpq-dev python3-dev gcc' \
    && apt-get update \
    && apt-get install -y --no-install-recommends \
		${BUILD_DEPENCIES} \
		make \
	&& pip install -r requirements.txt \
	&& apt-get remove --auto-remove -y ${BUILD_DEPENCIES} \
	&& rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# More python dependencies if in dev env
ARG BUILD_PRODUCTION_IMAGE="false"

COPY requirements.dev.txt /usr/src/app/requirements.dev.txt
RUN if [ "x$BUILD_PRODUCTION_IMAGE" = "xfalse" ]; then echo "building image in dev setting" && pip install -r requirements.dev.txt; else echo "building image in production setting"; fi

## Add the wait script to the image to wait for the database to be up for sure
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.5.0/wait /wait
RUN chmod +x /wait
