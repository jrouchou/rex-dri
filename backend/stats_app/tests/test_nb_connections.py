from datetime import timedelta

from django.test import TestCase

from base_app.models import User
from stats_app.compute_stats import update_daily_connections
from stats_app.models import DailyConnections
from stats_app.utils import get_daily_connections, get_today_as_datetime


class StatsConnectionsTest(TestCase):
    def test_get_daily_connections(self):
        today = get_today_as_datetime()
        yesterday = today - timedelta(days=1)
        for i in range(10):
            User.objects.update_or_create(
                username=f"{i}", defaults=dict(last_login=yesterday)
            )

        daily_connections = get_daily_connections(yesterday)
        self.assertEqual(daily_connections, 10)

        two_days_ago = today - timedelta(days=2)
        for i in range(10, 20):
            User.objects.update_or_create(
                username=f"{i}", defaults=dict(last_login=two_days_ago)
            )

        daily_connections = get_daily_connections(yesterday)
        self.assertEqual(daily_connections, 10)

    def test_update_daily_connections(self):
        today = get_today_as_datetime()
        yesterday = today - timedelta(days=1)
        for i in range(10):
            User.objects.update_or_create(
                username=f"{i}", defaults=dict(last_login=yesterday)
            )

        update_daily_connections()

        yesterday_daily_connections = DailyConnections.objects.get(date=yesterday)

        self.assertEqual(yesterday_daily_connections.nb_connections, 10)
