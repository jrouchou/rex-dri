from datetime import timedelta

from django.test import TestCase

from backend_app.models.exchangeFeedback import ExchangeFeedback
from backend_app.models.exchange import Exchange
from backend_app.tests.utils import get_dummy_university
from stats_app.compute_stats import update_daily_exchange_contributions_info
from stats_app.models import DailyExchangeContributionsInfo
from stats_app.utils import get_today_as_datetime, get_contributions_profiles


class StatsContributionsTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.today = get_today_as_datetime()
        cls.yesterday = cls.today - timedelta(days=1)

        cls.univ = get_dummy_university()
        utc_partner_id = cls.univ.corresponding_utc_partners.all()[0].utc_id

        for i in range(2):
            exchange = Exchange.objects.update_or_create(
                pk=i,
                defaults=dict(
                    utc_id=i,
                    utc_partner_id=utc_partner_id,
                    year=2019,
                    semester="A",
                    student_major_and_semester="IM4",
                    student_minor="IDI",
                    duration=2,
                    double_degree=False,
                    master_obtained=False,
                    utc_allow_courses=True,
                    utc_allow_login=True,
                    university=cls.univ,
                ),
            )[0]
            ExchangeFeedback.objects.update_or_create(
                exchange=exchange,
                defaults=dict(
                    updated_on=cls.yesterday, untouched=False, university=cls.univ
                ),
            )

    def test_get_contributions_profiles(self):
        daily_contributions_profiles = get_contributions_profiles(self.yesterday)
        self.assertEqual(len(daily_contributions_profiles), 2)
        for p in daily_contributions_profiles:
            self.assertEqual(p.major, "IM")
            self.assertEqual(p.minor, "IDI")
            self.assertEqual(p.exchange_semester, "A2019")
            self.assertEqual(p.university_pk, self.univ.pk)

    def test_update_daily_exchange_contributions_info(self):
        update_daily_exchange_contributions_info()

        contributions = DailyExchangeContributionsInfo.objects.filter(
            date=self.yesterday
        )

        self.assertEqual(len(contributions), 1)
        contribution: DailyExchangeContributionsInfo = contributions[0]
        self.assertEqual(contribution.university.pk, self.univ.pk)
        self.assertEqual(contribution.major, "IM")
        self.assertEqual(contribution.minor, "IDI")
        self.assertEqual(contribution.exchange_semester, "A2019")
        self.assertEqual(contribution.nb_contributions, 2)
        self.assertEqual(contribution.type, "exchange_feedback")
